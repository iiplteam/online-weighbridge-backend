var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cors = require('cors');
var autoIncrement = require('mongoose-auto-increment');


// config file
var configdata = require('./config.js');

//mongoose connection
var mongoose = require('mongoose');
mongoose.connect(configdata.mongo, {
  useNewUrlParser: true
});
autoIncrement = require('mongoose-auto-increment');
// mongodb
// var mongo_url = 'mongodb://localhost:27017/online-weighbridge'

var mongo_url = configdata.mongo;
var Agent = require('sqlagent/mongodb').connect(mongo_url);

// importing emitter module
const EventEmitter = require('events');
const myEmitter = new EventEmitter();

// importing pm2 module
var pm2 = require('pm2');
var as = require('async')
var app = express();


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var truckRouter = require('./routes/truckMaster.route');
var customerRouter = require('./routes/customerOrder.router');
var configusersRouter = require('./routes/configusers.router');
const roleMgmtRouter = require('./routes/roleMgmt.router');
const userMgmtRouter = require('./routes/userMgmt.router');
var configurationRouter = require('./routes/configuration.router');
var cardMasterRouter = require('./routes/cardMaster.router');
var reportRouter = require('./routes/reportRouter.router');
var offlineRouter = require('./routes/offlineWeightBridge.router');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//Integrating cors and bodyParser plugins to express
var corsOptions = {
  origin: 'http://localhost:4200/',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
app.use(cors(corsOptions));
app.use(bodyParser.json())
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static('public'));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname + '/public/index.html'));
});


app.use('/api/truck', truckRouter);
app.use('/api/customer', customerRouter);
app.use('/api/configUsers', configusersRouter);
app.use('/api/roleMgmt', roleMgmtRouter);
app.use('/api/userMgmt', userMgmtRouter);
app.use('/api/config', configurationRouter);
app.use('/api/card', cardMasterRouter);
app.use('/api/reports', reportRouter);
app.use('/api/offline', offlineRouter);


app.get('/api/filling-data', async function (req, res) {
  var bayNum = req.query.bayNum;
  try {
    var nosql = new Agent();
    // get the data from the bay_progress table
    nosql.select('customerOrder', 'customer_orders').make(function (builder) {
      builder.where('bay_no', bayNum);
      builder.sort("serial_id", "desc");
    })
    var customerOrder = await nosql.promise('customerOrder');
    if (customerOrder == null || customerOrder.length == 0) {
      res.json({ status: false, message: "No data found", data: {} })
    } else {
      res.json({ status: true,  data: JSON.stringify(customerOrder[0]) })
    }
  } catch (e) {
    console.log(e);
    res.json({ status: false, message: "No data found", data: {} })
  }
})


// app.get('/api/filling-data', async function (req, res) {
//   var bayNum = req.query.bayNum;
//   //var cardNum = req.query.cardNum;
//   res.setHeader('Cache-Control', 'no-cache');
//   res.setHeader('Content-Type', 'text/event-stream');
//   res.setHeader("Access-Control-Allow-Origin", "*");
//   res.flushHeaders(); // flush the headers to establish SSE with client
//   let counter = 0;
//   // get data from the db 

//   let interValID = setInterval(async () => {
//     try {
//       // console.log("BAYYYYYYYY NUM",bayNum);
//       var nosql = new Agent();

//       // get the data from the bay_progress table

//       nosql.select('customerOrder', 'customer_orders').make(function (builder) {

//         builder.where('bay_no', bayNum);
//         builder.sort("serial_id", "desc");
//         //builder.first();
//       })
//       var customerOrder = await nosql.promise('customerOrder');
//       // nosql.select('customerOrder', 'bay_details').make(function (builder) {
//       //   builder.where('card_num', cardNum);
//       //   builder.first();
//       // })
//       //var customerOrder = await nosql.promise('customerOrder');
//       //console.log("customerOrder",customerOrder);
//       if (customerOrder == null || customerOrder.length == 0) {
//         res.write(`data:{}`);
//       } else {
//         res.write(`data: ${JSON.stringify(customerOrder[0])}\n\n`); // res.write() instead of res.send()
//       }

//     } catch (e) {
//       console.log(e);
//       res.write(`data:{}`);
//     }
//   }, 1000);
// })

app.get('/loading.pdf', (req, res) => {
  res.sendFile(path.join(__dirname + '/loading.pdf'));
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/public/index.html'));
});

// emitter function/
myEmitter.on('comm-service', async function services() {
  var nosql = new Agent();
  nosql.select('docs', 'configuration').make(function (builder) {
    builder.where('configurationName', 'comm_config');
    builder.first();
  });

  var docs = await nosql.promise('docs');
  var configuration = docs.configurationDetails;
  pm2.connect(function (err) {
    if (err) {
      console.error(err);
      return;
    }
    as.eachOfLimit(configuration, 1, function (bayData, key, callback) {
      if (bayData.isActive == false) {
        pm2.stop(`bay-${bayData.bayNo}`, (err, proc) => {
          console.log(`bay-${bayData.bayNo} successfully stopped`);
          callback();
        })
      } else {
        pm2.restart(`bay-${bayData.bayNo}`, (err, proc) => {
          console.log(`bay-${bayData.bayNo} successfully restarted`);
          callback();
        })

      }

    }, function (err) {
      if (err) console.error(err.message);
      // configs is now a map of JSON data
      pm2.disconnect();
    });
  })

})


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.sendFile(path.join(__dirname + '/public/index.html'));
  //next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


app.listen(3000, console.log("SERVER RUNNING ON PORT 3000!"))
module.exports = app;
