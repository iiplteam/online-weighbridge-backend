// adjustableValue

const SerialPort = require('serialport');
const Delimiter = require('@serialport/parser-delimiter');
const Readline = require('@serialport/parser-readline');
var logger = require('logger').createLogger('development.log');
const Ready = require('@serialport/parser-ready');
var request = require('request');

//return;
// node .\comm.js --bay=2 --comm=COM2 --value=0
// mongodb
var mongo_url = 'mongodb://localhost:27017/online-weighbridge'
//var mongo_url = 'mongodb+srv://admin:VsKl9x8yEWIqllxh@developmentdb.m73oz.mongodb.net/online-weighbridge?retryWrites=true&w=majority'
var Agent = require('sqlagent/mongodb').connect(mongo_url);
var nosql = new Agent();
var action = "";
action = "AWAIT_CARD";
// action = "AWAIT_CARD"  - before card
// action = "AWAIT_FILLING_PROGRESS" -
// action = "FILLING_INPROGRESS" -   Y , 1 
// action = "LOADING_COMPLETED" - G , T
var chunk = "";
var DEBUG = false;
var port;
var WRITE_COMMAND;
var oldchunk = "";
var OLDCARD = "";
var BAY_NO = 7;
var com_port = "";
// COMM BAY 100 / 50 / 100 / 250 
var adjustableValue = 0;
async function init() {
    // get comm from the db
    var nosql = new Agent();
    nosql.select('docs', 'configuration').make(function (builder) {
        builder.where('configurationName', 'bay_config');
        builder.first();
    });

    var docs = await nosql.promise('docs');

    var configuration = docs.configurationDetails;
    //console.log("configuration",configuration);
    configuration.map(comm => {
       
        if(parseInt(comm.bayNo) == BAY_NO) {
            //console.log("comm",comm);
            com_port = comm.commm;
            adjustableValue = parseInt(comm.adjustableValue);
        }
    })
    console.log("comm--------", com_port, BAY_NO)
    port = new SerialPort(com_port, { baudRate: 9600 });
    port.on('open', function () {
        port.on('readable', function () {
            port.read();
        })
    })
    const parserD = port.pipe(new Delimiter({ delimiter: String.fromCharCode(30) }));
    parserD.on('data', async function (data) {
        chunk = data.toString();
        if (oldchunk != chunk) {
            console.log("DATA parserD", chunk, action);
        }

        DataProcessing(chunk);
        oldchunk = chunk;
    });
    port.on('error', function (err) {
        setTimeout(function () {
            init();
        },5000)
        console.log('Error: ', err.message)
    });
}
init();

async function DataProcessing(data) {
    var singleData;
    if (chunk.indexOf(String.fromCharCode(29))) {
        singleData = data.substring(chunk.indexOf(String.fromCharCode(29)));
    } else {
        singleData = data;
    }
    //var singleData = chunk.replace();
    //console.log("sin", singleData);
    var splitData = singleData.split(",");
    //console.log(singleData);
    var firstFieldLength = splitData[0].length;
    var CARD_NO = singleData[0];
    // console.log("firstFieldLength", splitData[0], firstFieldLength);
    // action = "AWAIT_CARD"  - before card
    // action = "AWAIT_FILLING_PROGRESS" -
    // action = "FILLING_INPROGRESS" -   Y , 1 
    // action = "LOADING_COMPLETED" - G , T

    if (firstFieldLength > 2) {
        nosql.select('customerOrder', 'customer_orders').make(function (builder) {
            builder.where('card_num', splitData[0].substring(1));
            builder.sort("creationDate", 'desc')
            builder.first();
        })
        var customerOrder = await nosql.promise('customerOrder');
        //console.log("customerOrder------------------", JSON.stringify(customerOrder));
        if (customerOrder == null) {
            //console.log("INVALID_CARD");
            var INVALID_CARD = `F#1=${String.fromCharCode(2)}IVC`;
            if (OLDCARD != CARD_NO) {
                sendCommandToScale(INVALID_CARD);
            }
            return;
        }
        if (customerOrder.action == null) {
            action = "AWAIT_CARD"
        } else {

            if(action == "AWAIT_CARD" && customerOrder.action == "AWAIT_FILLING_PROGRESS"){
                action = "AWAIT_CARD"
            }else{
                action = customerOrder.action;
            }          
        }
        var Wt = splitData[3];
        var vehicleNum = customerOrder.truck_number;
        var cardNum = customerOrder.card_num;
        var maxGrossWt = customerOrder.max_gross_wt;
        var x = maxGrossWt - parseInt(Wt);
        var z = parseInt(customerOrder.qty_to_be_loaded)
        var t2 = Math.min(x, z);
        if (adjustableValue != 0) {
            t2 = t2 - adjustableValue;
        }

        if(action == "AWAIT_CARD_RESET" && singleData.indexOf("G,R") != -1){
            
            var diff = parseInt(Wt) - actual_wt; 
            var next_filling = customerOrder.target_wt - Math.Abs(diff);
            t2 = Math.min(t2, next_filling);

            if(customerOrder.reset_logs == null){
                customerOrder.reset_logs = [];
            }

            var reset_logs = {
                target_wt : t2,
                actual_wt : parseInt(Wt),
                card_punched : new Date()
            };

            customerOrder.reset_logs.push(reset_logs);

            nosql.update('updateBayData', 'customer_orders').make(function (builder) {
                builder.set('action', action);
                builder.set('status', 'inprogress');
                builder.set('bay_no', BAY_NO + "");
                builder.set('reset_logs', customerOrder.reset_logs);
                builder.where('card_num', cardNum);
            })
            await nosql.promise('updateBayData');

            var wrirtecardNum = "";
            var wrirtevechileNumber = "";
            var wrirtetargetWt = "" + (t2);
            if (wrirtetargetWt.length != 5) {
                if (wrirtetargetWt.length == 4) {
                    wrirtetargetWt = wrirtetargetWt + " ";
                }
                if (wrirtetargetWt.length == 3) {
                    wrirtetargetWt = wrirtetargetWt + "  ";
                }
            }
            if (cardNum.length == 4) {
                wrirtecardNum = cardNum;
            }
            if (cardNum.length == 3) {
                wrirtecardNum = cardNum + " ";
            }
            if (vehicleNum.length == 10) {
                wrirtevechileNumber = vehicleNum;
            }
            if (vehicleNum.length == 9) {
                wrirtevechileNumber = vehicleNum + " ";
            }
            if (vehicleNum.length == 8) {
                wrirtevechileNumber = vehicleNum + "  ";
            }
            if (vehicleNum.length == 7) {
                wrirtevechileNumber = vehicleNum + "   ";
            }
            if (vehicleNum.length == 6) {
                wrirtevechileNumber = vehicleNum + "    ";
            }

            WRITE_COMMAND = `F#1=2S1${BAY_NO}${wrirtecardNum}${wrirtevechileNumber}${wrirtetargetWt}000000000000050 1`;
            sendCommandToScale(WRITE_COMMAND);
            sendCommandToScale(WRITE_COMMAND);
            setTimeout(function () {
                WRITE_COMMAND = `F#1=2S1${BAY_NO}${wrirtecardNum}${wrirtevechileNumber}${wrirtetargetWt}0000000000TOK50 1`;
            }, 500);
        }

        if(action == "AWAIT_CARD_RESET" && singleData.indexOf('G,V') != -1){
            action = "AWAIT_FILLING_PROGRESS";
            var bay_status = "Engaged";

            var dataObj = {
                earthing_connected: splitData[4],
                loading_started: splitData[5],
                loading_complete: splitData[9],
                card_punch_time: new Date(),
                bay_status: bay_status,
            }
            nosql.update('updateBayData', 'customer_orders').make(function (builder) {
                builder.set('action', action);
                builder.set('status', 'inprogress');
                builder.set(dataObj);
                builder.where('card_num', cardNum);
            });
            await nosql.promise('updateBayData');
        }


        if (action == "AWAIT_CARD" && singleData.indexOf("G,R") != -1) {
           
            nosql.update('updateBayData', 'customer_orders').make(function (builder) {
                builder.set('action', action);
                builder.set('status', 'inprogress');
                builder.set('bay_no', BAY_NO + "");
                builder.set('target_wt', t2);
                builder.where('card_num', cardNum);
            })
            await nosql.promise('updateBayData');

            // F#1=2S129650TS07UJ922417950000000000000050 
            var wrirtecardNum = "";
            var wrirtevechileNumber = "";
            var wrirtetargetWt = "" + (t2);
            if (wrirtetargetWt.length != 5) {
                if (wrirtetargetWt.length == 4) {
                    wrirtetargetWt = wrirtetargetWt + " ";
                }
                if (wrirtetargetWt.length == 3) {
                    wrirtetargetWt = wrirtetargetWt + "  ";
                }
            }
            if (cardNum.length == 4) {
                wrirtecardNum = cardNum;
            }
            if (cardNum.length == 3) {
                wrirtecardNum = cardNum + " ";
            }
            if (vehicleNum.length == 10) {
                wrirtevechileNumber = vehicleNum;
            }
            if (vehicleNum.length == 9) {
                wrirtevechileNumber = vehicleNum + " ";
            }
            if (vehicleNum.length == 8) {
                wrirtevechileNumber = vehicleNum + "  ";
            }
            if (vehicleNum.length == 7) {
                wrirtevechileNumber = vehicleNum + "   ";
            }
            if (vehicleNum.length == 6) {
                wrirtevechileNumber = vehicleNum + "    ";
            }

            WRITE_COMMAND = `F#1=2S1${bay_no}${wrirtecardNum}${wrirtevechileNumber}${wrirtetargetWt}000000000000050 1`;
            sendCommandToScale(WRITE_COMMAND);
            sendCommandToScale(WRITE_COMMAND);
            setTimeout(function () {
                WRITE_COMMAND = `F#1=2S1${bay_no}${wrirtecardNum}${wrirtevechileNumber}${wrirtetargetWt}0000000000TOK50 1`;
            }, 500);
        }
        if (singleData.indexOf('G,V') != -1 && action == "AWAIT_CARD") {
            action = "AWAIT_FILLING_PROGRESS";
            var bay_status = "Engaged";

            var dataObj = {
                actual_wt: parseInt(Wt),
                earthing_connected: splitData[4],
                loading_started: splitData[5],
                loading_complete: splitData[9],
                //target_wt: targetWt,
                card_punch_time: new Date(),
                bay_status: bay_status,
                target_wt_write: t2
            }
            nosql.update('updateBayData', 'customer_orders').make(function (builder) {
                builder.set('action', action);
                builder.set('status', 'inprogress');
                builder.set(dataObj);
                builder.where('card_num', cardNum);
            });
            await nosql.promise('updateBayData');
        }
        if (singleData.indexOf('Y,1') != -1 && action == "AWAIT_FILLING_PROGRESS") {
            //console.log("dataObj", dataObj);
            sendCommandToScale(WRITE_COMMAND);

            action = "FILLING_INPROGRESS";
            var dataObj = {
                filling_progress_wt: 0,
                earthing_connected: splitData[4],
                loading_started: splitData[5],
                loading_complete: splitData[9],
                //target_wt: targetWt,
                //card_punch_time: new Date()
            }
            nosql.update('updateBayData', 'customer_orders').make(function (builder) {
                builder.set(dataObj);
                builder.set('start_time', new Date());
                builder.set('action', action);
                builder.set('status', 'inprogress');
                builder.where('card_num', cardNum);
            })
            await nosql.promise('updateBayData');

            WRITE_COMMAND = "";
        }
        if (singleData.indexOf('N,V') != -1 && action == "FILLING_INPROGRESS") {

            action = "FILLING_INPROGRESS";
            var dataObj = {
                filling_progress_wt: parseInt(Wt),
                earthing_connected: splitData[4],
                loading_started: splitData[5],
                loading_complete: splitData[9],
                //target_wt: targetWt,
                //card_punch_time: new Date()
            }
            nosql.update('updateBayData', 'customer_orders').make(function (builder) {
                builder.set(dataObj);
                builder.set('action', action);
                builder.set('status', 'inprogress');
                builder.where('card_num', cardNum);
            })
            await nosql.promise('updateBayData');
        }
        if (singleData.indexOf('N,T') != -1 && action == "FILLING_INPROGRESS") {
            //console.log("dataObj", dataObj);
            action = "FILLING_INPROGRESS";
            var dataObj = {
                filling_progress_wt: parseInt(Wt),
                earthing_connected: splitData[4],
                loading_started: splitData[5],
                loading_complete: splitData[9],
                //target_wt: targetWt,
                //card_punch_time: new Date()
            }
            nosql.update('updateBayData', 'customer_orders').make(function (builder) {
                builder.set(dataObj);
                builder.set('action', action);
                builder.set('status', 'inprogress');
                builder.where('card_num', cardNum);
            })
            var updateBayData = await nosql.promise('updateBayData');
            // console.log("updateBayData", updateBayData);
        }
        if (singleData.indexOf() != -1) {
            var STOP_COMMAND = `F#1=${String.fromCharCode(2)}O`;
            sendCommandToScale(STOP_COMMAND);
        }
        if (singleData.indexOf('G,T') && (singleData.indexOf('Y,0,1') != -1 || singleData.indexOf('N,0,1') != -1) && action == "FILLING_INPROGRESS") {
            action = "LOADING_COMPLETED";

            var dataObj = {
                // filling_progress_wt: parseInt(Wt),
                earthing_connected: splitData[4],
                loading_started: splitData[5],
                loading_complete: splitData[9],
                //target_wt: targetWt,
                gross_wt: parseInt(Wt)
                //card_punch_time: new Date()
            }
            nosql.update('updateBayData', 'customer_orders').make(function (builder) {
                builder.set(dataObj);
                builder.set('end_time', new Date());
                builder.set('action', action);
                builder.set('status', 'completed');
                builder.where('card_num', cardNum);
            })
            await nosql.promise('updateBayData');

            setTimeout(function () {
                moveCustomerOrder(customerOrder._id);
            }, 100000);
        }
    }

    OLDCARD = CARD_NO;

    if (firstFieldLength < 2 && singleData.indexOf('G,R') != -1) {
        action = "AWAIT_CARD";
    }
}

async function moveCustomerOrder(id) {
    nosql.select('fetchCustomerOrder', 'customer_orders').make(function (builder) {
        builder.where('_id', id);
        builder.first();
    });
    var customerOrder = await nosql.promise('fetchCustomerOrder');
    if (customerOrder == null) {
        return;
    }
    customerOrder.temp_id = customerOrder._id;
    delete customerOrder._id;
    nosql.insert('reportSave', 'customer_orders_report').make(function (builder) {
        builder.set(customerOrder);
    });
    nosql.remove('removeTempOrder', 'customer_orders').make(function (builder) {
        builder.where('_id', customerOrder.temp_id);
    });
    await nosql.promise('reportSave');
    await nosql.promise('removeTempOrder');
    sendCommandToPrinter(customerOrder.serial_id);
}

function sendCommandToPrinter(id) {

    var options = {
        'method': 'POST',
        'url': 'http://10.198.1.5:3000/api/customer/order-bill?secure=1',
        'headers': {
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwNDIxMGE2OGY5MDU4NGFhOGQ5YzgxYiIsImlhdCI6MTYxNjQ5NTYwNiwiZXhwIjoxNjE5MDg3NjA2fQ.OfvGCT0gPCfJWA9HjhnzXS_A-heVgJEaEDTj5Z4VvWo',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ "id": id })

    };
    request(options, function (error, response) {
        if (error) throw new Error(error);
        console.log(response.body);
        setTimeout(function () {
            var options = {
                'method': 'GET',
                'url': 'http://10.198.1.5:3000/api/customer/print-order?secure=1',
                'headers': {
                    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwNDIxMGE2OGY5MDU4NGFhOGQ5YzgxYiIsImlhdCI6MTYxNjQ5NTYwNiwiZXhwIjoxNjE5MDg3NjA2fQ.OfvGCT0gPCfJWA9HjhnzXS_A-heVgJEaEDTj5Z4VvWo'
                }
            };

            request(options, function (error, response) {
                if (error) throw new Error(error);
                console.log(response.body);
            });
        }, 10000);

    });

}

function sendCommandToScale(command) {
    console.log(`WRITE : ${command}${String.fromCharCode(3)}${String.fromCharCode(13)}`);
    if (!DEBUG) {
        port.write(Buffer.from(command + String.fromCharCode(3) + String.fromCharCode(13), function (err) { }));
    }
}