
const SerialPort = require('serialport');
const Delimiter = require('@serialport/parser-delimiter');
const Readline = require('@serialport/parser-readline');
var logger = require('logger').createLogger('development.log');
const Ready = require('@serialport/parser-ready');
const args = require("args-parser")(process.argv)
console.info(args);
var request = require('request');

//return;
// node .\comm.js --bay=2 --comm=COM2 --value=0
// mongodb
var mongo_url = 'mongodb://localhost:27017/online-weighbridge'
//var mongo_url = 'mongodb+srv://admin:VsKl9x8yEWIqllxh@cluster0-m73oz.mongodb.net/online-bridge-comm?retryWrites=true&w=majority'
var Agent = require('sqlagent/mongodb').connect(mongo_url);
var nosql = new Agent();
var action = "";
action = "AWAIT_CARD";

var chunk = "";
var DEBUG = false;
var port;
var OLDCARD = "";
// COMM BAY 100 / 50 / 100 / 250 
var com_port = "";
if (!DEBUG) {
    // COMM port configure from database 
    var nosql = new Agent();
    nosql.select('docs', 'configuration').make(function (builder) {
        builder.where('configurationName', 'comm_config');
        builder.first();
    });

    var docs = await nosql.promise('docs');

    var configuration = docs.configurationDetails;
    configuration.map(comm => {
        if (comm.name == "offline-weighbridge") {
            com_port = comm.commm;
        }
    })
    console.log("comm--------", com_port)
    port = new SerialPort(com_port, { baudRate: 9600 });
    port.on('open', function () {
        port.on('readable', function () {
            port.read();
        })
    })
    // Setting there delimiter Char(30) (RS - record separator) 
    // to spit each data set from the serial port  
    const parserD = port.pipe(new Delimiter({ delimiter: String.fromCharCode(30) }));
    // reciving each data set 
    parserD.on('data', async function (data) {
        chunk = data.toString();
        DataProcessingOffline(chunk);
    });

    // Communication error in comm port 
    port.on('error', function (err) {
        console.log('Error: ', err.message)
        // if active retry after 5 sec
    });
}

// Processing each data set 
async function DataProcessingOffline(data) {

    var singleData;
    // if data contains Char(29) - GS  (group separator)
    // will be removed for next processing
    if (data.indexOf(String.fromCharCode(29))) {
        singleData = data.substring(data.indexOf(String.fromCharCode(29)));
    } else {
        singleData = data;
    }

    /**
     * Some data set doesn't contain ',' which needs to be ignored.
     *  sample data set
     * 0.00
     * ,,0.00
     * I,9564,34567
     */
    if (singleData.indexOf(",") != -1) {

        var splitData = singleData.split(",");
        var firstFieldLength = splitData[1].length;
        var CARD_NO = singleData[1];

        if (firstFieldLength > 2 && CARD_NO != OLDCARD) {
            nosql.select('customerOrder', 'customer_orders').make(function (builder) {
                builder.where('card_num', splitData[1]);
                builder.sort("creationDate", 'desc')
                builder.first();
            })
            var customerOrder = await nosql.promise('customerOrder');

            /**
             * Offline Wt 
             * if card not avaible in inprocessing check offline table 
             */
            if (customerOrder == null) {
                // Check the offline weight and apply 
                //return;
                nosql.select('getOffline', 'offlineWeighBridge').make(function (builder) {
                    builder.where('card_num', splitData[1]);
                    builder.and();
                    builder.where('status', true);
                    builder.first();
                })

                var getOffline = await nosql.promise('getOffline');
                if (getOffline != null) {
                    nosql.update('updateOffline', 'offlineWeighBridge').make(function (builder) {
                        builder.where('card_num', splitData[1]);
                        builder.set('status', false);
                        builder.set('offline_wt', parseInt(splitData[2]));
                    })
                    var updateOffline = await nosql.promise('updateOffline');
                }


            } else {
                /**
                             * Offline Gross Wt
                             * if card punched in gantry && loading completed 
                             */
                if (customerOrder.action == "LOADING_COMPLETED") {
                    nosql.update('updateOfflineGrossData', 'customer_orders').make(function (builder) {
                        builder.set('offline_gross', parseInt(singleData[2]));
                        builder.set('offline_gross_time', new Date());
                        builder.where('card_num', CARD_NO);
                    })
                    var updateOfflineGrossData = await nosql.promise('updateOfflineGrossData');
                    
                    // deviation find out 
                    nosql.select('getOffline', 'offlineWeighBridge').make(function (builder) {
                        builder.where('card_num', splitData[1]);
                        builder.first();
                    })
                    var getOffline = await nosql.promise('getOffline');
                    var deviation = getOffline.gross_wt - updateOfflineGrossData.gross_wt;

                    var nosql = new Agent();
                    nosql.select('docs', 'configuration').make(function (builder) {
                        builder.where('configurationName', 'global_config');
                        builder.first();
                    });
                
                    var docs = await nosql.promise('docs');
                    var deviation_global = 0;
                    var configuration = docs.configurationDetails;
                    configuration.map(comm => {
                        if (comm.name == "deviation") {
                            deviation_global = comm.value;
                        }
                    })
                    if (deviation > deviation_global) {
                        nosql.update('updateOfflineGrossData', 'customer_orders').make(function (builder) {
                            builder.set('offline_gross', parseInt(singleData[2]));
                            builder.set('offline_gross_time', new Date());
                            builder.set('deviated', true);
                            builder.set('deviated_value', deviation)
                            builder.where('card_num', CARD_NO);
                        })
                    } else {
                        nosql.update('updateOfflineGrossData', 'customer_orders').make(function (builder) {
                            builder.set('offline_gross', parseInt(singleData[2]));
                            builder.set('offline_gross_time', new Date());
                            builder.set('deviated', false);
                            builder.where('card_num', CARD_NO);
                        }) 
                    }
                    
                    var updateOfflineGrossData = await nosql.promise('updateOfflineGrossData');
                    moveCustomerOrder(customerOrder._id)


                    //return;
                }

                /**
                 * Offline Tare Wt
                 * if card not punched in gantry  
                 */
                if (customerOrder.action == null) {

                    nosql.update('updateOfflineTareData', 'customer_orders').make(function (builder) {
                        builder.set('offline_tare', parseInt(singleData[2]));
                        builder.set('offline_tare_time', new Date());
                        builder.where('card_num', CARD_NO);
                    })
                    await nosql.promise('updateOfflineTareData');
                    //return;
                }

            }




        }
        OLDCARD = CARD_NO;
    }
}

async function moveCustomerOrder(id) {
    nosql.select('fetchCustomerOrder', 'customer_orders').make(function (builder) {
        builder.where('_id', id);
        builder.first();
    });
    var customerOrder = await nosql.promise('fetchCustomerOrder');
    if (customerOrder == null) {
        return;
    }
    customerOrder.temp_id = customerOrder._id;
    delete customerOrder._id;
    nosql.insert('reportSave', 'customer_orders_report').make(function (builder) {
        builder.set(customerOrder);
    });
    nosql.remove('removeTempOrder', 'customer_orders').make(function (builder) {
        builder.where('_id', customerOrder.temp_id);
    });
    await nosql.promise('reportSave');
    await nosql.promise('removeTempOrder');
    sendCommandToPrinter(customerOrder.serial_id);
}

function sendCommandToPrinter(id) {

    var options = {
        'method': 'POST',
        'url': 'http://10.198.1.5:3000/api/customer/order-bill?secure=1',
        'headers': {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ "id": id })

    };
    request(options, function (error, response) {
        if (error) throw new Error(error);
        console.log(response.body);
        setTimeout(function () {
            var options = {
                'method': 'GET',
                'url': 'http://10.198.1.5:3000/api/customer/print-order?secure=1',
                'headers': {
                }
            };

            request(options, function (error, response) {
                if (error) throw new Error(error);
                console.log(response.body);
            });
        }, 10000);

    });

}