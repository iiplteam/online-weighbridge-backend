(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"], {
    /***/
    "+AxC":
    /*!************************************************!*\
      !*** ./src/app/services/my-service.service.ts ***!
      \************************************************/

    /*! exports provided: MyServiceService */

    /***/
    function AxC(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MyServiceService", function () {
        return MyServiceService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var _sse_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./sse-service.service */
      "AB80");

      var MyServiceService = /*#__PURE__*/function () {
        function MyServiceService(_zone, _sseService) {
          _classCallCheck(this, MyServiceService);

          this._zone = _zone;
          this._sseService = _sseService;
        }

        _createClass(MyServiceService, [{
          key: "getServerSentEvent",
          value: function getServerSentEvent(url) {
            var _this = this;

            return rxjs__WEBPACK_IMPORTED_MODULE_1__["Observable"].create(function (observer) {
              var eventSource = _this._sseService.getEventSource(url);

              eventSource.onmessage = function (event) {
                _this._zone.run(function () {
                  observer.next(event);
                });
              };

              eventSource.onerror = function (error) {
                _this._zone.run(function () {
                  observer.error(error);
                });
              };
            });
          }
        }]);

        return MyServiceService;
      }();

      MyServiceService.ɵfac = function MyServiceService_Factory(t) {
        return new (t || MyServiceService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_sse_service_service__WEBPACK_IMPORTED_MODULE_2__["SseServiceService"]));
      };

      MyServiceService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: MyServiceService,
        factory: MyServiceService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MyServiceService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]
          }, {
            type: _sse_service_service__WEBPACK_IMPORTED_MODULE_2__["SseServiceService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "AB80":
    /*!*************************************************!*\
      !*** ./src/app/services/sse-service.service.ts ***!
      \*************************************************/

    /*! exports provided: SseServiceService */

    /***/
    function AB80(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SseServiceService", function () {
        return SseServiceService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var SseServiceService = /*#__PURE__*/function () {
        function SseServiceService() {
          _classCallCheck(this, SseServiceService);
        }

        _createClass(SseServiceService, [{
          key: "getEventSource",
          value: function getEventSource(url) {
            return new EventSource(url);
          }
        }]);

        return SseServiceService;
      }();

      SseServiceService.ɵfac = function SseServiceService_Factory(t) {
        return new (t || SseServiceService)();
      };

      SseServiceService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: SseServiceService,
        factory: SseServiceService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SseServiceService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    }
  }]);
})();
//# sourceMappingURL=common-es5.js.map