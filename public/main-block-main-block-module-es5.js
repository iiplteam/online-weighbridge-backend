(function () {
  function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

  function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

  function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

  function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

  function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

  function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

  function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

  function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

  function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

  function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

  function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

  function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main-block-main-block-module"], {
    /***/
    "43Sd":
    /*!*******************************************************************************!*\
      !*** ./src/app/content/pages/main-block/card-create/card-create.component.ts ***!
      \*******************************************************************************/

    /*! exports provided: CardCreateComponent */

    /***/
    function Sd(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CardCreateComponent", function () {
        return CardCreateComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! axios */
      "vDqi");
      /* harmony import */


      var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! sweetalert2 */
      "PSD3");
      /* harmony import */


      var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
      /* harmony import */


      var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ng-bootstrap/ng-bootstrap */
      "1kSV");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _app_services_base_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @app/services/base-service.service */
      "eWbo");
      /* harmony import */


      var angular_datatables__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! angular-datatables */
      "njyG");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");

      function CardCreateComponent_tbody_19_tr_1_span_6_Template(rf, ctx) {
        if (rf & 1) {
          var _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CardCreateComponent_tbody_19_tr_1_span_6_Template_span_click_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r13);

            var data_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

            var ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            var _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](25);

            return ctx_r11.changeActiveStatus(data_r7, 2, _r4);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Active");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function CardCreateComponent_tbody_19_tr_1_span_7_Template(rf, ctx) {
        if (rf & 1) {
          var _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CardCreateComponent_tbody_19_tr_1_span_7_Template_span_click_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r16);

            var data_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

            var ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            var _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](25);

            return ctx_r14.changeActiveStatus(data_r7, 2, _r4);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "InActive");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function CardCreateComponent_tbody_19_tr_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, CardCreateComponent_tbody_19_tr_1_span_6_Template, 2, 0, "span", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, CardCreateComponent_tbody_19_tr_1_span_7_Template, 2, 0, "span", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "td");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var data_r7 = ctx.$implicit;
          var i_r8 = ctx.index;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](i_r8 + 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](data_r7.number);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", data_r7.isActive);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !data_r7.isActive);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](data_r7.remarks);
        }
      }

      function CardCreateComponent_tbody_19_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tbody", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, CardCreateComponent_tbody_19_tr_1_Template, 10, 5, "tr", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r0.cardData);
        }
      }

      function CardCreateComponent_tbody_20_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tbody");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "td", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "No data!");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function CardCreateComponent_ng_template_22_div_16_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Card Number is required. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function CardCreateComponent_ng_template_22_div_16_div_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Card No is 4 digit. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function CardCreateComponent_ng_template_22_div_16_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, CardCreateComponent_ng_template_22_div_16_div_1_Template, 2, 0, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, CardCreateComponent_ng_template_22_div_16_div_2_Template, 2, 0, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r19.errors.required);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r19.errors.pattern);
        }
      }

      function CardCreateComponent_ng_template_22_Template(rf, ctx) {
        if (rf & 1) {
          var _r25 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "form", 18, 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h4", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "New Card");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CardCreateComponent_ng_template_22_Template_button_click_5_listener() {
            var modal_r17 = ctx.$implicit;
            return modal_r17.dismiss("Cross click");
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "span", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "\xD7");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "label", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Card Number");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "input", 29, 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CardCreateComponent_ng_template_22_Template_input_ngModelChange_14_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r25);

            var ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r24.selectObj.number = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, CardCreateComponent_ng_template_22_div_16_Template, 3, 2, "div", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "button", 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CardCreateComponent_ng_template_22_Template_button_click_18_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r25);

            var modal_r17 = ctx.$implicit;

            var _r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](1);

            modal_r17.close();
            return _r18.reset();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Add");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var _r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](1);

          var _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](15);

          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r3.selectObj.number);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r19.invalid && (_r19.dirty || _r19.touched));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !_r18.valid);
        }
      }

      function CardCreateComponent_ng_template_24_div_24_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Remarks is required");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function CardCreateComponent_ng_template_24_div_24_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, CardCreateComponent_ng_template_24_div_24_div_1_Template, 2, 0, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r30 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r30.errors.required);
        }
      }

      function CardCreateComponent_ng_template_24_Template(rf, ctx) {
        if (rf & 1) {
          var _r35 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "form", 18, 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h4", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Status InActive");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CardCreateComponent_ng_template_24_Template_button_click_5_listener() {
            var modal_r27 = ctx.$implicit;
            return modal_r27.dismiss("Cross click");
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "span", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "\xD7");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "label", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Card Number");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "input", 36, 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CardCreateComponent_ng_template_24_Template_input_ngModelChange_14_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r35);

            var ctx_r34 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r34.selectObj.number = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "label", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Remarks");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "span", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "* ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "input", 38, 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CardCreateComponent_ng_template_24_Template_input_ngModelChange_22_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r35);

            var ctx_r36 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r36.selectObj.remarks = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](24, CardCreateComponent_ng_template_24_div_24_Template, 2, 1, "div", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "button", 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CardCreateComponent_ng_template_24_Template_button_click_26_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r35);

            var modal_r27 = ctx.$implicit;

            var _r28 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](1);

            modal_r27.close();
            return _r28.reset();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Confirm");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var _r28 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](1);

          var _r30 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](23);

          var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r5.selectObj.number);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r5.selectObj.remarks);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r30.invalid && (_r30.dirty || _r30.touched));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !_r28.valid);
        }
      }

      var DataTablesResponse = function DataTablesResponse() {
        _classCallCheck(this, DataTablesResponse);
      };

      var CardCreateComponent = /*#__PURE__*/function () {
        function CardCreateComponent(modalService, router, baseService) {
          _classCallCheck(this, CardCreateComponent);

          this.modalService = modalService;
          this.router = router;
          this.baseService = baseService;
          this.dtOptions = {};
          this.cardData = [];
          this.tableStatus = true; // We use this trigger because fetching the list of persons can be quite long,
          // thus we ensure the data is fetched before rendering

          this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
          this.selectObj = {};
          this.getCardDetails();
        } //Model


        _createClass(CardCreateComponent, [{
          key: "addNewRole",
          value: function addNewRole(content) {
            var _this = this;

            var self = this;
            self.selectObj.status = 'notinuse', self.selectObj.isActive = true;
            self.modalService.open(content, {
              ariaLabelledBy: 'modal-basic-title',
              keyboard: false,
              backdrop: 'static',
              centered: true
            }).result.then(function (result) {
              console.log(self.selectObj);
              axios__WEBPACK_IMPORTED_MODULE_1___default.a.post(_this.baseService.baseUrl + 'api/card/save-card', self.selectObj, {
                headers: {
                  "Authorization": "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
                }
              }).then(function (response) {
                if (response.data.status) {
                  _this.getCardDetails();

                  sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                    position: 'center',
                    icon: 'success',
                    title: response.data["message"],
                    showConfirmButton: false,
                    timer: 3000
                  }).then(function (result) {});
                }
              })["catch"](function (error) {
                console.log(error);
              }); // });
            }, function (reason) {// self.getNotificationsList();
            });
          }
        }, {
          key: "changeActiveStatus",
          value: function changeActiveStatus(data, no, content) {
            var _this2 = this;

            var self = this;
            self.selectObj = data;
            console.log(no);

            if (no == 1) {
              data.status = data.status == "inuse" ? "notinuse" : "inuse";
              console.log('Final status', data.status);
            } else {
              data.isActive = !data.isActive;
            }

            if (!data.isActive) {
              self.modalService.open(content, {
                ariaLabelledBy: 'modal-basic-title',
                keyboard: false,
                backdrop: 'static',
                centered: true
              }).result.then(function (result) {
                console.log(self.selectObj);
                axios__WEBPACK_IMPORTED_MODULE_1___default.a.post(_this2.baseService.baseUrl + 'api/card/save-card', self.selectObj, {
                  headers: {
                    "Authorization": "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
                  }
                }).then(function (response) {
                  if (response.data.status) {
                    _this2.getCardDetails();

                    sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                      position: 'center',
                      icon: 'success',
                      title: response.data["message"],
                      showConfirmButton: false,
                      timer: 3000
                    }).then(function (result) {});
                    _this2.selectObj = {};
                  }
                })["catch"](function (error) {
                  console.log(error);
                }); // });
              }, function (reason) {// self.getNotificationsList();
              });
            } else {
              console.log(data);
              self.selectObj.remarks = "-";
              axios__WEBPACK_IMPORTED_MODULE_1___default.a.post(this.baseService.baseUrl + 'api/card/save-card', self.selectObj, {
                headers: {
                  "Authorization": "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
                }
              }).then(function (response) {
                if (response.data.status) {
                  _this2.getCardDetails();

                  sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                    position: 'center',
                    icon: 'success',
                    title: response.data["message"],
                    showConfirmButton: false,
                    timer: 3000
                  }).then(function (result) {});
                  _this2.selectObj = {};
                }
              })["catch"](function (error) {
                console.log(error);
              });
            }
          }
        }, {
          key: "getCardDetails",
          value: function getCardDetails() {
            var _this3 = this;

            axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.baseService.baseUrl + 'api/card/get-cards', {
              headers: {
                'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
              }
            }).then(function (response) {
              console.log(response);
              _this3.cardData = response.data.data;

              if (_this3.tableStatus) {
                _this3.dtTrigger.next();

                _this3.tableStatus = false;
              }
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            this.dtOptions = {
              pagingType: 'full_numbers',
              pageLength: 10
            };
          }
        }]);

        return CardCreateComponent;
      }();

      CardCreateComponent.ɵfac = function CardCreateComponent_Factory(t) {
        return new (t || CardCreateComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_app_services_base_service_service__WEBPACK_IMPORTED_MODULE_6__["BaseServiceService"]));
      };

      CardCreateComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: CardCreateComponent,
        selectors: [["app-card-create"]],
        decls: 26,
        vars: 4,
        consts: [[1, "container"], [1, "row", "mt-3"], [1, "col-md-9"], [1, "float-right", "mb-3"], [1, "btn", "btn-primary", 3, "click"], ["datatable", "", 1, "row-border", "hover", 3, "dtOptions", "dtTrigger"], [1, "header"], ["style", "text-align: center;", 4, "ngIf"], [4, "ngIf"], ["remarksContent", ""], ["remarkContent", ""], [2, "text-align", "center"], [4, "ngFor", "ngForOf"], ["style", "background-color:green; color:#fff;padding:.1rem .5rem;border-radius: 5px;cursor: pointer;", 3, "click", 4, "ngIf"], ["style", "background-color:red; color:#fff;padding:.1rem .5rem;border-radius: 5px;cursor: pointer;", 3, "click", 4, "ngIf"], [2, "background-color", "green", "color", "#fff", "padding", ".1rem .5rem", "border-radius", "5px", "cursor", "pointer", 3, "click"], [2, "background-color", "red", "color", "#fff", "padding", ".1rem .5rem", "border-radius", "5px", "cursor", "pointer", 3, "click"], ["colspan", "3", 1, "no-data-available"], ["novalidate", ""], ["lpForm", "ngForm"], [1, "modal-header"], ["id", "modal-basic-title", 1, "modal-title"], ["type", "button", "aria-label", "Close", 1, "close", 3, "click"], ["aria-hidden", "true"], [1, "modal-body"], [1, "row"], [1, "form-group", "col-12"], ["for", "dateOfBirth"], [1, "input-group"], ["type", "text", "id", "normal-input-5", "placeholder", "Enter Card Number", "name", "number", "required", "", "pattern", "\\d{4}", "maxlength", "4", "minlength", "4", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["number", "ngModel"], ["class", "form-text text-danger", 4, "ngIf"], [1, "modal-footer"], ["type", "button", 1, "btn", "btn-outline-danger", 3, "disabled", "click"], [1, "form-text", "text-danger"], [1, "form-group", "col-6"], ["type", "input", "name", "number", "readonly", "", 1, "form-control", 3, "ngModel", "ngModelChange"], [2, "color", "red"], ["type", "input", "name", "remarks", "required", "", 1, "form-control", 3, "ngModel", "ngModelChange"], ["remarks", "ngModel"]],
        template: function CardCreateComponent_Template(rf, ctx) {
          if (rf & 1) {
            var _r38 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h3");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Card Create");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CardCreateComponent_Template_button_click_6_listener() {
              _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r38);

              var _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](23);

              return ctx.addNewRole(_r2);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "+ card");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "table", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "thead");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "tr", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "th");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "S No");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "th");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Card Number");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "th");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Status");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "th");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Remark");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](19, CardCreateComponent_tbody_19_Template, 2, 1, "tbody", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, CardCreateComponent_tbody_20_Template, 4, 0, "tbody", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "tbody");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](22, CardCreateComponent_ng_template_22_Template, 20, 3, "ng-template", null, 9, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](24, CardCreateComponent_ng_template_24_Template, 28, 4, "ng-template", null, 10, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("dtOptions", ctx.dtOptions)("dtTrigger", ctx.dtTrigger);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.cardData == null ? null : ctx.cardData.length) != 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.cardData == null ? null : ctx.cardData.length) == 0);
          }
        },
        directives: [angular_datatables__WEBPACK_IMPORTED_MODULE_7__["DataTableDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["PatternValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["MaxLengthValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["MinLengthValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["NgModel"]],
        styles: [".header[_ngcontent-%COMP%] {\n  text-align: center;\n  margin: 20px;\n  padding-top: 10px;\n  font-weight: 500;\n  padding-bottom: 10px;\n}\n\n.del-list[_ngcontent-%COMP%] {\n  background-color: red;\n  padding: 0 1rem;\n  border-radius: 4px;\n  color: #fff;\n  margin-left: 1rem;\n  display: flex;\n  align-items: center;\n  font-size: 20px;\n  cursor: pointer;\n}\n\n.login-container[_ngcontent-%COMP%] {\n  font-family: \"Source Sans Pro\";\n}\n\n.login-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%] {\n  background-image: url(/assets/images/group_4.png), linear-gradient(-136.82353093deg, #242348 0%, #5a55aa 100%);\n  background-size: cover;\n  min-height: 100vh;\n}\n\n.login-container[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%], .login-container[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #43425d;\n}\n\n.login-container[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%] {\n  font-weight: bold;\n  letter-spacing: 5px;\n}\n\n.login-container[_ngcontent-%COMP%]   .tag[_ngcontent-%COMP%] {\n  color: rgba(77, 79, 92, 0.5);\n}\n\n.login-container[_ngcontent-%COMP%]   .btn-main[_ngcontent-%COMP%] {\n  background: #43425d;\n}\n\n.login-container[_ngcontent-%COMP%]   .btn-main-outline[_ngcontent-%COMP%] {\n  border: 1px solid #43425d;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFwuLlxcLi5cXGNhcmQtY3JlYXRlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBO0VBRUUsa0JBQWtCO0VBRWxCLFlBQVk7RUFFWixpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtBQUx0Qjs7QUFPQTtFQUNJLHFCQUFxQjtFQUNyQixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxpQkFBaUI7RUFDakIsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YsZUFBZTtBQUpuQjs7QUFPQTtFQUNJLDhCQUE4QjtBQUpsQzs7QUFHQTtFQUdRLDhHQUM0RjtFQUM1RixzQkFBc0I7RUFDdEIsaUJBQWlCO0FBSHpCOztBQUhBO0VBVVEsY0FsQ1k7QUErQnBCOztBQVBBO0VBY1EsaUJBQWlCO0VBQ2pCLG1CQUFtQjtBQUgzQjs7QUFaQTtFQW1CUSw0QkExQ3NCO0FBdUM5Qjs7QUFoQkE7RUF1QlEsbUJBaEQwQjtBQTZDbEM7O0FBcEJBO0VBMkJRLHlCQXBEMEI7QUFpRGxDIiwiZmlsZSI6ImNhcmQtY3JlYXRlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiJGNvbG9yLXByaW1hcnk6IHJnYmEoNjcsNjYsOTMsMjU1KTtcclxuJGNvbG9yLXRleHQ6ICM0MzQyNWQ7XHJcbiRjb2xvci10ZXh0LWxpdGU6IHJnYmEoIzRkNGY1YywuNSk7XHJcbi5oZWFkZXJcclxue1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuLy8gICBiYWNrZ3JvdW5kLWNvbG9yOiM1YThlZDI7XHJcbiAgbWFyZ2luOiAyMHB4O1xyXG4vLyAgIGNvbG9yOiB3aGl0ZTtcclxuICBwYWRkaW5nLXRvcDogMTBweDtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG59XHJcbi5kZWwtbGlzdCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XHJcbiAgICBwYWRkaW5nOiAwIDFyZW07XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIG1hcmdpbi1sZWZ0OiAxcmVtO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi5sb2dpbi1jb250YWluZXIge1xyXG4gICAgZm9udC1mYW1pbHk6IFwiU291cmNlIFNhbnMgUHJvXCI7XHJcbiAgICAuc2lkZWJhciB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC9hc3NldHMvaW1hZ2VzL2dyb3VwXzQucG5nKSwgXHJcbiAgICAgICAgICAgICAgbGluZWFyLWdyYWRpZW50KC0xMzYuODIzNTMwOTMyMDNkZWcgLCByZ2JhKDM2LDM1LDcyLDI1NSkgMCUsIHJnYmEoOTAsODUsMTcwLDI1NSkgMTAwJSk7XHJcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgICAgICBtaW4taGVpZ2h0OiAxMDB2aDtcclxuICAgIH1cclxuXHJcbiAgICAudGl0bGUsIGEge1xyXG4gICAgICAgIGNvbG9yOiAkY29sb3ItdGV4dDtcclxuICAgIH1cclxuXHJcbiAgICAudGl0bGUge1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGxldHRlci1zcGFjaW5nOiA1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLnRhZyB7XHJcbiAgICAgICAgY29sb3I6ICRjb2xvci10ZXh0LWxpdGVcclxuICAgIH1cclxuXHJcbiAgICAuYnRuLW1haW4ge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICRjb2xvci1wcmltYXJ5O1xyXG4gICAgfVxyXG5cclxuICAgIC5idG4tbWFpbi1vdXRsaW5lIHtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAkY29sb3ItcHJpbWFyeVxyXG4gICAgfVxyXG5cclxufSJdfQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CardCreateComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-card-create',
            templateUrl: './card-create.component.html',
            styleUrls: ['./card-create.component.scss']
          }]
        }], function () {
          return [{
            type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"]
          }, {
            type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
          }, {
            type: _app_services_base_service_service__WEBPACK_IMPORTED_MODULE_6__["BaseServiceService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "GWr9":
    /*!*************************************************************************************!*\
      !*** ./src/app/content/pages/main-block/product-create/product-create.component.ts ***!
      \*************************************************************************************/

    /*! exports provided: ProductCreateComponent */

    /***/
    function GWr9(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductCreateComponent", function () {
        return ProductCreateComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! axios */
      "vDqi");
      /* harmony import */


      var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! sweetalert2 */
      "PSD3");
      /* harmony import */


      var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _app_services_base_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @app/services/base-service.service */
      "eWbo");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      function ProductCreateComponent_div_11_div_9_Template(rf, ctx) {
        if (rf & 1) {
          var _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ProductCreateComponent_div_11_div_9_Template_div_click_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7);

            var i_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().index;

            var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r5.removeAddress(i_r3);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "-");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function ProductCreateComponent_div_11_Template(rf, ctx) {
        if (rf & 1) {
          var _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "section", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "label", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Product");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "input", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function ProductCreateComponent_div_11_Template_input_ngModelChange_7_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9);

            var obj_r2 = ctx.$implicit;
            return obj_r2.prodName = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "input", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function ProductCreateComponent_div_11_Template_input_ngModelChange_8_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9);

            var obj_r2 = ctx.$implicit;
            return obj_r2.isActive = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, ProductCreateComponent_div_11_div_9_Template, 2, 0, "div", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var obj_r2 = ctx.$implicit;

          var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("name", "address_", obj_r2.id, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", obj_r2.prodName);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("name", "check_", obj_r2.id, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", obj_r2.isActive);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.productList.length > 1);
        }
      }

      var ProductCreateComponent = /*#__PURE__*/function () {
        function ProductCreateComponent(router, baseService) {
          _classCallCheck(this, ProductCreateComponent);

          this.router = router;
          this.baseService = baseService;
          this.dtOptions = {};
          this.customerOrderData = []; // We use this trigger because fetching the list of persons can be quite long,
          // thus we ensure the data is fetched before rendering

          this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
          this.productList = [{
            id: 1,
            prodName: '',
            isActive: true
          }];
        }

        _createClass(ProductCreateComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.getUsers();
          }
        }, {
          key: "addAddress",
          value: function addAddress() {
            this.productList.push({
              id: this.productList.length + 1,
              prodName: '',
              isActive: true
            });
          }
        }, {
          key: "removeAddress",
          value: function removeAddress(i) {
            this.productList.splice(i, 1);
          }
        }, {
          key: "logValue",
          value: function logValue() {
            console.log(this.productList);

            for (var i = 0; i < this.productList.length; i++) {
              var element = this.productList[i];

              if (element.prodName == '') {
                console.log("Eer");
              }
            }

            var tempObj = {
              configName: "products",
              configData: this.productList
            }; ///api/configUsers/save-config

            axios__WEBPACK_IMPORTED_MODULE_1___default.a.put(this.baseService.baseUrl + 'api/configUsers/' + this.productId, tempObj, {
              // headers: {
              //   "Authorization": `Bearer ${this.authS.userValue.jwtToken}`
              // }
              headers: {
                'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
              }
            }).then(function (response) {
              console.log(response);

              if (response.data.status) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                  position: 'center',
                  icon: 'success',
                  title: "Product List Updated Successfully",
                  showConfirmButton: false,
                  timer: 3000
                }).then(function () {
                  location.reload();
                });
              } else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                  position: 'center',
                  icon: 'error',
                  title: "Adding Failed..",
                  showConfirmButton: false,
                  timer: 3000
                });
              }
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }, {
          key: "getUsers",
          value: function getUsers() {
            var _this4 = this;

            axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.baseService.baseUrl + 'api/configUsers', {
              headers: {
                'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
              }
            }).then(function (response) {
              console.log(response);
              _this4.productId = response.data.data[1]._id;
              _this4.productList = response.data.data[1].configData;

              _this4.dtTrigger.next();
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }]);

        return ProductCreateComponent;
      }();

      ProductCreateComponent.ɵfac = function ProductCreateComponent_Factory(t) {
        return new (t || ProductCreateComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_app_services_base_service_service__WEBPACK_IMPORTED_MODULE_5__["BaseServiceService"]));
      };

      ProductCreateComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: ProductCreateComponent,
        selectors: [["app-product-create"]],
        decls: 15,
        vars: 1,
        consts: [[1, "row"], [1, "col-md-12"], [1, "header"], [1, "row", "m-0"], [1, "col-12", 2, "text-align", "right"], ["type", "button", 1, "btn", "btn-primary", "mb-2", "p-1", 3, "click"], [1, "container", "mt-3", 3, "ngSubmit"], ["addressForm", "ngForm"], ["class", "col-6", 4, "ngFor", "ngForOf"], [1, "col-3"], ["type", "submit", "value", "Submit", 1, "btn", "btn-primary", 2, "padding", "5px"], [1, "col-6"], [1, "p-1"], ["for", "normal-input-1", 1, "col-md-4", "col-sm-5", "col-form-label", "col-form-label-sm", "text-sm-right"], [1, "col-md-8", "col-sm-7", "mb-1"], [1, "form-group", "d-flex"], ["type", "text", "placeholder", "Enter name", 1, "form-control", "form-control-sm", 3, "ngModel", "name", "ngModelChange"], ["type", "checkbox", 3, "ngModel", "name", "ngModelChange"], ["class", "del-list", 3, "click", 4, "ngIf"], [1, "del-list", 3, "click"]],
        template: function ProductCreateComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Product Master");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ProductCreateComponent_Template_button_click_6_listener() {
              return ctx.addAddress();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Add Product");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "form", 6, 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function ProductCreateComponent_Template_form_ngSubmit_8_listener() {
              return ctx.logValue();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, ProductCreateComponent_div_11_Template, 10, 5, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "input", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.productList);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgForm"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["CheckboxControlValueAccessor"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"]],
        styles: [".header[_ngcontent-%COMP%] {\n  text-align: center;\n  margin: 20px;\n  padding-top: 10px;\n  font-weight: 500;\n  padding-bottom: 10px;\n}\n\n.del-list[_ngcontent-%COMP%] {\n  background-color: red;\n  padding: 0 1rem;\n  border-radius: 4px;\n  color: #fff;\n  margin-left: 1rem;\n  display: flex;\n  align-items: center;\n  font-size: 20px;\n  cursor: pointer;\n}\n\n.login-container[_ngcontent-%COMP%] {\n  font-family: \"Source Sans Pro\";\n}\n\n.login-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%] {\n  background-image: url(/assets/images/group_4.png), linear-gradient(-136.82353093deg, #242348 0%, #5a55aa 100%);\n  background-size: cover;\n  min-height: 100vh;\n}\n\n.login-container[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%], .login-container[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #43425d;\n}\n\n.login-container[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%] {\n  font-weight: bold;\n  letter-spacing: 5px;\n}\n\n.login-container[_ngcontent-%COMP%]   .tag[_ngcontent-%COMP%] {\n  color: rgba(77, 79, 92, 0.5);\n}\n\n.login-container[_ngcontent-%COMP%]   .btn-main[_ngcontent-%COMP%] {\n  background: #43425d;\n}\n\n.login-container[_ngcontent-%COMP%]   .btn-main-outline[_ngcontent-%COMP%] {\n  border: 1px solid #43425d;\n}\n\ninput[type=\"radio\"][_ngcontent-%COMP%], input[type=\"checkbox\"][_ngcontent-%COMP%] {\n  box-sizing: border-box;\n  padding: 6px;\n  margin: 10px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFwuLlxcLi5cXHByb2R1Y3QtY3JlYXRlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBO0VBRUUsa0JBQWtCO0VBRWxCLFlBQVk7RUFFWixpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtBQUx0Qjs7QUFPQTtFQUNJLHFCQUFxQjtFQUNyQixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxpQkFBaUI7RUFDakIsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YsZUFBZTtBQUpuQjs7QUFPQTtFQUNJLDhCQUE4QjtBQUpsQzs7QUFHQTtFQUdRLDhHQUM0RjtFQUM1RixzQkFBc0I7RUFDdEIsaUJBQWlCO0FBSHpCOztBQUhBO0VBVVEsY0FsQ1k7QUErQnBCOztBQVBBO0VBY1EsaUJBQWlCO0VBQ2pCLG1CQUFtQjtBQUgzQjs7QUFaQTtFQW1CUSw0QkExQ3NCO0FBdUM5Qjs7QUFoQkE7RUF1QlEsbUJBaEQwQjtBQTZDbEM7O0FBcEJBO0VBMkJRLHlCQXBEMEI7QUFpRGxDOztBQVNBO0VBQ0ksc0JBQXNCO0VBQ3RCLFlBQVk7RUFDWix1QkFBc0I7QUFOMUIiLCJmaWxlIjoicHJvZHVjdC1jcmVhdGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIkY29sb3ItcHJpbWFyeTogcmdiYSg2Nyw2Niw5MywyNTUpO1xyXG4kY29sb3ItdGV4dDogIzQzNDI1ZDtcclxuJGNvbG9yLXRleHQtbGl0ZTogcmdiYSgjNGQ0ZjVjLC41KTtcclxuLmhlYWRlclxyXG57XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4vLyAgIGJhY2tncm91bmQtY29sb3I6I2VjOTEyMTtcclxuICBtYXJnaW46IDIwcHg7XHJcbi8vICAgY29sb3I6IHdoaXRlO1xyXG4gIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbn1cclxuLmRlbC1saXN0IHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJlZDtcclxuICAgIHBhZGRpbmc6IDAgMXJlbTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDFyZW07XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLmxvZ2luLWNvbnRhaW5lciB7XHJcbiAgICBmb250LWZhbWlseTogXCJTb3VyY2UgU2FucyBQcm9cIjtcclxuICAgIC5zaWRlYmFyIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoL2Fzc2V0cy9pbWFnZXMvZ3JvdXBfNC5wbmcpLCBcclxuICAgICAgICAgICAgICBsaW5lYXItZ3JhZGllbnQoLTEzNi44MjM1MzA5MzIwM2RlZyAsIHJnYmEoMzYsMzUsNzIsMjU1KSAwJSwgcmdiYSg5MCw4NSwxNzAsMjU1KSAxMDAlKTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICAgIG1pbi1oZWlnaHQ6IDEwMHZoO1xyXG4gICAgfVxyXG5cclxuICAgIC50aXRsZSwgYSB7XHJcbiAgICAgICAgY29sb3I6ICRjb2xvci10ZXh0O1xyXG4gICAgfVxyXG5cclxuICAgIC50aXRsZSB7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDVweDtcclxuICAgIH1cclxuXHJcbiAgICAudGFnIHtcclxuICAgICAgICBjb2xvcjogJGNvbG9yLXRleHQtbGl0ZVxyXG4gICAgfVxyXG5cclxuICAgIC5idG4tbWFpbiB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogJGNvbG9yLXByaW1hcnk7XHJcbiAgICB9XHJcblxyXG4gICAgLmJ0bi1tYWluLW91dGxpbmUge1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICRjb2xvci1wcmltYXJ5XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5cclxuaW5wdXRbdHlwZT1cInJhZGlvXCJdLCBpbnB1dFt0eXBlPVwiY2hlY2tib3hcIl0ge1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgIHBhZGRpbmc6IDZweDtcclxuICAgIG1hcmdpbjogMTBweCFpbXBvcnRhbnQ7XHJcbn0iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ProductCreateComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-product-create',
            templateUrl: './product-create.component.html',
            styleUrls: ['./product-create.component.scss']
          }]
        }], function () {
          return [{
            type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
          }, {
            type: _app_services_base_service_service__WEBPACK_IMPORTED_MODULE_5__["BaseServiceService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "HBhd":
    /*!***************************************************************************************!*\
      !*** ./src/app/content/pages/main-block/customer-create/customer-create.component.ts ***!
      \***************************************************************************************/

    /*! exports provided: CustomerCreateComponent */

    /***/
    function HBhd(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CustomerCreateComponent", function () {
        return CustomerCreateComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! axios */
      "vDqi");
      /* harmony import */


      var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
      /* harmony import */


      var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! sweetalert2 */
      "PSD3");
      /* harmony import */


      var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _app_services_base_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @app/services/base-service.service */
      "eWbo");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      function CustomerCreateComponent_div_14_div_7_Template(rf, ctx) {
        if (rf & 1) {
          var _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CustomerCreateComponent_div_14_div_7_Template_div_click_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7);

            var i_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().index;

            var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r5.removeAddress(i_r3);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "-");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function CustomerCreateComponent_div_14_Template(rf, ctx) {
        if (rf & 1) {
          var _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "label", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Customer");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "input", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CustomerCreateComponent_div_14_Template_input_ngModelChange_5_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9);

            var obj_r2 = ctx.$implicit;
            return obj_r2.custName = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "input", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CustomerCreateComponent_div_14_Template_input_ngModelChange_6_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9);

            var obj_r2 = ctx.$implicit;
            return obj_r2.isActive = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, CustomerCreateComponent_div_14_div_7_Template, 2, 0, "div", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var obj_r2 = ctx.$implicit;

          var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("name", "address_", obj_r2.id, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", obj_r2.custName);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("name", "check_", obj_r2.id, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", obj_r2.isActive);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.customerList.length > 1);
        }
      }

      var CustomerCreateComponent = /*#__PURE__*/function () {
        function CustomerCreateComponent(router, baseService) {
          _classCallCheck(this, CustomerCreateComponent);

          this.router = router;
          this.baseService = baseService;
          this.dtOptions = {};
          this.customerOrderData = []; // We use this trigger because fetching the list of persons can be quite long,
          // thus we ensure the data is fetched before rendering

          this.dtTrigger = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
          this.customerList = [{
            id: 1,
            custName: '',
            isActive: true
          }];
          this.getUsers();
        }

        _createClass(CustomerCreateComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "addAddress",
          value: function addAddress() {
            this.customerList.push({
              id: this.customerList.length + 1,
              custName: '',
              isActive: true
            });
          }
        }, {
          key: "removeAddress",
          value: function removeAddress(i) {
            this.customerList.splice(i, 1);
          }
        }, {
          key: "logValue",
          value: function logValue() {
            console.log(this.customerList);

            for (var i = 0; i < this.customerList.length; i++) {
              var element = this.customerList[i];

              if (element.custName == '') {
                console.log("Eer");
              }
            }

            var tempObj = {
              configName: "customers",
              configData: this.customerList
            }; // 'api/configUsers/save-config'

            axios__WEBPACK_IMPORTED_MODULE_1___default.a.put(this.baseService.baseUrl + 'api/configUsers/' + this.customerId, tempObj, {
              headers: {
                'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
              }
            }).then(function (response) {
              console.log(response);

              if (response.data.status) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                  position: 'center',
                  icon: 'success',
                  title: response.data["message"],
                  showConfirmButton: false,
                  timer: 3000
                }).then(function () {
                  location.reload();
                });
              } else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                  position: 'center',
                  icon: 'error',
                  title: "Adding Failed..",
                  showConfirmButton: false,
                  timer: 3000
                });
              }
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }, {
          key: "getUsers",
          value: function getUsers() {
            var _this5 = this;

            axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.baseService.baseUrl + 'api/configUsers', {
              headers: {
                'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
              }
            }).then(function (response) {
              console.log(response);
              _this5.customerId = response.data.data[0]._id;
              _this5.customerList = response.data.data[0].configData; // this.dtTrigger.next();
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            // Do not forget to unsubscribe the event
            this.dtTrigger.unsubscribe();
          }
        }]);

        return CustomerCreateComponent;
      }();

      CustomerCreateComponent.ɵfac = function CustomerCreateComponent_Factory(t) {
        return new (t || CustomerCreateComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_app_services_base_service_service__WEBPACK_IMPORTED_MODULE_5__["BaseServiceService"]));
      };

      CustomerCreateComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: CustomerCreateComponent,
        selectors: [["app-customer-create"]],
        decls: 19,
        vars: 1,
        consts: [[1, "row"], [1, "col-md-12"], [1, "header"], [1, "row", "m-0"], [1, "col-12", 2, "text-align", "right"], ["type", "button", 1, "btn", "btn-primary", "mb-2", "p-1", 3, "click"], [1, "container", "mt-3", 3, "ngSubmit"], ["addressForm", "ngForm"], [1, "col-12"], [1, "p-1"], ["class", "col-md-6 col-sm-12", 4, "ngFor", "ngForOf"], [1, "col-2"], [1, "col-3"], ["type", "submit", "value", "Submit", 1, "btn", "btn-primary", 2, "padding", "5px"], [1, "col-md-6", "col-sm-12"], ["for", "normal-input-1", 1, "col-md-4", "col-sm-4", "col-form-label", "col-form-label-sm", "text-sm-left"], [1, "col-md-8", "col-sm-8", "mb-1"], [1, "form-group", "d-flex"], ["type", "text", "placeholder", "Enter name", 1, "form-control", "form-control-sm", 3, "ngModel", "name", "ngModelChange"], ["type", "checkbox", 3, "ngModel", "name", "ngModelChange"], ["class", "del-list", 3, "click", 4, "ngIf"], [1, "del-list", 3, "click"]],
        template: function CustomerCreateComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Customer Master");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CustomerCreateComponent_Template_button_click_6_listener() {
              return ctx.addAddress();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Add Customer");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "form", 6, 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function CustomerCreateComponent_Template_form_ngSubmit_8_listener() {
              return ctx.logValue();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "section", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, CustomerCreateComponent_div_14_Template, 8, 5, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "input", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.customerList);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgForm"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["CheckboxControlValueAccessor"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"]],
        styles: [".header[_ngcontent-%COMP%] {\n  text-align: center;\n  margin: 20px;\n  font-weight: 500;\n  padding-top: 10px;\n  padding-bottom: 10px;\n}\n\n.del-list[_ngcontent-%COMP%] {\n  background-color: red;\n  padding: 0 1rem;\n  border-radius: 4px;\n  color: #fff;\n  margin-left: 1rem;\n  display: flex;\n  align-items: center;\n  font-size: 20px;\n  cursor: pointer;\n}\n\n.login-container[_ngcontent-%COMP%] {\n  font-family: \"Source Sans Pro\";\n}\n\n.login-container[_ngcontent-%COMP%]   .sidebar[_ngcontent-%COMP%] {\n  background-image: url(/assets/images/group_4.png), linear-gradient(-136.82353093deg, #242348 0%, #5a55aa 100%);\n  background-size: cover;\n  min-height: 100vh;\n}\n\n.login-container[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%], .login-container[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #43425d;\n}\n\n.login-container[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%] {\n  font-weight: bold;\n  letter-spacing: 5px;\n}\n\n.login-container[_ngcontent-%COMP%]   .tag[_ngcontent-%COMP%] {\n  color: rgba(77, 79, 92, 0.5);\n}\n\n.login-container[_ngcontent-%COMP%]   .btn-main[_ngcontent-%COMP%] {\n  background: #43425d;\n}\n\n.login-container[_ngcontent-%COMP%]   .btn-main-outline[_ngcontent-%COMP%] {\n  border: 1px solid #43425d;\n}\n\ninput[type=\"radio\"][_ngcontent-%COMP%], input[type=\"checkbox\"][_ngcontent-%COMP%] {\n  box-sizing: border-box;\n  padding: 6px;\n  margin: 10px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFwuLlxcLi5cXGN1c3RvbWVyLWNyZWF0ZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHQTtFQUVFLGtCQUFrQjtFQUVsQixZQUFZO0VBRVosZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixvQkFBb0I7QUFMdEI7O0FBT0E7RUFDSSxxQkFBcUI7RUFDckIsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsaUJBQWlCO0VBQ2pCLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLGVBQWU7QUFKbkI7O0FBT0E7RUFDSSw4QkFBOEI7QUFKbEM7O0FBR0E7RUFHUSw4R0FDNEY7RUFDNUYsc0JBQXNCO0VBQ3RCLGlCQUFpQjtBQUh6Qjs7QUFIQTtFQVVRLGNBbENZO0FBK0JwQjs7QUFQQTtFQWNRLGlCQUFpQjtFQUNqQixtQkFBbUI7QUFIM0I7O0FBWkE7RUFtQlEsNEJBMUNzQjtBQXVDOUI7O0FBaEJBO0VBdUJRLG1CQWhEMEI7QUE2Q2xDOztBQXBCQTtFQTJCUSx5QkFwRDBCO0FBaURsQzs7QUFVQTtFQUNJLHNCQUFzQjtFQUN0QixZQUFZO0VBQ1osdUJBQXNCO0FBUDFCIiwiZmlsZSI6ImN1c3RvbWVyLWNyZWF0ZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiRjb2xvci1wcmltYXJ5OiByZ2JhKDY3LDY2LDkzLDI1NSk7XHJcbiRjb2xvci10ZXh0OiAjNDM0MjVkO1xyXG4kY29sb3ItdGV4dC1saXRlOiByZ2JhKCM0ZDRmNWMsLjUpO1xyXG4uaGVhZGVyXHJcbntcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vICAgYmFja2dyb3VuZC1jb2xvcjojNWE4ZWQyO1xyXG4gIG1hcmdpbjogMjBweDtcclxuLy8gICBjb2xvcjogd2hpdGU7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICBwYWRkaW5nLXRvcDogMTBweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxufVxyXG4uZGVsLWxpc3Qge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xyXG4gICAgcGFkZGluZzogMCAxcmVtO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBtYXJnaW4tbGVmdDogMXJlbTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG4ubG9naW4tY29udGFpbmVyIHtcclxuICAgIGZvbnQtZmFtaWx5OiBcIlNvdXJjZSBTYW5zIFByb1wiO1xyXG4gICAgLnNpZGViYXIge1xyXG4gICAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgvYXNzZXRzL2ltYWdlcy9ncm91cF80LnBuZyksIFxyXG4gICAgICAgICAgICAgIGxpbmVhci1ncmFkaWVudCgtMTM2LjgyMzUzMDkzMjAzZGVnICwgcmdiYSgzNiwzNSw3MiwyNTUpIDAlLCByZ2JhKDkwLDg1LDE3MCwyNTUpIDEwMCUpO1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICAgICAgbWluLWhlaWdodDogMTAwdmg7XHJcbiAgICB9XHJcblxyXG4gICAgLnRpdGxlLCBhIHtcclxuICAgICAgICBjb2xvcjogJGNvbG9yLXRleHQ7XHJcbiAgICB9XHJcblxyXG4gICAgLnRpdGxlIHtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgICAgICBsZXR0ZXItc3BhY2luZzogNXB4O1xyXG4gICAgfVxyXG5cclxuICAgIC50YWcge1xyXG4gICAgICAgIGNvbG9yOiAkY29sb3ItdGV4dC1saXRlXHJcbiAgICB9XHJcblxyXG4gICAgLmJ0bi1tYWluIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAkY29sb3ItcHJpbWFyeTtcclxuICAgIH1cclxuXHJcbiAgICAuYnRuLW1haW4tb3V0bGluZSB7XHJcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgJGNvbG9yLXByaW1hcnlcclxuICAgIH1cclxuXHJcbn1cclxuXHJcblxyXG5cclxuaW5wdXRbdHlwZT1cInJhZGlvXCJdLCBpbnB1dFt0eXBlPVwiY2hlY2tib3hcIl0ge1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgIHBhZGRpbmc6IDZweDtcclxuICAgIG1hcmdpbjogMTBweCFpbXBvcnRhbnQ7XHJcbn0iXX0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CustomerCreateComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-customer-create',
            templateUrl: './customer-create.component.html',
            styleUrls: ['./customer-create.component.scss']
          }]
        }], function () {
          return [{
            type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
          }, {
            type: _app_services_base_service_service__WEBPACK_IMPORTED_MODULE_5__["BaseServiceService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "K0QA":
    /*!****************************************************************************************************!*\
      !*** ./src/app/content/pages/main-block/entry-form/truck-entry-form/truck-entry-form.component.ts ***!
      \****************************************************************************************************/

    /*! exports provided: TruckEntryFormComponent */

    /***/
    function K0QA(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TruckEntryFormComponent", function () {
        return TruckEntryFormComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! axios */
      "vDqi");
      /* harmony import */


      var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
      /* harmony import */


      var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! sweetalert2 */
      "PSD3");
      /* harmony import */


      var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _app_services_base_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @app/services/base-service.service */
      "eWbo");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! angular-ng-autocomplete */
      "bH2/");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      function TruckEntryFormComponent_ng_template_13_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "small", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var item_r29 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("innerHTML", item_r29.truck_number, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeHtml"]);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r29.truck_number);
        }
      }

      function TruckEntryFormComponent_ng_template_15_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 52);
        }

        if (rf & 2) {
          var notFound_r30 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("innerHTML", notFound_r30, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeHtml"]);
        }
      }

      function TruckEntryFormComponent_option_39_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var data_r31 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](data_r31.custName);
        }
      }

      function TruckEntryFormComponent_div_40_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Customer Name is required. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function TruckEntryFormComponent_div_40_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TruckEntryFormComponent_div_40_div_1_Template, 2, 0, "div", 54);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r9.errors.required);
        }
      }

      function TruckEntryFormComponent_option_53_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var data_r33 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](data_r33.prodName);
        }
      }

      function TruckEntryFormComponent_div_54_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Product Name is required. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function TruckEntryFormComponent_div_54_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TruckEntryFormComponent_div_54_div_1_Template, 2, 0, "div", 54);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](50);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r13.errors.required);
        }
      }

      function TruckEntryFormComponent_div_65_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Tare Weight is required. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function TruckEntryFormComponent_div_65_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TruckEntryFormComponent_div_65_div_1_Template, 2, 0, "div", 54);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](64);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r17.errors.required);
        }
      }

      function TruckEntryFormComponent_div_76_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Gross Weight is required. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function TruckEntryFormComponent_div_76_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TruckEntryFormComponent_div_76_div_1_Template, 2, 0, "div", 54);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](75);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r20.errors.required);
        }
      }

      function TruckEntryFormComponent_div_87_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Target Weight is required. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function TruckEntryFormComponent_div_87_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TruckEntryFormComponent_div_87_div_1_Template, 2, 0, "div", 54);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r23 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](86);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r23.errors.required);
        }
      }

      function TruckEntryFormComponent_div_98_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " CCOE Weight is required. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function TruckEntryFormComponent_div_98_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TruckEntryFormComponent_div_98_div_1_Template, 2, 0, "div", 54);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r26 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](97);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r26.errors.required);
        }
      }

      var TruckEntryFormComponent = /*#__PURE__*/function () {
        function TruckEntryFormComponent(router, baseService) {
          _classCallCheck(this, TruckEntryFormComponent);

          this.router = router;
          this.baseService = baseService;
          this.truckData = {
            customer_code: "",
            product_code: ""
          };
          this.keyword = 'truck_number';
          this.truckList = [];
          this.truckNumberSelected = {};
          this.newTruck = true;
          this.productList = [];
          this.customerList = [];
          this.getTrucksList();
          this.getDropdownData();
        }

        _createClass(TruckEntryFormComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "truckSelected",
          value: function truckSelected(a) {
            // console.log(a);
            this.newTruck = false;
            this.getTruckById(a);
          }
        }, {
          key: "getTrucksList",
          value: function getTrucksList() {
            var _this6 = this;

            axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.baseService.baseUrl + 'api/truck', {
              headers: {
                'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
              }
            }).then(function (response) {
              console.log(response);
              _this6.truckList = response.data.data;
            })["catch"](function (error) {
              console.log(error);
            });
          } // convetDateFormat(dt) {
          //   console.log(dt);
          //  var date = new Date(dt)
          // let year = date.getFullYear();
          // let month = date.getMonth().toString().padStart(2, '0');
          // let day = (date.getDate()).toString().padStart(2, '0');
          //  console.log("convert Date...")
          //  console.log(year + '-' + month + '-' +day)
          // return  year + '-' + month + '-' +day;
          // }

        }, {
          key: "getTruckById",
          value: function getTruckById(obj) {
            var _this7 = this;

            axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.baseService.baseUrl + 'api/truck/' + obj.truck_number, {
              headers: {
                'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
              }
            }).then(function (response) {
              var _a, _b, _c, _d;

              if (response.status) {
                _this7.truckData = response.data.data;
                console.log(_this7.truckData);
                if (_this7.truckData.srv_due_date) _this7.truckData.srv_due_date = response.data.data.srv_due_date.split('T')[0];
                if (_this7.truckData.efcv_due_date) _this7.truckData.efcv_due_date = response.data.data.efcv_due_date.split('T')[0];
                if (_this7.truckData.fc_due_date) _this7.truckData.fc_due_date = response.data.data.fc_due_date.split('T')[0];
                if (_this7.truckData.fire_extinguisher_date) _this7.truckData.fire_extinguisher_date = (_a = response.data.data) === null || _a === void 0 ? void 0 : _a.fire_extinguisher_date.split('T')[0];
                if (_this7.truckData.hydro_test_due_date) _this7.truckData.hydro_test_due_date = (_b = response.data.data) === null || _b === void 0 ? void 0 : _b.hydro_test_due_date.split('T')[0];
                if (_this7.truckData.insurance_due_date) _this7.truckData.insurance_due_date = (_c = response.data.data) === null || _c === void 0 ? void 0 : _c.insurance_due_date.split('T')[0];
                if (_this7.truckData.road_permit_date) _this7.truckData.road_permit_date = (_d = response.data.data) === null || _d === void 0 ? void 0 : _d.road_permit_date.split('T')[0];
                if (_this7.truckData.ccoe_licence_date) _this7.truckData.ccoe_licence_date = response.data.data.ccoe_licence_date.split('T')[0];
                console.log("Object Test");
                console.log(_this7.truckData); // this.truckData.efcv_due_date = this.convetDateFormat(response.data.data.efcv_due_date);
                // this.truckData.fc_due_date = this.convetDateFormat(response.data.data.fc_due_date);
                // this.truckData.fire_extinguisher_date = this.convetDateFormat(response.data.data.fire_extinguisher_date);
                // this.truckData.hydro_test_due_date = this.convetDateFormat(response.data.data.hydro_test_due_date);
                // this.truckData.insurance_due_date = this.convetDateFormat(response.data.data.insurance_due_date);
                // this.truckData.read_permit_date =  this.convetDateFormat(response.data.data.read_permit_date);
                // this.truckData.srv_due_date = this.convetDateFormat(response.data.data.srv_due_date);
                // this.truckData.ccoe_licence_date = this.convetDateFormat(response.data.data.ccoe_licence_date);
              }
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }, {
          key: "onFocused",
          value: function onFocused(a) {
            if (typeof a == 'string') {
              this.newTruck = true;
              console.log(typeof a);
            } else {
              this.newTruck = false;
            }
          }
        }, {
          key: "resetForm",
          value: function resetForm() {
            this.truckData = {};
          }
        }, {
          key: "formSubmit",
          value: function formSubmit(form) {
            console.log(this.truckData);

            if (this.newTruck) {
              this.truckData.truck_number = this.truckNumberSelected.truckNumber;
              delete this.truckData._id;
              axios__WEBPACK_IMPORTED_MODULE_1___default.a.post(this.baseService.baseUrl + 'api/truck', this.truckData, {
                headers: {
                  'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
                }
              }).then(function (response) {
                console.log('resp: ', response);

                if (response.data.status) {
                  console.log(response.data.data);
                  sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    position: 'center',
                    icon: 'success',
                    title: "Successfully Saved.",
                    showConfirmButton: false,
                    timer: 3000
                  }); // .then(() => {
                  //   location.reload(); });

                  form.resetForm("");
                }
              })["catch"](function (error) {
                console.log(error);
              });
            } else {
              axios__WEBPACK_IMPORTED_MODULE_1___default.a.put(this.baseService.baseUrl + 'api/truck/' + this.truckData._id, this.truckData, {
                headers: {
                  'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
                }
              }).then(function (response) {
                console.log('resp: ', response);

                if (response.data.status) {
                  sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    position: 'center',
                    icon: 'success',
                    title: response.data["message"],
                    showConfirmButton: false,
                    timer: 3000
                  }); // .then(() => {
                  //   location.reload(); });

                  form.resetForm("");
                }
              })["catch"](function (error) {
                console.log(error);
              });
            }
          }
        }, {
          key: "getDropdownData",
          value: function getDropdownData() {
            var _this8 = this;

            axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.baseService.baseUrl + 'api/configUsers', {
              headers: {
                'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
              }
            }).then(function (response) {
              console.log(response);
              _this8.productList = response.data.data[1].configData.filter(function (element) {
                console.log(element);
                if (element.isActive) return element;
              });
              _this8.customerList = response.data.data[0].configData.filter(function (element) {
                if (element.isActive) return element;
              });
              console.log(_this8.customerList);
              console.log(_this8.productList); // this.dtTrigger.next();
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }, {
          key: "post",
          value: function post() {
            var obj = {};
            axios__WEBPACK_IMPORTED_MODULE_1___default.a.post(this.baseService.baseUrl + '', obj, {
              headers: {
                'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
              }
            }).then(function (response) {
              console.log('resp: ', response);
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }, {
          key: "patch",
          value: function patch() {
            var obj = {};
            axios__WEBPACK_IMPORTED_MODULE_1___default.a.patch(this.baseService.baseUrl + '', obj, {
              headers: {
                'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
              }
            }).then(function (response) {
              console.log('resp: ', response);
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }]);

        return TruckEntryFormComponent;
      }();

      TruckEntryFormComponent.ɵfac = function TruckEntryFormComponent_Factory(t) {
        return new (t || TruckEntryFormComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_app_services_base_service_service__WEBPACK_IMPORTED_MODULE_4__["BaseServiceService"]));
      };

      TruckEntryFormComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: TruckEntryFormComponent,
        selectors: [["app-truck-entry-form"]],
        decls: 112,
        vars: 29,
        consts: [[1, "row"], [1, "col-md-12"], [1, "header"], ["novalidate", "", 3, "ngSubmit"], ["truckForm", "ngForm"], [1, "form-group", "form-row"], ["for", "normal-input-1", 1, "col-md-2", "col-sm-3", "col-form-label", "col-form-label-sm", "text-sm-right"], [1, "col-md-4", "col-sm-9", "mb-1"], [1, "ng-autocomplete", 2, "width", "auto"], ["historyIdentifier", "truckList", "name", "truckNumber", 3, "data", "searchKeyword", "ngModel", "itemTemplate", "notFoundTemplate", "selected", "closed", "ngModelChange"], ["truckNumber", "ngModel"], ["itemTemplate", ""], ["notFoundTemplate", ""], ["type", "date", "placeholder", "dd-mm-yyyy", "name", "srv_due_date", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["srv_due_date", "ngModel"], ["type", "text", "name", "transporter_name", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["transporter_name", "ngModel"], ["type", "date", "name", "hydro_test_due_date", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["hydro_test_due_date", "ngModel"], ["id", "customer_code", "name", "customer_code", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["customer_code", "ngModel"], ["value", ""], [4, "ngFor", "ngForOf"], ["class", "form-text text-danger", 4, "ngIf"], ["type", "date", "name", "road_permit_date", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["road_permit_date", "ngModel"], ["id", "product_code", "name", "product_code", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["product_code", "ngModel"], ["type", "date", "name", "insurance_due_date", "placeholder", "dd-mm-yyyy", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["insurance_due_date", "ngModel"], ["type", "number", "name", "tare_wt", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["tare_wt", "ngModel"], ["type", "date", "name", "ccoe_licence_date", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["ccoe_licence_date", "ngModel"], ["type", "number", "name", "max_gross_wt", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["max_gross_wt", "ngModel"], ["type", "date", "name", "fc_due_date", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["fc_due_date", "ngModel"], ["type", "number", "name", "target_wt", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["target_wt", "ngModel"], ["type", "date", "name", "fire_extinguisher_date", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["fire_extinguisher_date", "ngModel"], ["type", "number", "name", "ccoe_wt", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["ccoe_wt", "ngModel"], ["_ngcontent-yqo-c332", "", "aria-label", "With textarea", "name", "special_remarks", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["special_remarks", "ngModel"], [1, "form-group", "form-row", 2, "margin-left", "32%"], [1, "col-md-2", "col-sm-3", "text-sm-right", "mb-4", "mb-sm-0"], [1, "col-md-1", "col-sm-3", "mb-4", "mb-sm-0", 2, "width", "150px"], ["type", "submit", 1, "btn", "btn-primary", 3, "disabled"], ["type", "button", 1, "btn", "btn-primary", 3, "click"], [1, "item"], [3, "innerHTML"], [1, "form-text", "text-danger"], [4, "ngIf"]],
        template: function TruckEntryFormComponent_Template(rf, ctx) {
          if (rf & 1) {
            var _r39 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "TRUCK MASTER");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "form", 3, 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function TruckEntryFormComponent_Template_form_ngSubmit_4_listener() {
              _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r39);

              var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](5);

              return ctx.formSubmit(_r0);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "label", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Truck Number");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "ng-autocomplete", 9, 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("selected", function TruckEntryFormComponent_Template_ng_autocomplete_selected_11_listener($event) {
              return ctx.truckSelected($event);
            })("closed", function TruckEntryFormComponent_Template_ng_autocomplete_closed_11_listener() {
              return ctx.onFocused(ctx.truckNumberSelected.truckNumber);
            })("ngModelChange", function TruckEntryFormComponent_Template_ng_autocomplete_ngModelChange_11_listener($event) {
              return ctx.truckNumberSelected.truckNumber = $event;
            })("selected", function TruckEntryFormComponent_Template_ng_autocomplete_selected_11_listener($event) {
              return ctx.truckSelected($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, TruckEntryFormComponent_ng_template_13_Template, 3, 2, "ng-template", null, 11, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](15, TruckEntryFormComponent_ng_template_15_Template, 1, 1, "ng-template", null, 12, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "label", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "SRV Due Date");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "input", 13, 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TruckEntryFormComponent_Template_input_ngModelChange_20_listener($event) {
              return ctx.truckData.srv_due_date = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "label", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Transporter\n");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "input", 15, 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TruckEntryFormComponent_Template_input_ngModelChange_25_listener($event) {
              return ctx.truckData.transporter_name = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "label", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Hydro Test Due Date");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "input", 17, 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TruckEntryFormComponent_Template_input_ngModelChange_30_listener($event) {
              return ctx.truckData.hydro_test_due_date = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "label", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Customer\n");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "select", 19, 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TruckEntryFormComponent_Template_select_ngModelChange_35_listener($event) {
              return ctx.truckData.customer_code = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "option", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, " -Select Customer Name-");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](39, TruckEntryFormComponent_option_39_Template, 2, 1, "option", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](40, TruckEntryFormComponent_div_40_Template, 2, 1, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "label", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Road Permit Date");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "input", 24, 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TruckEntryFormComponent_Template_input_ngModelChange_44_listener($event) {
              return ctx.truckData.road_permit_date = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "label", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "Product ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "select", 26, 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TruckEntryFormComponent_Template_select_ngModelChange_49_listener($event) {
              return ctx.truckData.product_code = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "option", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, " -Select Product Name-");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](53, TruckEntryFormComponent_option_53_Template, 2, 1, "option", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](54, TruckEntryFormComponent_div_54_Template, 2, 1, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "label", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "Insurance Due Date");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "input", 28, 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TruckEntryFormComponent_Template_input_ngModelChange_58_listener($event) {
              return ctx.truckData.insurance_due_date = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "label", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Tare Weight");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "input", 30, 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TruckEntryFormComponent_Template_input_ngModelChange_63_listener($event) {
              return ctx.truckData.tare_wt = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](65, TruckEntryFormComponent_div_65_Template, 2, 1, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "label", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "CCOE Licence Date");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "input", 32, 33);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TruckEntryFormComponent_Template_input_ngModelChange_69_listener($event) {
              return ctx.truckData.ccoe_licence_date = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "label", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](72, "Gross Weight");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "input", 34, 35);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TruckEntryFormComponent_Template_input_ngModelChange_74_listener($event) {
              return ctx.truckData.max_gross_wt = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](76, TruckEntryFormComponent_div_76_Template, 2, 1, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "label", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "FC Due Date");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "input", 36, 37);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TruckEntryFormComponent_Template_input_ngModelChange_80_listener($event) {
              return ctx.truckData.fc_due_date = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "label", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](83, "Target Weight");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "input", 38, 39);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TruckEntryFormComponent_Template_input_ngModelChange_85_listener($event) {
              return ctx.truckData.target_wt = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](87, TruckEntryFormComponent_div_87_Template, 2, 1, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "label", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](89, "Fire Extinguisher Date");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "input", 40, 41);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TruckEntryFormComponent_Template_input_ngModelChange_91_listener($event) {
              return ctx.truckData.fire_extinguisher_date = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "label", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, "CCOE Weight");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "input", 42, 43);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TruckEntryFormComponent_Template_input_ngModelChange_96_listener($event) {
              return ctx.truckData.ccoe_wt = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](98, TruckEntryFormComponent_div_98_Template, 2, 1, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "label", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](100, "Any Special Remarks");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "textarea", 44, 45);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TruckEntryFormComponent_Template_textarea_ngModelChange_102_listener($event) {
              return ctx.truckData.special_remarks = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "div", 46);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](105, "div", 47);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "div", 48);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "button", 49);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](108, "Save");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "div", 48);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "button", 50);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TruckEntryFormComponent_Template_button_click_110_listener() {
              return ctx.resetForm();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](111, "Reset");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](5);

            var _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](14);

            var _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](16);

            var _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](36);

            var _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](50);

            var _r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](64);

            var _r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](75);

            var _r23 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](86);

            var _r26 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](97);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.truckList)("searchKeyword", ctx.keyword)("ngModel", ctx.truckNumberSelected.truckNumber)("itemTemplate", _r2)("notFoundTemplate", _r4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.srv_due_date);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.transporter_name);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.hydro_test_due_date);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.customer_code);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.customerList);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r9.invalid && (_r9.dirty || _r9.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.road_permit_date);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.product_code);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.productList);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r13.invalid && (_r13.dirty || _r13.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.insurance_due_date);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.tare_wt);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r17.invalid && (_r17.dirty || _r17.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.ccoe_licence_date);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.max_gross_wt);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r20.invalid && (_r20.dirty || _r20.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.fc_due_date);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.target_wt);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r23.invalid && (_r23.dirty || _r23.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.fire_extinguisher_date);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.ccoe_wt);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r26.invalid && (_r26.dirty || _r26.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.special_remarks);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !_r0.valid);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgForm"], angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_6__["AutocompleteComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["SelectControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ɵangular_packages_forms_forms_x"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NumberValueAccessor"]],
        styles: [".text-danger[_ngcontent-%COMP%] {\n  margin-top: 0 !important;\n  font-size: 7px !important;\n}\n\nbutton[_ngcontent-%COMP%] {\n  padding: 12px;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  font-size: 11px;\n}\n\n.header[_ngcontent-%COMP%] {\n  text-align: center;\n  margin: 20px;\n  font-weight: 500;\n  padding-top: 10px;\n  padding-bottom: 10px;\n}\n\n.form-control[_ngcontent-%COMP%] {\n  width: 85% !important;\n}\n\n.autocomplete-container[_ngcontent-%COMP%]   .input-container[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n  width: 85% !important;\n}\n\nb[_ngcontent-%COMP%], strong[_ngcontent-%COMP%] {\n  font-weight: bolder;\n  color: #f12e2d;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFwuLlxcLi5cXC4uXFx0cnVjay1lbnRyeS1mb3JtLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksd0JBQXdCO0VBQ3hCLHlCQUF3QjtBQUM1Qjs7QUFDQTtFQUNJLGFBQWE7RUFFYiwwQkFBa0I7RUFBbEIsdUJBQWtCO0VBQWxCLGtCQUFrQjtFQUFDLGVBQWU7QUFFdEM7O0FBQ0E7RUFFRSxrQkFBa0I7RUFHbEIsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsb0JBQW9CO0FBRHRCOztBQUlBO0VBRUEscUJBQW1CO0FBRm5COztBQUtBO0VBQ0kscUJBQW9CO0FBRnhCOztBQU1BO0VBQ0UsbUJBQW1CO0VBQ25CLGNBQWE7QUFIZiIsImZpbGUiOiJ0cnVjay1lbnRyeS1mb3JtLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRleHQtZGFuZ2VyIHtcclxuICAgIG1hcmdpbi10b3A6IDAgIWltcG9ydGFudDtcclxuICAgIGZvbnQtc2l6ZTo3cHggIWltcG9ydGFudDtcclxufVxyXG5idXR0b24ge1xyXG4gICAgcGFkZGluZzogMTJweDtcclxuICAgXHJcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7Zm9udC1zaXplOiAxMXB4O1xyXG59XHJcblxyXG4uaGVhZGVyXHJcbntcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vICAgYmFja2dyb3VuZC1jb2xvcjojZWM5MTIxO1xyXG4vLyAgICBjb2xvcjogd2hpdGU7XHJcbiAgbWFyZ2luOiAyMHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbn1cclxuXHJcbi5mb3JtLWNvbnRyb2xcclxue1xyXG53aWR0aDo4NSUhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uYXV0b2NvbXBsZXRlLWNvbnRhaW5lciAuaW5wdXQtY29udGFpbmVyIGlucHV0IHtcclxuICAgIHdpZHRoOiA4NSUhaW1wb3J0YW50O1xyXG59XHJcblxyXG5cclxuYiwgc3Ryb25nIHtcclxuICBmb250LXdlaWdodDogYm9sZGVyO1xyXG4gIGNvbG9yOiNmMTJlMmQ7XHJcbn1cclxuXHJcbiJdfQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TruckEntryFormComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-truck-entry-form',
            templateUrl: './truck-entry-form.component.html',
            styleUrls: ['./truck-entry-form.component.scss']
          }]
        }], function () {
          return [{
            type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
          }, {
            type: _app_services_base_service_service__WEBPACK_IMPORTED_MODULE_4__["BaseServiceService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "L8iN":
    /*!****************************************************************************************************!*\
      !*** ./src/app/content/pages/main-block/entry-form/order-entry-form/order-entry-form.component.ts ***!
      \****************************************************************************************************/

    /*! exports provided: OrderEntryFormComponent */

    /***/
    function L8iN(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OrderEntryFormComponent", function () {
        return OrderEntryFormComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! axios */
      "vDqi");
      /* harmony import */


      var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
      /* harmony import */


      var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! sweetalert2 */
      "PSD3");
      /* harmony import */


      var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _app_services_base_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @app/services/base-service.service */
      "eWbo");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! angular-ng-autocomplete */
      "bH2/");

      function OrderEntryFormComponent_div_13_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Card No is required. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function OrderEntryFormComponent_div_13_div_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Card No is 4 digit. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function OrderEntryFormComponent_div_13_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, OrderEntryFormComponent_div_13_div_1_Template, 2, 0, "div", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, OrderEntryFormComponent_div_13_div_2_Template, 2, 0, "div", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r1.errors.required);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r1.errors.pattern);
        }
      }

      function OrderEntryFormComponent_ng_template_32_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "small", 54);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var item_r38 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("innerHTML", item_r38.truck_number, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeHtml"]);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r38.truck_number);
        }
      }

      function OrderEntryFormComponent_ng_template_34_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 54);
        }

        if (rf & 2) {
          var notFound_r39 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("innerHTML", notFound_r39, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeHtml"]);
        }
      }

      function OrderEntryFormComponent_div_39_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 55);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](3, "date");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](3, 1, ctx_r7.truckData == null ? null : ctx_r7.truckData.srv_due_date, "dd/MM/yyyy" || false), "");
        }
      }

      function OrderEntryFormComponent_div_40_Template(rf, ctx) {
        if (rf & 1) {
          var _r42 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "input", 56, 57);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function OrderEntryFormComponent_div_40_Template_input_ngModelChange_1_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r42);

            var ctx_r41 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r41.truckData.srv_due_date = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r8.truckData.srv_due_date);
        }
      }

      function OrderEntryFormComponent_div_47_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Customer Name is required. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function OrderEntryFormComponent_div_47_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, OrderEntryFormComponent_div_47_div_1_Template, 2, 0, "div", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r9.errors.required);
        }
      }

      function OrderEntryFormComponent_div_50_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 55);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](3, "date");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](3, 1, ctx_r11.truckData == null ? null : ctx_r11.truckData.hydro_test_due_date, "dd/MM/yyyy" || false), "");
        }
      }

      function OrderEntryFormComponent_div_51_Template(rf, ctx) {
        if (rf & 1) {
          var _r46 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "input", 58, 59);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function OrderEntryFormComponent_div_51_Template_input_ngModelChange_1_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r46);

            var ctx_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r45.truckData.hydro_test_due_date = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r12.truckData.hydro_test_due_date);
        }
      }

      function OrderEntryFormComponent_div_58_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Product Name is required. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function OrderEntryFormComponent_div_58_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, OrderEntryFormComponent_div_58_div_1_Template, 2, 0, "div", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](57);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r13.errors.required);
        }
      }

      function OrderEntryFormComponent_div_61_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 55);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](3, "date");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](3, 1, ctx_r15.truckData == null ? null : ctx_r15.truckData.road_permit_date, "dd/MM/yyyy" || false), "");
        }
      }

      function OrderEntryFormComponent_div_62_Template(rf, ctx) {
        if (rf & 1) {
          var _r50 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "input", 60, 61);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function OrderEntryFormComponent_div_62_Template_input_ngModelChange_1_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r50);

            var ctx_r49 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r49.truckData.road_permit_date = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r16.truckData.road_permit_date);
        }
      }

      function OrderEntryFormComponent_div_69_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Tare Weight is required. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function OrderEntryFormComponent_div_69_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, OrderEntryFormComponent_div_69_div_1_Template, 2, 0, "div", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](68);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r17.errors.required);
        }
      }

      function OrderEntryFormComponent_div_72_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 55);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](3, "date");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](3, 1, ctx_r19.truckData == null ? null : ctx_r19.truckData.insurance_due_date, "dd/MM/yyyy" || false), "");
        }
      }

      function OrderEntryFormComponent_div_73_div_3_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Insurance Date is required. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function OrderEntryFormComponent_div_73_div_3_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, OrderEntryFormComponent_div_73_div_3_div_1_Template, 2, 0, "div", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r52 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r52.errors.required);
        }
      }

      function OrderEntryFormComponent_div_73_Template(rf, ctx) {
        if (rf & 1) {
          var _r56 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "input", 62, 63);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function OrderEntryFormComponent_div_73_Template_input_ngModelChange_1_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r56);

            var ctx_r55 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r55.truckData.insurance_due_date = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, OrderEntryFormComponent_div_73_div_3_Template, 2, 1, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var _r52 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](2);

          var ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r20.truckData.insurance_due_date);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r52.invalid && (_r52.dirty || _r52.touched));
        }
      }

      function OrderEntryFormComponent_div_80_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Gross Weight is required. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function OrderEntryFormComponent_div_80_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, OrderEntryFormComponent_div_80_div_1_Template, 2, 0, "div", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](79);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r21.errors.required);
        }
      }

      function OrderEntryFormComponent_div_83_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 55);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](3, "date");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r23 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](3, 1, ctx_r23.truckData == null ? null : ctx_r23.truckData.ccoe_licence_date, "dd/MM/yyyy" || false), "");
        }
      }

      function OrderEntryFormComponent_div_84_Template(rf, ctx) {
        if (rf & 1) {
          var _r60 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "input", 64, 65);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function OrderEntryFormComponent_div_84_Template_input_ngModelChange_1_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r60);

            var ctx_r59 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r59.truckData.ccoe_licence_date = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r24.truckData.ccoe_licence_date);
        }
      }

      function OrderEntryFormComponent_div_91_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Target Weight is required. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function OrderEntryFormComponent_div_91_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, OrderEntryFormComponent_div_91_div_1_Template, 2, 0, "div", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r25 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](90);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r25.errors.required);
        }
      }

      function OrderEntryFormComponent_div_94_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 55);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](3, "date");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](3, 1, ctx_r27.truckData == null ? null : ctx_r27.truckData.fc_due_date, "dd/MM/yyyy" || false), "");
        }
      }

      function OrderEntryFormComponent_div_95_Template(rf, ctx) {
        if (rf & 1) {
          var _r64 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "input", 66, 67);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function OrderEntryFormComponent_div_95_Template_input_ngModelChange_1_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r64);

            var ctx_r63 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r63.truckData.fc_due_date = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r28.truckData.fc_due_date);
        }
      }

      function OrderEntryFormComponent_div_102_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " CCOE Weight is required. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function OrderEntryFormComponent_div_102_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, OrderEntryFormComponent_div_102_div_1_Template, 2, 0, "div", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r29 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](101);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r29.errors.required);
        }
      }

      function OrderEntryFormComponent_div_105_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 55);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](3, "date");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](3, 1, ctx_r31.truckData == null ? null : ctx_r31.truckData.fire_extinguisher_date, "dd/MM/yyyy" || false), "");
        }
      }

      function OrderEntryFormComponent_div_106_Template(rf, ctx) {
        if (rf & 1) {
          var _r68 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "input", 68, 69);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function OrderEntryFormComponent_div_106_Template_input_ngModelChange_1_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r68);

            var ctx_r67 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r67.truckData.fire_extinguisher_date = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r32 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r32.truckData.fire_extinguisher_date);
        }
      }

      function OrderEntryFormComponent_div_113_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Qty To be Loaded is required. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function OrderEntryFormComponent_div_113_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, OrderEntryFormComponent_div_113_div_1_Template, 2, 0, "div", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r33 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](112);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r33.errors.required);
        }
      }

      var _c0 = function _c0(a0) {
        return {
          "loadingLable": a0
        };
      };

      var _c1 = function _c1() {
        return {
          "color": "red"
        };
      };

      var OrderEntryFormComponent = /*#__PURE__*/function () {
        function OrderEntryFormComponent(router, baseService) {
          _classCallCheck(this, OrderEntryFormComponent);

          this.router = router;
          this.baseService = baseService;
          this.truckData = {};
          this.truckDataTime = {};
          this.keyword = 'truck_number';
          this.truckList = [];
          this.status = true;
          this.extraDetials = {};
          this.btnStatus = false;
          this.loadingStatus = true;
          this.unloadingStatus = false;
          this.cardEntered = false;
        }

        _createClass(OrderEntryFormComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this9 = this;

            var today = new Date();
            var dd = today.getDate().toString().padStart(2, '0');
            var mm = (today.getMonth() + 1).toString().padStart(2, '0');
            var yyyy = today.getFullYear();
            this.toDayDate = yyyy + "-" + mm + "-" + dd;
            console.log("Today Date");
            console.log(this.toDayDate);
            this.getTrucksList();
            setInterval(function () {
              var dt = new Date();
              _this9.currentDate = dt.getDate().toString().padStart(2, '0') + "/" + (dt.getMonth() + 1).toString().padStart(2, '0') + "/" + dt.getFullYear() + " " + dt.getHours().toString().padStart(2, '0') + ":" + dt.getMinutes().toString().padStart(2, '0') + ":" + dt.getSeconds().toString().padStart(2, '0');
            }, 1000);
          }
        }, {
          key: "resetForm",
          value: function resetForm() {
            window.location.reload();
          }
        }, {
          key: "truckSelected",
          value: function truckSelected(a) {
            this.checkTruck(a);
          }
        }, {
          key: "checkTruck",
          value: function checkTruck(val) {
            var _this10 = this;

            var tempObj = {
              "truckNum": val.truck_number
            };
            axios__WEBPACK_IMPORTED_MODULE_1___default.a.post(this.baseService.baseUrl + 'api/customer/truck-verify', tempObj, {
              headers: {
                'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
              }
            }).then(function (response) {
              console.log("RES", response);

              if (!response.data.status) {
                _this10.truckMessage = "Truck is already in use";
              } else {
                _this10.truckMessage = '';

                _this10.getTruckById(val);
              }
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }, {
          key: "getTrucksList",
          value: function getTrucksList() {
            var _this11 = this;

            axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.baseService.baseUrl + 'api/truck', {
              headers: {
                'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
              }
            }).then(function (response) {
              console.log(response);
              _this11.truckList = response.data.data;
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }, {
          key: "loadingStatusChange",
          value: function loadingStatusChange() {
            this.loadingStatus = !this.loadingStatus;
            this.unloadingStatus = !this.unloadingStatus;

            if (this.unloadingStatus) {
              this.truckData.qty_to_be_loaded = '49999';
            } else {
              this.truckData.qty_to_be_loaded = '';
            }
          }
        }, {
          key: "checkValue",
          value: function checkValue(val) {
            console.log(val);

            if (val > this.truckData.target_wt) {
              console.log("ERRR");
              var appendElement = 'Qty To be Loaded value must be less than Target Weight';
              this.divMessages = appendElement;
            } else {
              console.log("no err");
              var appendElement = '';
              this.divMessages = appendElement;
            }
          }
        }, {
          key: "convetDateFormat",
          value: function convetDateFormat(dt) {
            console.log(dt);
            var date = new Date(dt); //.split('T')[0].split('-')
            //   let tempDt = dt.split('T')[0].split('-');
            //   tempDt = tempDt[2] + "/"
            //   + tempDt[1] + "/"
            //   + tempDt[0];
            // return tempDt;
            // getFormattedDate(new Date())

            var year = date.getFullYear();
            var month = (date.getMonth() + 1).toString().padStart(2, '0');
            var day = date.getDate().toString().padStart(2, '0');
            console.log("convert Date...");
            console.log(year + '-' + month + '-' + day);
            return year + '-' + month + '-' + day;
          }
        }, {
          key: "compareDateFormat",
          value: function compareDateFormat(dt) {
            console.log(dt);
            var tempDt = dt.split("/"); //.split('T')[0].split('-');

            tempDt = tempDt[2] + "/" + tempDt[1].toString().padStart(2, '0') + "/" + tempDt[0].toString().padStart(2, '0');
            return tempDt;
          }
        }, {
          key: "getTruckById",
          value: function getTruckById(obj) {
            var _this12 = this;

            this.sampleObj = obj;
            console.log("Todate Date");
            console.log(this.toDayDate);
            axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.baseService.baseUrl + 'api/truck/' + obj.truck_number, {
              headers: {
                'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
              }
            }).then(function (response) {
              if (response.data.status) {
                _this12.truckData = response.data.data;
                _this12.truckData.qty_to_be_loaded = _this12.truckData.target_wt;
                _this12.truckData.srv_due_date = _this12.convetDateFormat(response.data.data.srv_due_date);
                console.log("SrV Date:" + _this12.truckData.srv_due_date);
                _this12.truckData.hydro_test_due_date = _this12.convetDateFormat(response.data.data.hydro_test_due_date);
                _this12.truckData.road_permit_date = _this12.convetDateFormat(response.data.data.road_permit_date);

                if (response.data.data.insurance_due_date) {
                  _this12.truckData.insurance_due_date = _this12.convetDateFormat(response.data.data.insurance_due_date);
                } else {
                  _this12.truckData.insurance_due_date = "";
                }

                if (response.data.data.ccoe_licence_date) {
                  _this12.truckData.ccoe_licence_date = _this12.convetDateFormat(response.data.data.ccoe_licence_date);
                } else {
                  _this12.truckData.ccoe_licence_date = "";
                }

                _this12.truckData.fc_due_date = _this12.convetDateFormat(response.data.data.fc_due_date);
                _this12.truckData.fire_extinguisher_date = _this12.convetDateFormat(response.data.data.fire_extinguisher_date); // this.truckData.eecv_due_date = response.data.data.eecv_due_date.split('T')[0];    
                //////////////Date Compare Purpouse
                // this.truckDataTime.insurance_due_date = this.compareDateFormat(response.data.data.srv_due_date);
                // this.truckDataTime.ccoe_licence_date = this.compareDateFormat(response.data.data.fire_extinguisher_date);
                // this.truckDataTime.fc_due_date = this.compareDateFormat(response.data.data.fc_due_date);
                // this.truckDataTime.fire_extinguisher_date = this.compareDateFormat(response.data.data.fire_extinguisher_date);
                // this.truckDataTime.hydro_test_due_date = this.compareDateFormat(response.data.data.hydro_test_due_date);
                // this.truckDataTime.road_permit_date = this.compareDateFormat(response.data.data.road_permit_date);
                // this.truckDataTime.srv_due_date = this.compareDateFormat(response.data.data.srv_due_date);

                console.log("Truck Data");
                console.log(_this12.truckData);
                console.log("truckDataTime Data");
                console.log(_this12.truckDataTime);

                _this12.buttonStatus();
              }
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }, {
          key: "buttonStatus",
          value: function buttonStatus() {
            if (this.truckData.insurance_due_date < this.toDayDate || this.truckData.ccoe_licence_date < this.toDayDate || this.truckData.fc_due_date < this.toDayDate || this.truckData.fire_extinguisher_date < this.toDayDate || this.truckData.hydro_test_due_date < this.toDayDate || this.truckData.road_permit_date < this.toDayDate || this.truckData.srv_due_date < this.toDayDate) {
              this.btnStatus = false;
            } else {
              this.btnStatus = false;
            }
          }
        }, {
          key: "formSubmit",
          value: function formSubmit(form) {
            var _this13 = this;

            this.truckData.card_num = this.extraDetials.card_num;
            this.truckData.bayNo = this.extraDetials.bayNo;
            this.truckData.is_loading = this.loadingStatus;
            this.truckData.date = new Date(); // this.truckData.insurance_due_date = new Date(this.truckData.insurance_due_date);
            // this.truckData.ccoe_licence_date = new Date(this.truckData.ccoe_licence_date);
            // this.truckData.fc_due_date = new Date(this.truckData.fc_due_date);
            // this.truckData.fire_extinguisher_date = new Date(this.truckData.fire_extinguisher_date);
            // this.truckData.hydro_test_due_date = new Date(this.truckData.hydro_test_due_date);
            // this.truckData.road_permit_date = new Date(this.truckData.road_permit_date);
            // this.truckData.srv_due_date = new Date(this.truckData.srv_due_date);

            console.log(this.truckData);
            console.log("valid form");

            if (this.btnStatus) {
              console.log("Update Record");
              console.log(this.truckData);
              axios__WEBPACK_IMPORTED_MODULE_1___default.a.put(this.baseService.baseUrl + 'api/truck/' + this.truckData._id, this.truckData, {
                headers: {
                  'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
                }
              }).then(function (response) {
                if (response.data.status) {
                  sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    position: 'center',
                    icon: 'success',
                    title: "Updated Record Successfully",
                    showConfirmButton: false,
                    timer: 3000
                  }); //  form.resetForm(""); 

                  _this13.getTruckById(_this13.sampleObj);
                } else {
                  sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    position: 'center',
                    icon: 'error',
                    title: "Updated Fail Try Again",
                    showConfirmButton: false,
                    timer: 3000
                  });
                }
              })["catch"](function (error) {
                console.log(error);
              });
            } else if (!this.btnStatus && this.cardEntered) {
              console.log("Book Record");
              console.log(this.truckData);
              axios__WEBPACK_IMPORTED_MODULE_1___default.a.post(this.baseService.baseUrl + 'api/customer/', this.truckData, {
                headers: {
                  'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
                }
              }).then(function (response) {
                if (response.data.status) {
                  sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    position: 'center',
                    icon: 'success',
                    title: "Successfully Booked",
                    showConfirmButton: false,
                    timer: 3000
                  });
                  form.resetForm("");
                  _this13.truckData = {};
                } else {
                  sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    position: 'center',
                    icon: 'error',
                    title: "Fail Booked",
                    showConfirmButton: false,
                    timer: 3000
                  });
                }
              })["catch"](function (error) {
                console.log(error);
              });
            } else {
              console.log("invalid form");
              this.cardMessage = "Please Enter Card Number";
            }
          }
        }, {
          key: "enteredField",
          value: function enteredField() {
            // this.validCardNum = true;
            console.log("entering");
          }
        }, {
          key: "checkCarNum",
          value: function checkCarNum(val) {
            var _this14 = this;

            var tempObj = {
              "cardNum": val
            };
            this.cardEntered = false;

            if (val.length == 4 && val.length != undefined && !isNaN(val)) {
              axios__WEBPACK_IMPORTED_MODULE_1___default.a.post(this.baseService.baseUrl + 'api/customer/card-verify', tempObj, {
                headers: {
                  'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
                }
              }).then(function (response) {
                if (response.data.status) {
                  _this14.cardMessage = "";
                  _this14.cardEntered = true;
                } else {
                  _this14.cardMessage = response.data.message;
                  _this14.cardEntered = false;
                }
              })["catch"](function (error) {
                console.log(error);
              });
            } else {}
          }
        }]);

        return OrderEntryFormComponent;
      }();

      OrderEntryFormComponent.ɵfac = function OrderEntryFormComponent_Factory(t) {
        return new (t || OrderEntryFormComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_app_services_base_service_service__WEBPACK_IMPORTED_MODULE_4__["BaseServiceService"]));
      };

      OrderEntryFormComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: OrderEntryFormComponent,
        selectors: [["app-order-entry-form"]],
        decls: 129,
        vars: 61,
        consts: [[1, ""], [1, "row"], [1, "col-md-12"], [1, "header"], ["novalidate", "", 3, "ngSubmit"], ["orderForm", "ngForm"], [1, "form-group", "form-row", "mb-3"], ["for", "normal-input-1", 1, "col-md-2", "col-sm-2", "col-form-label", "col-form-label-sm", "text-sm-right", "required"], [1, "col-md-4", "col-sm-4", "mb-1"], ["type", "text", "name", "card_num", "required", "", "pattern", "\\d{4}", "maxlength", "4", "minlength", "4", 1, "form-control", "form-control-sm", "mb-0", 3, "ngModel", "blur", "ngModelChange", "keypress"], ["card_num", "ngModel"], ["class", "form-text text-danger", 4, "ngIf"], [1, "", 2, "font-size", "12px", "color", "red", 3, "innerHTML"], ["for", "normal-input-1", 1, "col-md-2", "col-sm-2", "col-form-label", "col-form-label-sm", "text-sm-right"], [1, "col-md-4", "col-sm-4", "mb-1", "d-flex", "align-items-center"], [1, "custom-control", "custom-radio", "custom-control-inline"], ["type", "radio", "id", "customRadioInline1", "name", "is_loading", 1, "custom-control-input", 3, "checked", "change"], ["for", "customRadioInline1", 1, "custom-control-label", 3, "ngClass"], ["type", "radio", "id", "customRadioInline2", "name", "customRadioInline1", "name", "is_loading", 1, "custom-control-input", 3, "checked", "change"], ["for", "customRadioInline2", 1, "custom-control-label", "loadingLable", 3, "ngClass"], [1, "form-group", "form-row"], ["for", "normal-input-1", 1, "col-md-2", "col-sm-3", "col-form-label", "col-form-label-sm", "text-sm-right"], [1, "col-md-4", "col-sm-9", "mb-1"], [1, "ng-autocomplete", 2, "width", "auto"], ["historyIdentifier", "truckList", 3, "data", "searchKeyword", "itemTemplate", "notFoundTemplate", "selected"], ["itemTemplate", ""], ["notFoundTemplate", ""], ["for", "normal-input-1", 1, "col-md-2", "col-sm-3", "col-form-label", "col-form-label-sm", "text-sm-right", 3, "ngStyle"], ["class", "col-md-4 col-sm-9 mb-1 align-items-center d-flex", 4, "ngIf"], ["class", "col-md-2 col-sm-9 mb-1", 4, "ngIf"], ["type", "text", "disabled", "", "name", "customer_code", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["customer_code", "ngModel"], ["type", "text", "disabled", "", "name", "product_code", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["product_code", "ngModel"], ["type", "number", "disabled", "", "name", "tare_wt", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["tare_wt", "ngModel"], ["type", "number", "disabled", "", "name", "max_gross_wt", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["max_gross_wt", "ngModel"], ["type", "number", "disabled", "", "name", "target_wt", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["target_wt", "ngModel"], ["type", "number", "disabled", "", "name", "ccoe_wt", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["ccoe_wt", "ngModel"], ["type", "text", "placeholder", "Enter the Quantity", "name", "qty_to_be_loaded", "required", "", 1, "form-control", "form-control-sm", 3, "disabled", "ngModel", "ngModelChange", "blur"], ["qty_to_be_loaded", "ngModel"], [1, "col-md-2", "col-sm-9", "mb-1"], ["type", "text", "placeholder", "Enter Order No", "name", "customerOrderNo", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["customerOrderNo", "ngModel"], [1, "col-md-3", "col-sm-3", "mb-4", "mb-sm-0", 2, "margin-left", "42%"], ["type", "submit", 1, "btn", "btn-primary", "mr-2"], ["type", "submit", 1, "btn", "btn-primary", "mr-2", 2, "display", "none"], [1, "btn", "btn-danger", 3, "click"], [1, "form-text", "text-danger"], [4, "ngIf"], [1, "item"], [3, "innerHTML"], [1, "col-md-4", "col-sm-9", "mb-1", "align-items-center", "d-flex"], ["type", "date", "name", "srv_due_date", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["srv_due_date", "ngModel"], ["type", "date", "name", "hydro_test_due_date", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["hydro_test_due_date", "ngModel"], ["type", "date", "name", "road_permit_date", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["road_permit_date", "ngModel"], ["type", "date", "name", "insurance_due_date", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["insurance_due_date", "ngModel"], ["type", "date", "value", "truckData.ccoe_licence_date", "name", "ccoe_licence_date", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["ccoe_licence_date", "ngModel"], ["type", "date", "name", "fc_due_date", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["fc_due_date", "ngModel"], ["type", "date", "name", "fire_extinguisher_date", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["fire_extinguisher_date", "ngModel"]],
        template: function OrderEntryFormComponent_Template(rf, ctx) {
          if (rf & 1) {
            var _r70 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h1", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Loading Order");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "form", 4, 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function OrderEntryFormComponent_Template_form_ngSubmit_5_listener() {
              _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r70);

              var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](6);

              return ctx.formSubmit(_r0);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "label", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Card No ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "input", 9, 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function OrderEntryFormComponent_Template_input_blur_11_listener() {
              return ctx.checkCarNum(ctx.extraDetials.card_num);
            })("ngModelChange", function OrderEntryFormComponent_Template_input_ngModelChange_11_listener($event) {
              return ctx.extraDetials.card_num = $event;
            })("keypress", function OrderEntryFormComponent_Template_input_keypress_11_listener() {
              return ctx.enteredField();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, OrderEntryFormComponent_div_13_Template, 3, 2, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "span", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "label", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Loading Type");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "input", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function OrderEntryFormComponent_Template_input_change_19_listener() {
              return ctx.loadingStatusChange();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "label", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Loading");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "input", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function OrderEntryFormComponent_Template_input_change_23_listener() {
              return ctx.loadingStatusChange();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "label", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Un Loading");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "label", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Truck Number");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "ng-autocomplete", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("selected", function OrderEntryFormComponent_Template_ng_autocomplete_selected_31_listener($event) {
              return ctx.truckSelected($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](32, OrderEntryFormComponent_ng_template_32_Template, 3, 2, "ng-template", null, 25, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](34, OrderEntryFormComponent_ng_template_34_Template, 1, 1, "ng-template", null, 26, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "span", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "label", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "SRV Due Date");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](39, OrderEntryFormComponent_div_39_Template, 4, 4, "div", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](40, OrderEntryFormComponent_div_40_Template, 3, 1, "div", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "label", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "Customer ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "input", 30, 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function OrderEntryFormComponent_Template_input_ngModelChange_45_listener($event) {
              return ctx.truckData.customer_code = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](47, OrderEntryFormComponent_div_47_Template, 2, 1, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "label", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "Hydro Test Due Date");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](50, OrderEntryFormComponent_div_50_Template, 4, 4, "div", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](51, OrderEntryFormComponent_div_51_Template, 3, 1, "div", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "label", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "Product ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "input", 32, 33);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function OrderEntryFormComponent_Template_input_ngModelChange_56_listener($event) {
              return ctx.truckData.product_code = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](58, OrderEntryFormComponent_div_58_Template, 2, 1, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "label", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, "Road Permit Date");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](61, OrderEntryFormComponent_div_61_Template, 4, 4, "div", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](62, OrderEntryFormComponent_div_62_Template, 3, 1, "div", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "label", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, "Tare Weight");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "input", 34, 35);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function OrderEntryFormComponent_Template_input_ngModelChange_67_listener($event) {
              return ctx.truckData.tare_wt = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](69, OrderEntryFormComponent_div_69_Template, 2, 1, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "label", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "Insurance Due Date");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](72, OrderEntryFormComponent_div_72_Template, 4, 4, "div", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](73, OrderEntryFormComponent_div_73_Template, 4, 2, "div", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "label", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](76, "Gross Weight");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "div", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "input", 36, 37);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function OrderEntryFormComponent_Template_input_ngModelChange_78_listener($event) {
              return ctx.truckData.max_gross_wt = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](80, OrderEntryFormComponent_div_80_Template, 2, 1, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "label", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "CCOE Licence Date");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](83, OrderEntryFormComponent_div_83_Template, 4, 4, "div", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](84, OrderEntryFormComponent_div_84_Template, 3, 1, "div", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "label", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](87, "Target Weight");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "div", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "input", 38, 39);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function OrderEntryFormComponent_Template_input_ngModelChange_89_listener($event) {
              return ctx.truckData.target_wt = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](91, OrderEntryFormComponent_div_91_Template, 2, 1, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "label", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](93, "FC Due Date");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](94, OrderEntryFormComponent_div_94_Template, 4, 4, "div", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](95, OrderEntryFormComponent_div_95_Template, 3, 1, "div", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "label", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](98, "CCOE Weight");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "input", 40, 41);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function OrderEntryFormComponent_Template_input_ngModelChange_100_listener($event) {
              return ctx.truckData.ccoe_wt = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](102, OrderEntryFormComponent_div_102_Template, 2, 1, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "label", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "Fire Extinguisher Date");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](105, OrderEntryFormComponent_div_105_Template, 4, 4, "div", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](106, OrderEntryFormComponent_div_106_Template, 3, 1, "div", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "label", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](109, "Qty To be Loaded");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "div", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "input", 42, 43);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function OrderEntryFormComponent_Template_input_ngModelChange_111_listener($event) {
              return ctx.truckData.qty_to_be_loaded = $event;
            })("blur", function OrderEntryFormComponent_Template_input_blur_111_listener() {
              return ctx.checkValue(ctx.truckData.qty_to_be_loaded);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](113, OrderEntryFormComponent_div_113_Template, 2, 1, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](114, "span", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "label", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](116, "Customer Order No");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "div", 44);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "input", 45, 46);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function OrderEntryFormComponent_Template_input_ngModelChange_118_listener($event) {
              return ctx.truckData.customerOrderNo = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "div", 47);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "button", 48);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](123, "Book");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "button", 49);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](125, "Update Record");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](126, "button", 50);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function OrderEntryFormComponent_Template_button_click_126_listener() {
              return ctx.resetForm();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](127, "Reset");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](128, "br");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            var _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](12);

            var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](33);

            var _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](35);

            var _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](46);

            var _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](57);

            var _r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](68);

            var _r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](79);

            var _r25 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](90);

            var _r29 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](101);

            var _r33 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](112);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.extraDetials.card_num);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r1.invalid && (_r1.dirty || _r1.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("innerHTML", ctx.cardMessage, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeHtml"]);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("checked", ctx.loadingStatus);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](50, _c0, ctx.loadingStatus));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("checked", !ctx.loadingStatus);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](52, _c0, !ctx.loadingStatus));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.truckList)("searchKeyword", ctx.keyword)("itemTemplate", _r3)("notFoundTemplate", _r5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("innerHTML", ctx.truckMessage, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeHtml"]);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", (ctx.truckData == null ? null : ctx.truckData.srv_due_date) < ctx.toDayDate && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](54, _c1));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.truckData == null ? null : ctx.truckData.srv_due_date) >= ctx.toDayDate);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.truckData == null ? null : ctx.truckData.srv_due_date) < ctx.toDayDate);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.customer_code);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r9.invalid && (_r9.dirty || _r9.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", (ctx.truckData == null ? null : ctx.truckData.hydro_test_due_date) < ctx.toDayDate && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](55, _c1));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.truckData == null ? null : ctx.truckData.hydro_test_due_date) >= ctx.toDayDate);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.truckData == null ? null : ctx.truckData.hydro_test_due_date) < ctx.toDayDate);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.product_code);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r13.invalid && (_r13.dirty || _r13.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", (ctx.truckData == null ? null : ctx.truckData.road_permit_date) < ctx.toDayDate && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](56, _c1));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.truckData == null ? null : ctx.truckData.road_permit_date) >= ctx.toDayDate);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.truckData == null ? null : ctx.truckData.road_permit_date) < ctx.toDayDate);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.tare_wt);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r17.invalid && (_r17.dirty || _r17.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", (ctx.truckData == null ? null : ctx.truckData.insurance_due_date) < ctx.toDayDate && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](57, _c1));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.truckData == null ? null : ctx.truckData.insurance_due_date) >= ctx.toDayDate);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.truckData == null ? null : ctx.truckData.insurance_due_date) < ctx.toDayDate);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.max_gross_wt);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r21.invalid && (_r21.dirty || _r21.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", (ctx.truckData == null ? null : ctx.truckData.ccoe_licence_date) < ctx.toDayDate && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](58, _c1));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.truckData == null ? null : ctx.truckData.ccoe_licence_date) >= ctx.toDayDate);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.truckData == null ? null : ctx.truckData.ccoe_licence_date) < ctx.toDayDate);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.target_wt);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r25.invalid && (_r25.dirty || _r25.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", (ctx.truckData == null ? null : ctx.truckData.fc_due_date) < ctx.toDayDate && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](59, _c1));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.truckData == null ? null : ctx.truckData.fc_due_date) >= ctx.toDayDate);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.truckData == null ? null : ctx.truckData.fc_due_date) < ctx.toDayDate);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.ccoe_wt);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r29.invalid && (_r29.dirty || _r29.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", (ctx.truckData == null ? null : ctx.truckData.fire_extinguisher_date) < ctx.toDayDate && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](60, _c1));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.truckData == null ? null : ctx.truckData.fire_extinguisher_date) >= ctx.toDayDate);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (ctx.truckData == null ? null : ctx.truckData.fire_extinguisher_date) < ctx.toDayDate);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", ctx.unloadingStatus)("ngModel", ctx.truckData.qty_to_be_loaded);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r33.invalid && (_r33.dirty || _r33.touched));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("innerHTML", ctx.divMessages, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeHtml"]);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.truckData.customerOrderNo);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["PatternValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["MaxLengthValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["MinLengthValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgClass"], angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_7__["AutocompleteComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgStyle"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NumberValueAccessor"]],
        pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_6__["DatePipe"]],
        styles: [".text-danger[_ngcontent-%COMP%] {\n  margin-top: 0 !important;\n  font-size: 7px !important;\n}\n\nbutton[_ngcontent-%COMP%] {\n  padding: 12px;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  font-size: 11px;\n}\n\n.required[_ngcontent-%COMP%]:after {\n  content: \" *\";\n  font-size: 20px;\n  color: red;\n}\n\n.header[_ngcontent-%COMP%] {\n  text-align: center;\n  margin: 20px;\n  font-weight: 500;\n  padding-top: 10px;\n  padding-bottom: 10px;\n}\n\n.form-control[_ngcontent-%COMP%] {\n  width: 85% !important;\n}\n\n.input-group-size[_ngcontent-%COMP%] {\n  width: 85% !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFwuLlxcLi5cXC4uXFxvcmRlci1lbnRyeS1mb3JtLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksd0JBQXdCO0VBQ3hCLHlCQUF3QjtBQUM1Qjs7QUFDQTtFQUNJLGFBQWE7RUFDYiwwQkFBa0I7RUFBbEIsdUJBQWtCO0VBQWxCLGtCQUFrQjtFQUFDLGVBQWU7QUFHdEM7O0FBREE7RUFDSSxhQUFZO0VBQ1osZUFBZTtFQUNmLFVBQVU7QUFJZDs7QUFGQTtFQUVFLGtCQUFrQjtFQUVsQixZQUFZO0VBRVosZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixvQkFBb0I7QUFFdEI7O0FBQ0E7RUFFQSxxQkFBbUI7QUFDbkI7O0FBRUE7RUFDSSxxQkFBb0I7QUFDeEIiLCJmaWxlIjoib3JkZXItZW50cnktZm9ybS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50ZXh0LWRhbmdlciB7XHJcbiAgICBtYXJnaW4tdG9wOiAwICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXNpemU6N3B4ICFpbXBvcnRhbnQ7XHJcbn1cclxuYnV0dG9uIHtcclxuICAgIHBhZGRpbmc6IDEycHg7XHJcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7Zm9udC1zaXplOiAxMXB4O1xyXG59XHJcbi5yZXF1aXJlZDphZnRlciB7XHJcbiAgICBjb250ZW50OlwiICpcIjtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGNvbG9yOiByZWQ7XHJcbiAgfVxyXG4uaGVhZGVyXHJcbntcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgLy8gYmFja2dyb3VuZC1jb2xvcjojNWE4ZWQyO1xyXG4gIG1hcmdpbjogMjBweDtcclxuICAvLyBjb2xvcjogb3JhbmdlO1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbn1cclxuXHJcbi5mb3JtLWNvbnRyb2xcclxue1xyXG53aWR0aDo4NSUhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uaW5wdXQtZ3JvdXAtc2l6ZXtcclxuICAgIHdpZHRoOiA4NSUhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubG9hZGluZ0xhYmxlXHJcbntcclxuICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiM1MTJkYTggO1xyXG4gIC8vIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuXHJcbiJdfQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](OrderEntryFormComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-order-entry-form',
            templateUrl: './order-entry-form.component.html',
            styleUrls: ['./order-entry-form.component.scss']
          }]
        }], function () {
          return [{
            type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
          }, {
            type: _app_services_base_service_service__WEBPACK_IMPORTED_MODULE_4__["BaseServiceService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "d8wM":
    /*!***************************************************************!*\
      !*** ./src/app/content/pages/main-block/main-block.module.ts ***!
      \***************************************************************/

    /*! exports provided: MainBlockModule */

    /***/
    function d8wM(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MainBlockModule", function () {
        return MainBlockModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _entry_form_entry_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./entry-form/entry-form.component */
      "ekZ8");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var angular_datatables__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! angular-datatables */
      "njyG");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ng-bootstrap/ng-bootstrap */
      "1kSV");
      /* harmony import */


      var angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! angular-ng-autocomplete */
      "bH2/");
      /* harmony import */


      var _entry_form_truck_entry_form_truck_entry_form_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./entry-form/truck-entry-form/truck-entry-form.component */
      "K0QA");
      /* harmony import */


      var _entry_form_order_entry_form_order_entry_form_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./entry-form/order-entry-form/order-entry-form.component */
      "L8iN");
      /* harmony import */


      var ngx_mask__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ngx-mask */
      "tmjD");
      /* harmony import */


      var _customer_create_customer_create_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ./customer-create/customer-create.component */
      "HBhd");
      /* harmony import */


      var _product_create_product_create_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! ./product-create/product-create.component */
      "GWr9");
      /* harmony import */


      var _card_create_card_create_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! ./card-create/card-create.component */
      "43Sd");

      var appsRoutes = [// {
      //   path: 'entry-form',
      //   component: EntryFormComponent
      // },
      {
        path: 'truck-entry-form',
        component: _entry_form_truck_entry_form_truck_entry_form_component__WEBPACK_IMPORTED_MODULE_8__["TruckEntryFormComponent"]
      }, {
        path: 'order-form',
        component: _entry_form_order_entry_form_order_entry_form_component__WEBPACK_IMPORTED_MODULE_9__["OrderEntryFormComponent"]
      }, {
        path: 'customer-create',
        component: _customer_create_customer_create_component__WEBPACK_IMPORTED_MODULE_11__["CustomerCreateComponent"]
      }, {
        path: 'product-create',
        component: _product_create_product_create_component__WEBPACK_IMPORTED_MODULE_12__["ProductCreateComponent"]
      }, {
        path: 'card-create',
        component: _card_create_card_create_component__WEBPACK_IMPORTED_MODULE_13__["CardCreateComponent"]
      }, {
        path: '**',
        component: _entry_form_truck_entry_form_truck_entry_form_component__WEBPACK_IMPORTED_MODULE_8__["TruckEntryFormComponent"]
      }];

      var MainBlockModule = function MainBlockModule() {
        _classCallCheck(this, MainBlockModule);
      };

      MainBlockModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: MainBlockModule
      });
      MainBlockModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function MainBlockModule_Factory(t) {
          return new (t || MainBlockModule)();
        },
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModule"], angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_7__["AutocompleteLibModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(appsRoutes), ngx_mask__WEBPACK_IMPORTED_MODULE_10__["NgxMaskModule"], angular_datatables__WEBPACK_IMPORTED_MODULE_4__["DataTablesModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](MainBlockModule, {
          declarations: [_entry_form_entry_form_component__WEBPACK_IMPORTED_MODULE_2__["EntryFormComponent"], _entry_form_truck_entry_form_truck_entry_form_component__WEBPACK_IMPORTED_MODULE_8__["TruckEntryFormComponent"], _entry_form_order_entry_form_order_entry_form_component__WEBPACK_IMPORTED_MODULE_9__["OrderEntryFormComponent"], _card_create_card_create_component__WEBPACK_IMPORTED_MODULE_13__["CardCreateComponent"], _customer_create_customer_create_component__WEBPACK_IMPORTED_MODULE_11__["CustomerCreateComponent"], _product_create_product_create_component__WEBPACK_IMPORTED_MODULE_12__["ProductCreateComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModule"], angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_7__["AutocompleteLibModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"], ngx_mask__WEBPACK_IMPORTED_MODULE_10__["NgxMaskModule"], angular_datatables__WEBPACK_IMPORTED_MODULE_4__["DataTablesModule"]],
          exports: [_entry_form_entry_form_component__WEBPACK_IMPORTED_MODULE_2__["EntryFormComponent"], _entry_form_truck_entry_form_truck_entry_form_component__WEBPACK_IMPORTED_MODULE_8__["TruckEntryFormComponent"], _card_create_card_create_component__WEBPACK_IMPORTED_MODULE_13__["CardCreateComponent"], _entry_form_order_entry_form_order_entry_form_component__WEBPACK_IMPORTED_MODULE_9__["OrderEntryFormComponent"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MainBlockModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            declarations: [_entry_form_entry_form_component__WEBPACK_IMPORTED_MODULE_2__["EntryFormComponent"], _entry_form_truck_entry_form_truck_entry_form_component__WEBPACK_IMPORTED_MODULE_8__["TruckEntryFormComponent"], _entry_form_order_entry_form_order_entry_form_component__WEBPACK_IMPORTED_MODULE_9__["OrderEntryFormComponent"], _card_create_card_create_component__WEBPACK_IMPORTED_MODULE_13__["CardCreateComponent"], _customer_create_customer_create_component__WEBPACK_IMPORTED_MODULE_11__["CustomerCreateComponent"], _product_create_product_create_component__WEBPACK_IMPORTED_MODULE_12__["ProductCreateComponent"]],
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModule"], angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_7__["AutocompleteLibModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(appsRoutes), ngx_mask__WEBPACK_IMPORTED_MODULE_10__["NgxMaskModule"], angular_datatables__WEBPACK_IMPORTED_MODULE_4__["DataTablesModule"]],
            exports: [_entry_form_entry_form_component__WEBPACK_IMPORTED_MODULE_2__["EntryFormComponent"], _entry_form_truck_entry_form_truck_entry_form_component__WEBPACK_IMPORTED_MODULE_8__["TruckEntryFormComponent"], _card_create_card_create_component__WEBPACK_IMPORTED_MODULE_13__["CardCreateComponent"], _entry_form_order_entry_form_order_entry_form_component__WEBPACK_IMPORTED_MODULE_9__["OrderEntryFormComponent"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "ekZ8":
    /*!*****************************************************************************!*\
      !*** ./src/app/content/pages/main-block/entry-form/entry-form.component.ts ***!
      \*****************************************************************************/

    /*! exports provided: EntryFormComponent */

    /***/
    function ekZ8(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EntryFormComponent", function () {
        return EntryFormComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ng-bootstrap/ng-bootstrap */
      "1kSV");
      /* harmony import */


      var _truck_entry_form_truck_entry_form_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./truck-entry-form/truck-entry-form.component */
      "K0QA");
      /* harmony import */


      var _order_entry_form_order_entry_form_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./order-entry-form/order-entry-form.component */
      "L8iN");

      function EntryFormComponent_ng_template_8_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-truck-entry-form");
        }
      }

      function EntryFormComponent_ng_template_12_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-order-entry-form");
        }
      }

      var EntryFormComponent = /*#__PURE__*/function () {
        function EntryFormComponent() {
          _classCallCheck(this, EntryFormComponent);

          this.snippets = '';
          this.navsAlignment = 'start';
          this.navsOrientation = 'horizontal';
          this.active = 1;
        }

        _createClass(EntryFormComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return EntryFormComponent;
      }();

      EntryFormComponent.ɵfac = function EntryFormComponent_Factory(t) {
        return new (t || EntryFormComponent)();
      };

      EntryFormComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: EntryFormComponent,
        selectors: [["app-entry-form"]],
        decls: 14,
        vars: 4,
        consts: [[1, "row", "m-4"], [1, "card", "col-12"], [1, "card-body", "pt-3"], ["ngbNav", "", "demoTitle", "Pills Tabs", 1, "nav-tabs", "justify-content-around", 2, "border-bottom", "1px solid #512da8 !important", 3, "activeId", "activeIdChange"], ["nav", "ngbNav"], [3, "ngbNavItem"], ["ngbNavLink", ""], ["ngbNavContent", ""], [1, "mt-2", 3, "ngbNavOutlet"]],
        template: function EntryFormComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "ul", 3, 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("activeIdChange", function EntryFormComponent_Template_ul_activeIdChange_3_listener($event) {
              return ctx.active = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "li", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "a", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Truck Master Form");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, EntryFormComponent_ng_template_8_Template, 1, 0, "ng-template", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "li", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "a", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Customer Order Entry");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, EntryFormComponent_ng_template_12_Template, 1, 0, "ng-template", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("activeId", ctx.active);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngbNavItem", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngbNavItem", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngbNavOutlet", _r0);
          }
        },
        directives: [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbNav"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbNavItem"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbNavLink"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbNavContent"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbNavOutlet"], _truck_entry_form_truck_entry_form_component__WEBPACK_IMPORTED_MODULE_2__["TruckEntryFormComponent"], _order_entry_form_order_entry_form_component__WEBPACK_IMPORTED_MODULE_3__["OrderEntryFormComponent"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJlbnRyeS1mb3JtLmNvbXBvbmVudC5zY3NzIn0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](EntryFormComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-entry-form',
            templateUrl: './entry-form.component.html',
            styleUrls: ['./entry-form.component.scss']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "tmjD":
    /*!*****************************************************************!*\
      !*** ./node_modules/ngx-mask/__ivy_ngcc__/fesm2015/ngx-mask.js ***!
      \*****************************************************************/

    /*! exports provided: INITIAL_CONFIG, MaskApplierService, MaskDirective, MaskPipe, MaskService, NEW_CONFIG, NgxMaskModule, _configFactory, config, initialConfig, timeMasks, withoutValidation */

    /***/
    function tmjD(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "INITIAL_CONFIG", function () {
        return INITIAL_CONFIG;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MaskApplierService", function () {
        return MaskApplierService;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MaskDirective", function () {
        return MaskDirective;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MaskPipe", function () {
        return MaskPipe;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MaskService", function () {
        return MaskService;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NEW_CONFIG", function () {
        return NEW_CONFIG;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NgxMaskModule", function () {
        return NgxMaskModule;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "_configFactory", function () {
        return _configFactory;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "config", function () {
        return config;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "initialConfig", function () {
        return initialConfig;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "timeMasks", function () {
        return timeMasks;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "withoutValidation", function () {
        return withoutValidation;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      var config = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('config');
      var NEW_CONFIG = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('NEW_CONFIG');
      var INITIAL_CONFIG = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('INITIAL_CONFIG');
      var initialConfig = {
        suffix: '',
        prefix: '',
        thousandSeparator: ' ',
        decimalMarker: '.',
        clearIfNotMatch: false,
        showTemplate: false,
        showMaskTyped: false,
        placeHolderCharacter: '_',
        dropSpecialCharacters: true,
        hiddenInput: undefined,
        shownMaskExpression: '',
        separatorLimit: '',
        allowNegativeNumbers: false,
        validation: true,
        // tslint:disable-next-line: quotemark
        specialCharacters: ['-', '/', '(', ')', '.', ':', ' ', '+', ',', '@', '[', ']', '"', "'"],
        leadZeroDateTime: false,
        patterns: {
          '0': {
            pattern: new RegExp('\\d')
          },
          '9': {
            pattern: new RegExp('\\d'),
            optional: true
          },
          X: {
            pattern: new RegExp('\\d'),
            symbol: '*'
          },
          A: {
            pattern: new RegExp('[a-zA-Z0-9]')
          },
          S: {
            pattern: new RegExp('[a-zA-Z]')
          },
          d: {
            pattern: new RegExp('\\d')
          },
          m: {
            pattern: new RegExp('\\d')
          },
          M: {
            pattern: new RegExp('\\d')
          },
          H: {
            pattern: new RegExp('\\d')
          },
          h: {
            pattern: new RegExp('\\d')
          },
          s: {
            pattern: new RegExp('\\d')
          }
        }
      };
      var timeMasks = ['Hh:m0:s0', 'Hh:m0', 'm0:s0'];
      var withoutValidation = ['percent', 'Hh', 's0', 'm0', 'separator', 'd0/M0/0000', 'd0/M0', 'd0', 'M0'];

      var MaskApplierService = /*#__PURE__*/function () {
        function MaskApplierService(_config) {
          var _this15 = this;

          _classCallCheck(this, MaskApplierService);

          this._config = _config;
          this.maskExpression = '';
          this.actualValue = '';
          this.shownMaskExpression = '';

          this._formatWithSeparators = function (str, thousandSeparatorChar, decimalChar, precision) {
            var x = str.split(decimalChar);
            var decimals = x.length > 1 ? "".concat(decimalChar).concat(x[1]) : '';
            var res = x[0];

            var separatorLimit = _this15.separatorLimit.replace(/\s/g, '');

            if (separatorLimit && +separatorLimit) {
              if (res[0] === '-') {
                res = "-".concat(res.slice(1, res.length).slice(0, separatorLimit.length));
              } else {
                res = res.slice(0, separatorLimit.length);
              }
            }

            var rgx = /(\d+)(\d{3})/;

            while (thousandSeparatorChar && rgx.test(res)) {
              res = res.replace(rgx, '$1' + thousandSeparatorChar + '$2');
            }

            if (precision === undefined) {
              return res + decimals;
            } else if (precision === 0) {
              return res;
            }

            return res + decimals.substr(0, precision + 1);
          };

          this.percentage = function (str) {
            return Number(str) >= 0 && Number(str) <= 100;
          };

          this.getPrecision = function (maskExpression) {
            var x = maskExpression.split('.');

            if (x.length > 1) {
              return Number(x[x.length - 1]);
            }

            return Infinity;
          };

          this.checkAndRemoveSuffix = function (inputValue) {
            var _a, _b, _c;

            for (var i = ((_a = _this15.suffix) === null || _a === void 0 ? void 0 : _a.length) - 1; i >= 0; i--) {
              var substr = _this15.suffix.substr(i, (_b = _this15.suffix) === null || _b === void 0 ? void 0 : _b.length);

              if (inputValue.includes(substr) && (i - 1 < 0 || !inputValue.includes(_this15.suffix.substr(i - 1, (_c = _this15.suffix) === null || _c === void 0 ? void 0 : _c.length)))) {
                return inputValue.replace(substr, '');
              }
            }

            return inputValue;
          };

          this.checkInputPrecision = function (inputValue, precision, decimalMarker) {
            if (precision < Infinity) {
              var precisionRegEx = new RegExp(_this15._charToRegExpExpression(decimalMarker) + "\\d{".concat(precision, "}.*$"));
              var precisionMatch = inputValue.match(precisionRegEx);

              if (precisionMatch && precisionMatch[0].length - 1 > precision) {
                var diff = precisionMatch[0].length - 1 - precision;
                inputValue = inputValue.substring(0, inputValue.length - diff);
              }

              if (precision === 0 && inputValue.endsWith(decimalMarker)) {
                inputValue = inputValue.substring(0, inputValue.length - 1);
              }
            }

            return inputValue;
          };

          this._shift = new Set();
          this.clearIfNotMatch = this._config.clearIfNotMatch;
          this.dropSpecialCharacters = this._config.dropSpecialCharacters;
          this.maskSpecialCharacters = this._config.specialCharacters;
          this.maskAvailablePatterns = this._config.patterns;
          this.prefix = this._config.prefix;
          this.suffix = this._config.suffix;
          this.thousandSeparator = this._config.thousandSeparator;
          this.decimalMarker = this._config.decimalMarker;
          this.hiddenInput = this._config.hiddenInput;
          this.showMaskTyped = this._config.showMaskTyped;
          this.placeHolderCharacter = this._config.placeHolderCharacter;
          this.validation = this._config.validation;
          this.separatorLimit = this._config.separatorLimit;
          this.allowNegativeNumbers = this._config.allowNegativeNumbers;
          this.leadZeroDateTime = this._config.leadZeroDateTime;
        }

        _createClass(MaskApplierService, [{
          key: "applyMaskWithPattern",
          value: function applyMaskWithPattern(inputValue, maskAndPattern) {
            var _maskAndPattern = _slicedToArray(maskAndPattern, 2),
                mask = _maskAndPattern[0],
                customPattern = _maskAndPattern[1];

            this.customPattern = customPattern;
            return this.applyMask(inputValue, mask);
          }
        }, {
          key: "applyMask",
          value: function applyMask(inputValue, maskExpression) {
            var _this16 = this;

            var position = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
            var justPasted = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
            var backspaced = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
            var cb = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : function () {};

            if (inputValue === undefined || inputValue === null || maskExpression === undefined) {
              return '';
            }

            var cursor = 0;
            var result = '';
            var multi = false;
            var backspaceShift = false;
            var shift = 1;
            var stepBack = false;

            if (inputValue.slice(0, this.prefix.length) === this.prefix) {
              inputValue = inputValue.slice(this.prefix.length, inputValue.length);
            }

            if (!!this.suffix && (inputValue === null || inputValue === void 0 ? void 0 : inputValue.length) > 0) {
              inputValue = this.checkAndRemoveSuffix(inputValue);
            }

            var inputArray = inputValue.toString().split('');

            if (maskExpression === 'IP') {
              this.ipError = !!(inputArray.filter(function (i) {
                return i === '.';
              }).length < 3 && inputArray.length < 7);
              maskExpression = '099.099.099.099';
            }

            var arr = [];

            for (var i = 0; i < inputValue.length; i++) {
              if (inputValue[i].match('\\d')) {
                arr.push(inputValue[i]);
              }
            }

            if (maskExpression === 'CPF_CNPJ') {
              this.cpfCnpjError = !!(arr.length !== 11 && arr.length !== 14);

              if (arr.length > 11) {
                maskExpression = '00.000.000/0000-00';
              } else {
                maskExpression = '000.000.000-00';
              }
            }

            if (maskExpression.startsWith('percent')) {
              if (inputValue.match('[a-z]|[A-Z]') || inputValue.match(/[-!$%^&*()_+|~=`{}\[\]:";'<>?,\/.]/)) {
                inputValue = this._stripToDecimal(inputValue);
                var precision = this.getPrecision(maskExpression);
                inputValue = this.checkInputPrecision(inputValue, precision, this.decimalMarker);
              }

              if (inputValue.indexOf('.') > 0 && !this.percentage(inputValue.substring(0, inputValue.indexOf('.')))) {
                var base = inputValue.substring(0, inputValue.indexOf('.') - 1);
                inputValue = "".concat(base).concat(inputValue.substring(inputValue.indexOf('.'), inputValue.length));
              }

              if (this.percentage(inputValue)) {
                result = inputValue;
              } else {
                result = inputValue.substring(0, inputValue.length - 1);
              }
            } else if (maskExpression.startsWith('separator')) {
              if (inputValue.match('[wа-яА-Я]') || inputValue.match('[ЁёА-я]') || inputValue.match('[a-z]|[A-Z]') || inputValue.match(/[-@#!$%\\^&*()_£¬'+|~=`{}\[\]:";<>.?\/]/) || inputValue.match('[^A-Za-z0-9,]')) {
                inputValue = this._stripToDecimal(inputValue);
              }

              inputValue = inputValue.length > 1 && inputValue[0] === '0' && inputValue[1] !== this.decimalMarker ? inputValue.slice(1, inputValue.length) : inputValue; // TODO: we had different rexexps here for the different cases... but tests dont seam to bother - check this
              //  separator: no COMMA, dot-sep: no SPACE, COMMA OK, comma-sep: no SPACE, COMMA OK

              var thousandSeperatorCharEscaped = this._charToRegExpExpression(this.thousandSeparator);

              var decimalMarkerEscaped = this._charToRegExpExpression(this.decimalMarker);

              var invalidChars = '@#!$%^&*()_+|~=`{}\\[\\]:\\s,\\.";<>?\\/'.replace(thousandSeperatorCharEscaped, '').replace(decimalMarkerEscaped, '');
              var invalidCharRegexp = new RegExp('[' + invalidChars + ']');

              if (inputValue.match(invalidCharRegexp)) {
                inputValue = inputValue.substring(0, inputValue.length - 1);
              }

              var _precision = this.getPrecision(maskExpression);

              inputValue = this.checkInputPrecision(inputValue, _precision, this.decimalMarker);
              var strForSep = inputValue.replace(new RegExp(thousandSeperatorCharEscaped, 'g'), '');
              result = this._formatWithSeparators(strForSep, this.thousandSeparator, this.decimalMarker, _precision);
              var commaShift = result.indexOf(',') - inputValue.indexOf(',');
              var shiftStep = result.length - inputValue.length;

              if (shiftStep > 0 && result[position] !== ',') {
                backspaceShift = true;
                var _shift = 0;

                do {
                  this._shift.add(position + _shift);

                  _shift++;
                } while (_shift < shiftStep);
              } else if (commaShift !== 0 && position > 0 && !(result.indexOf(',') >= position && position > 3) || !(result.indexOf('.') >= position && position > 3) && shiftStep <= 0) {
                this._shift.clear();

                backspaceShift = true;
                shift = shiftStep;
                position += shiftStep;

                this._shift.add(position);
              } else {
                this._shift.clear();
              }
            } else {
              for ( // tslint:disable-next-line
              var _i2 = 0, inputSymbol = inputArray[0]; _i2 < inputArray.length; _i2++, inputSymbol = inputArray[_i2]) {
                if (cursor === maskExpression.length) {
                  break;
                }

                if (this._checkSymbolMask(inputSymbol, maskExpression[cursor]) && maskExpression[cursor + 1] === '?') {
                  result += inputSymbol;
                  cursor += 2;
                } else if (maskExpression[cursor + 1] === '*' && multi && this._checkSymbolMask(inputSymbol, maskExpression[cursor + 2])) {
                  result += inputSymbol;
                  cursor += 3;
                  multi = false;
                } else if (this._checkSymbolMask(inputSymbol, maskExpression[cursor]) && maskExpression[cursor + 1] === '*') {
                  result += inputSymbol;
                  multi = true;
                } else if (maskExpression[cursor + 1] === '?' && this._checkSymbolMask(inputSymbol, maskExpression[cursor + 2])) {
                  result += inputSymbol;
                  cursor += 3;
                } else if (this._checkSymbolMask(inputSymbol, maskExpression[cursor])) {
                  if (maskExpression[cursor] === 'H') {
                    if (Number(inputSymbol) > 2) {
                      cursor += 1;

                      this._shiftStep(maskExpression, cursor, inputArray.length);

                      _i2--;

                      if (this.leadZeroDateTime) {
                        result += '0';
                      }

                      continue;
                    }
                  }

                  if (maskExpression[cursor] === 'h') {
                    if (result === '2' && Number(inputSymbol) > 3) {
                      cursor += 1;
                      _i2--;
                      continue;
                    }
                  }

                  if (maskExpression[cursor] === 'm') {
                    if (Number(inputSymbol) > 5) {
                      cursor += 1;

                      this._shiftStep(maskExpression, cursor, inputArray.length);

                      _i2--;

                      if (this.leadZeroDateTime) {
                        result += '0';
                      }

                      continue;
                    }
                  }

                  if (maskExpression[cursor] === 's') {
                    if (Number(inputSymbol) > 5) {
                      cursor += 1;

                      this._shiftStep(maskExpression, cursor, inputArray.length);

                      _i2--;

                      if (this.leadZeroDateTime) {
                        result += '0';
                      }

                      continue;
                    }
                  }

                  var daysCount = 31;

                  if (maskExpression[cursor] === 'd') {
                    if (Number(inputSymbol) > 3 && this.leadZeroDateTime || Number(inputValue.slice(cursor, cursor + 2)) > daysCount || inputValue[cursor + 1] === '/') {
                      cursor += 1;

                      this._shiftStep(maskExpression, cursor, inputArray.length);

                      _i2--;

                      if (this.leadZeroDateTime) {
                        result += '0';
                      }

                      continue;
                    }
                  }

                  if (maskExpression[cursor] === 'M') {
                    var monthsCount = 12; // mask without day

                    var withoutDays = cursor === 0 && (Number(inputSymbol) > 2 || Number(inputValue.slice(cursor, cursor + 2)) > monthsCount || inputValue[cursor + 1] === '/'); // day<10 && month<12 for input

                    var day1monthInput = inputValue.slice(cursor - 3, cursor - 1).includes('/') && (inputValue[cursor - 2] === '/' && Number(inputValue.slice(cursor - 1, cursor + 1)) > monthsCount && inputValue[cursor] !== '/' || inputValue[cursor] === '/' || inputValue[cursor - 3] === '/' && Number(inputValue.slice(cursor - 2, cursor)) > monthsCount && inputValue[cursor - 1] !== '/' || inputValue[cursor - 1] === '/'); // 10<day<31 && month<12 for input

                    var day2monthInput = Number(inputValue.slice(cursor - 3, cursor - 1)) <= daysCount && !inputValue.slice(cursor - 3, cursor - 1).includes('/') && inputValue[cursor - 1] === '/' && (Number(inputValue.slice(cursor, cursor + 2)) > monthsCount || inputValue[cursor + 1] === '/'); // day<10 && month<12 for paste whole data

                    var day1monthPaste = Number(inputValue.slice(cursor - 3, cursor - 1)) > daysCount && !inputValue.slice(cursor - 3, cursor - 1).includes('/') && !inputValue.slice(cursor - 2, cursor).includes('/') && Number(inputValue.slice(cursor - 2, cursor)) > monthsCount; // 10<day<31 && month<12 for paste whole data

                    var day2monthPaste = Number(inputValue.slice(cursor - 3, cursor - 1)) <= daysCount && !inputValue.slice(cursor - 3, cursor - 1).includes('/') && inputValue[cursor - 1] !== '/' && Number(inputValue.slice(cursor - 1, cursor + 1)) > monthsCount;

                    if (Number(inputSymbol) > 1 && this.leadZeroDateTime || withoutDays || day1monthInput || day2monthInput || day1monthPaste || day2monthPaste) {
                      cursor += 1;

                      this._shiftStep(maskExpression, cursor, inputArray.length);

                      _i2--;

                      if (this.leadZeroDateTime) {
                        result += '0';
                      }

                      continue;
                    }
                  }

                  result += inputSymbol;
                  cursor++;
                } else if (this.maskSpecialCharacters.indexOf(maskExpression[cursor]) !== -1) {
                  result += maskExpression[cursor];
                  cursor++;

                  this._shiftStep(maskExpression, cursor, inputArray.length);

                  _i2--;
                } else if (this.maskSpecialCharacters.indexOf(inputSymbol) > -1 && this.maskAvailablePatterns[maskExpression[cursor]] && this.maskAvailablePatterns[maskExpression[cursor]].optional) {
                  if (!!inputArray[cursor] && maskExpression !== '099.099.099.099' && maskExpression !== '000.000.000-00' && maskExpression !== '00.000.000/0000-00') {
                    result += inputArray[cursor];
                  }

                  cursor++;
                  _i2--;
                } else if (this.maskExpression[cursor + 1] === '*' && this._findSpecialChar(this.maskExpression[cursor + 2]) && this._findSpecialChar(inputSymbol) === this.maskExpression[cursor + 2] && multi) {
                  cursor += 3;
                  result += inputSymbol;
                } else if (this.maskExpression[cursor + 1] === '?' && this._findSpecialChar(this.maskExpression[cursor + 2]) && this._findSpecialChar(inputSymbol) === this.maskExpression[cursor + 2] && multi) {
                  cursor += 3;
                  result += inputSymbol;
                } else if (this.showMaskTyped && this.maskSpecialCharacters.indexOf(inputSymbol) < 0 && inputSymbol !== this.placeHolderCharacter) {
                  stepBack = true;
                }
              }
            }

            if (result.length + 1 === maskExpression.length && this.maskSpecialCharacters.indexOf(maskExpression[maskExpression.length - 1]) !== -1) {
              result += maskExpression[maskExpression.length - 1];
            }

            var newPosition = position + 1;

            while (this._shift.has(newPosition)) {
              shift++;
              newPosition++;
            }

            var actualShift = justPasted ? cursor : this._shift.has(position) ? shift : 0;

            if (stepBack) {
              actualShift--;
            }

            cb(actualShift, backspaceShift);

            if (shift < 0) {
              this._shift.clear();
            }

            var onlySpecial = false;

            if (backspaced) {
              onlySpecial = inputArray.every(function (_char) {
                return _this16.maskSpecialCharacters.includes(_char);
              });
            }

            var res = "".concat(this.prefix).concat(onlySpecial ? '' : result).concat(this.suffix);

            if (result.length === 0) {
              res = "".concat(this.prefix).concat(result);
            }

            return res;
          }
        }, {
          key: "_findSpecialChar",
          value: function _findSpecialChar(inputSymbol) {
            return this.maskSpecialCharacters.find(function (val) {
              return val === inputSymbol;
            });
          }
        }, {
          key: "_checkSymbolMask",
          value: function _checkSymbolMask(inputSymbol, maskSymbol) {
            this.maskAvailablePatterns = this.customPattern ? this.customPattern : this.maskAvailablePatterns;
            return this.maskAvailablePatterns[maskSymbol] && this.maskAvailablePatterns[maskSymbol].pattern && this.maskAvailablePatterns[maskSymbol].pattern.test(inputSymbol);
          }
        }, {
          key: "_stripToDecimal",
          value: function _stripToDecimal(str) {
            var _this17 = this;

            return str.split('').filter(function (i, idx) {
              return i.match('^-?\\d') || i.match('\\s') || i === '.' || i === ',' || i === '-' && idx === 0 && _this17.allowNegativeNumbers;
            }).join('');
          }
        }, {
          key: "_charToRegExpExpression",
          value: function _charToRegExpExpression(_char2) {
            if (_char2) {
              var charsToEscape = '[\\^$.|?*+()';
              return _char2 === ' ' ? '\\s' : charsToEscape.indexOf(_char2) >= 0 ? '\\' + _char2 : _char2;
            }

            return _char2;
          }
        }, {
          key: "_shiftStep",
          value: function _shiftStep(maskExpression, cursor, inputLength) {
            var shiftStep = /[*?]/g.test(maskExpression.slice(0, cursor)) ? inputLength : cursor;

            this._shift.add(shiftStep + this.prefix.length || 0);
          }
        }]);

        return MaskApplierService;
      }();

      MaskApplierService.ɵfac = function MaskApplierService_Factory(t) {
        return new (t || MaskApplierService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](config));
      };

      MaskApplierService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: MaskApplierService,
        factory: MaskApplierService.ɵfac
      });

      MaskApplierService.ctorParameters = function () {
        return [{
          type: undefined,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
            args: [config]
          }]
        }];
      };
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MaskApplierService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
        }], function () {
          return [{
            type: undefined,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
              args: [config]
            }]
          }];
        }, null);
      })();

      var MaskService = /*#__PURE__*/function (_MaskApplierService) {
        _inherits(MaskService, _MaskApplierService);

        var _super = _createSuper(MaskService);

        function MaskService(document, _config, _elementRef, _renderer) {
          var _this18;

          _classCallCheck(this, MaskService);

          _this18 = _super.call(this, _config);
          _this18.document = document;
          _this18._config = _config;
          _this18._elementRef = _elementRef;
          _this18._renderer = _renderer;
          _this18.maskExpression = '';
          _this18.isNumberValue = false;
          _this18.placeHolderCharacter = '_';
          _this18.maskIsShown = '';
          _this18.selStart = null;
          _this18.selEnd = null;
          /**
           * Whether we are currently in writeValue function, in this case when applying the mask we don't want to trigger onChange function,
           * since writeValue should be a one way only process of writing the DOM value based on the Angular model value.
           */

          _this18.writingValue = false;

          _this18.onChange = function (_) {};

          return _this18;
        } // tslint:disable-next-line:cyclomatic-complexity


        _createClass(MaskService, [{
          key: "applyMask",
          value: function applyMask(inputValue, maskExpression) {
            var _this19 = this;

            var position = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
            var justPasted = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
            var backspaced = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
            var cb = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : function () {};

            if (!maskExpression) {
              return inputValue;
            }

            this.maskIsShown = this.showMaskTyped ? this.showMaskInInput() : '';

            if (this.maskExpression === 'IP' && this.showMaskTyped) {
              this.maskIsShown = this.showMaskInInput(inputValue || '#');
            }

            if (this.maskExpression === 'CPF_CNPJ' && this.showMaskTyped) {
              this.maskIsShown = this.showMaskInInput(inputValue || '#');
            }

            if (!inputValue && this.showMaskTyped) {
              this.formControlResult(this.prefix);
              return this.prefix + this.maskIsShown;
            }

            var getSymbol = !!inputValue && typeof this.selStart === 'number' ? inputValue[this.selStart] : '';
            var newInputValue = '';

            if (this.hiddenInput !== undefined) {
              var actualResult = this.actualValue.split(''); // tslint:disable no-unused-expression

              inputValue !== '' && actualResult.length ? typeof this.selStart === 'number' && typeof this.selEnd === 'number' ? inputValue.length > actualResult.length ? actualResult.splice(this.selStart, 0, getSymbol) : inputValue.length < actualResult.length ? actualResult.length - inputValue.length === 1 ? actualResult.splice(this.selStart - 1, 1) : actualResult.splice(this.selStart, this.selEnd - this.selStart) : null : null : actualResult = []; // tslint:enable no-unused-expression

              newInputValue = this.actualValue.length ? this.shiftTypedSymbols(actualResult.join('')) : inputValue;
            }

            newInputValue = Boolean(newInputValue) && newInputValue.length ? newInputValue : inputValue;

            var result = _get(_getPrototypeOf(MaskService.prototype), "applyMask", this).call(this, newInputValue, maskExpression, position, justPasted, backspaced, cb);

            this.actualValue = this.getActualValue(result); // handle some separator implications:
            // a.) adjust decimalMarker default (. -> ,) if thousandSeparator is a dot

            if (this.thousandSeparator === '.' && this.decimalMarker === '.') {
              this.decimalMarker = ',';
            } // b) remove decimal marker from list of special characters to mask


            if (this.maskExpression.startsWith('separator') && this.dropSpecialCharacters === true) {
              this.maskSpecialCharacters = this.maskSpecialCharacters.filter(function (item) {
                return item !== _this19.decimalMarker;
              });
            }

            this.formControlResult(result);

            if (!this.showMaskTyped) {
              if (this.hiddenInput) {
                return result && result.length ? this.hideInput(result, this.maskExpression) : result;
              }

              return result;
            }

            var resLen = result.length;
            var prefNmask = this.prefix + this.maskIsShown;

            if (this.maskExpression.includes('H')) {
              var countSkipedSymbol = this._numberSkipedSymbols(result);

              return result + prefNmask.slice(resLen + countSkipedSymbol);
            } else if (this.maskExpression === 'IP' || this.maskExpression === 'CPF_CNPJ') {
              return result + prefNmask;
            }

            return result + prefNmask.slice(resLen);
          } // get the number of characters that were shifted

        }, {
          key: "_numberSkipedSymbols",
          value: function _numberSkipedSymbols(value) {
            var regex = /(^|\D)(\d\D)/g;
            var match = regex.exec(value);
            var countSkipedSymbol = 0;

            while (match != null) {
              countSkipedSymbol += 1;
              match = regex.exec(value);
            }

            return countSkipedSymbol;
          }
        }, {
          key: "applyValueChanges",
          value: function applyValueChanges() {
            var position = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
            var justPasted = arguments.length > 1 ? arguments[1] : undefined;
            var backspaced = arguments.length > 2 ? arguments[2] : undefined;
            var cb = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : function () {};
            var formElement = this._elementRef.nativeElement;
            formElement.value = this.applyMask(formElement.value, this.maskExpression, position, justPasted, backspaced, cb);

            if (formElement === this.document.activeElement) {
              return;
            }

            this.clearIfNotMatchFn();
          }
        }, {
          key: "hideInput",
          value: function hideInput(inputValue, maskExpression) {
            var _this20 = this;

            return inputValue.split('').map(function (curr, index) {
              if (_this20.maskAvailablePatterns && _this20.maskAvailablePatterns[maskExpression[index]] && _this20.maskAvailablePatterns[maskExpression[index]].symbol) {
                return _this20.maskAvailablePatterns[maskExpression[index]].symbol;
              }

              return curr;
            }).join('');
          } // this function is not necessary, it checks result against maskExpression

        }, {
          key: "getActualValue",
          value: function getActualValue(res) {
            var _this21 = this;

            var compare = res.split('').filter(function (symbol, i) {
              return _this21._checkSymbolMask(symbol, _this21.maskExpression[i]) || _this21.maskSpecialCharacters.includes(_this21.maskExpression[i]) && symbol === _this21.maskExpression[i];
            });

            if (compare.join('') === res) {
              return compare.join('');
            }

            return res;
          }
        }, {
          key: "shiftTypedSymbols",
          value: function shiftTypedSymbols(inputValue) {
            var _this22 = this;

            var symbolToReplace = '';
            var newInputValue = inputValue && inputValue.split('').map(function (currSymbol, index) {
              if (_this22.maskSpecialCharacters.includes(inputValue[index + 1]) && inputValue[index + 1] !== _this22.maskExpression[index + 1]) {
                symbolToReplace = currSymbol;
                return inputValue[index + 1];
              }

              if (symbolToReplace.length) {
                var replaceSymbol = symbolToReplace;
                symbolToReplace = '';
                return replaceSymbol;
              }

              return currSymbol;
            }) || [];
            return newInputValue.join('');
          }
        }, {
          key: "showMaskInInput",
          value: function showMaskInInput(inputVal) {
            if (this.showMaskTyped && !!this.shownMaskExpression) {
              if (this.maskExpression.length !== this.shownMaskExpression.length) {
                throw new Error('Mask expression must match mask placeholder length');
              } else {
                return this.shownMaskExpression;
              }
            } else if (this.showMaskTyped) {
              if (inputVal) {
                if (this.maskExpression === 'IP') {
                  return this._checkForIp(inputVal);
                }

                if (this.maskExpression === 'CPF_CNPJ') {
                  return this._checkForCpfCnpj(inputVal);
                }
              }

              return this.maskExpression.replace(/\w/g, this.placeHolderCharacter);
            }

            return '';
          }
        }, {
          key: "clearIfNotMatchFn",
          value: function clearIfNotMatchFn() {
            var formElement = this._elementRef.nativeElement;

            if (this.clearIfNotMatch && this.prefix.length + this.maskExpression.length + this.suffix.length !== formElement.value.replace(/_/g, '').length) {
              this.formElementProperty = ['value', ''];
              this.applyMask(formElement.value, this.maskExpression);
            }
          }
        }, {
          key: "checkSpecialCharAmount",
          value: function checkSpecialCharAmount(mask) {
            var _this23 = this;

            var chars = mask.split('').filter(function (item) {
              return _this23._findSpecialChar(item);
            });
            return chars.length;
          }
        }, {
          key: "removeMask",
          value: function removeMask(inputValue) {
            return this._removeMask(this._removeSuffix(this._removePrefix(inputValue)), this.maskSpecialCharacters.concat('_').concat(this.placeHolderCharacter));
          }
        }, {
          key: "_checkForIp",
          value: function _checkForIp(inputVal) {
            if (inputVal === '#') {
              return "".concat(this.placeHolderCharacter, ".").concat(this.placeHolderCharacter, ".").concat(this.placeHolderCharacter, ".").concat(this.placeHolderCharacter);
            }

            var arr = [];

            for (var i = 0; i < inputVal.length; i++) {
              if (inputVal[i].match('\\d')) {
                arr.push(inputVal[i]);
              }
            }

            if (arr.length <= 3) {
              return "".concat(this.placeHolderCharacter, ".").concat(this.placeHolderCharacter, ".").concat(this.placeHolderCharacter);
            }

            if (arr.length > 3 && arr.length <= 6) {
              return "".concat(this.placeHolderCharacter, ".").concat(this.placeHolderCharacter);
            }

            if (arr.length > 6 && arr.length <= 9) {
              return this.placeHolderCharacter;
            }

            if (arr.length > 9 && arr.length <= 12) {
              return '';
            }

            return '';
          }
        }, {
          key: "_checkForCpfCnpj",
          value: function _checkForCpfCnpj(inputVal) {
            var cpf = "".concat(this.placeHolderCharacter).concat(this.placeHolderCharacter).concat(this.placeHolderCharacter) + ".".concat(this.placeHolderCharacter).concat(this.placeHolderCharacter).concat(this.placeHolderCharacter) + ".".concat(this.placeHolderCharacter).concat(this.placeHolderCharacter).concat(this.placeHolderCharacter) + "-".concat(this.placeHolderCharacter).concat(this.placeHolderCharacter);
            var cnpj = "".concat(this.placeHolderCharacter).concat(this.placeHolderCharacter) + ".".concat(this.placeHolderCharacter).concat(this.placeHolderCharacter).concat(this.placeHolderCharacter) + ".".concat(this.placeHolderCharacter).concat(this.placeHolderCharacter).concat(this.placeHolderCharacter) + "/".concat(this.placeHolderCharacter).concat(this.placeHolderCharacter).concat(this.placeHolderCharacter).concat(this.placeHolderCharacter) + "-".concat(this.placeHolderCharacter).concat(this.placeHolderCharacter);

            if (inputVal === '#') {
              return cpf;
            }

            var arr = [];

            for (var i = 0; i < inputVal.length; i++) {
              if (inputVal[i].match('\\d')) {
                arr.push(inputVal[i]);
              }
            }

            if (arr.length <= 3) {
              return cpf.slice(arr.length, cpf.length);
            }

            if (arr.length > 3 && arr.length <= 6) {
              return cpf.slice(arr.length + 1, cpf.length);
            }

            if (arr.length > 6 && arr.length <= 9) {
              return cpf.slice(arr.length + 2, cpf.length);
            }

            if (arr.length > 9 && arr.length < 11) {
              return cpf.slice(arr.length + 3, cpf.length);
            }

            if (arr.length === 11) {
              return '';
            }

            if (arr.length === 12) {
              if (inputVal.length === 17) {
                return cnpj.slice(16, cnpj.length);
              }

              return cnpj.slice(15, cnpj.length);
            }

            if (arr.length > 12 && arr.length <= 14) {
              return cnpj.slice(arr.length + 4, cnpj.length);
            }

            return '';
          }
          /**
           * Propogates the input value back to the Angular model by triggering the onChange function. It won't do this if writingValue
           * is true. If that is true it means we are currently in the writeValue function, which is supposed to only update the actual
           * DOM element based on the Angular model value. It should be a one way process, i.e. writeValue should not be modifying the Angular
           * model value too. Therefore, we don't trigger onChange in this scenario.
           * @param inputValue the current form input value
           */

        }, {
          key: "formControlResult",
          value: function formControlResult(inputValue) {
            if (this.writingValue) {
              return;
            }

            if (Array.isArray(this.dropSpecialCharacters)) {
              this.onChange(this._toNumber(this._removeMask(this._removeSuffix(this._removePrefix(inputValue)), this.dropSpecialCharacters)));
            } else if (this.dropSpecialCharacters) {
              this.onChange(this._toNumber(this._checkSymbols(inputValue)));
            } else {
              this.onChange(this._removeSuffix(inputValue));
            }
          }
        }, {
          key: "_toNumber",
          value: function _toNumber(value) {
            if (!this.isNumberValue) {
              return value;
            }

            var num = Number(value);
            return Number.isNaN(num) ? value : num;
          }
        }, {
          key: "_removeMask",
          value: function _removeMask(value, specialCharactersForRemove) {
            return value ? value.replace(this._regExpForRemove(specialCharactersForRemove), '') : value;
          }
        }, {
          key: "_removePrefix",
          value: function _removePrefix(value) {
            if (!this.prefix) {
              return value;
            }

            return value ? value.replace(this.prefix, '') : value;
          }
        }, {
          key: "_removeSuffix",
          value: function _removeSuffix(value) {
            if (!this.suffix) {
              return value;
            }

            return value ? value.replace(this.suffix, '') : value;
          }
        }, {
          key: "_retrieveSeparatorValue",
          value: function _retrieveSeparatorValue(result) {
            return this._removeMask(this._removeSuffix(this._removePrefix(result)), this.maskSpecialCharacters);
          }
        }, {
          key: "_regExpForRemove",
          value: function _regExpForRemove(specialCharactersForRemove) {
            return new RegExp(specialCharactersForRemove.map(function (item) {
              return "\\".concat(item);
            }).join('|'), 'gi');
          }
        }, {
          key: "_checkSymbols",
          value: function _checkSymbols(result) {
            if (result === '') {
              return result;
            }

            var separatorPrecision = this._retrieveSeparatorPrecision(this.maskExpression);

            var separatorValue = this._retrieveSeparatorValue(result);

            if (this.decimalMarker !== '.') {
              separatorValue = separatorValue.replace(this.decimalMarker, '.');
            }

            if (!this.isNumberValue) {
              return separatorValue;
            }

            if (separatorPrecision) {
              if (result === this.decimalMarker) {
                return null;
              }

              return this._checkPrecision(this.maskExpression, separatorValue);
            } else {
              return Number(separatorValue);
            }
          } // TODO should think about helpers or separting decimal precision to own property

        }, {
          key: "_retrieveSeparatorPrecision",
          value: function _retrieveSeparatorPrecision(maskExpretion) {
            var matcher = maskExpretion.match(new RegExp("^separator\\.([^d]*)"));
            return matcher ? Number(matcher[1]) : null;
          }
        }, {
          key: "_checkPrecision",
          value: function _checkPrecision(separatorExpression, separatorValue) {
            if (separatorExpression.indexOf('2') > 0) {
              return Number(separatorValue).toFixed(2);
            }

            return Number(separatorValue);
          }
        }, {
          key: "formElementProperty",
          set: function set(_ref) {
            var _this24 = this;

            var _ref2 = _slicedToArray(_ref, 2),
                name = _ref2[0],
                value = _ref2[1];

            Promise.resolve().then(function () {
              return _this24._renderer.setProperty(_this24._elementRef.nativeElement, name, value);
            });
          }
        }]);

        return MaskService;
      }(MaskApplierService);

      MaskService.ɵfac = function MaskService_Factory(t) {
        return new (t || MaskService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](config), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]));
      };

      MaskService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: MaskService,
        factory: MaskService.ɵfac
      });

      MaskService.ctorParameters = function () {
        return [{
          type: undefined,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
            args: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"]]
          }]
        }, {
          type: undefined,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
            args: [config]
          }]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]
        }];
      };
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MaskService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
        }], function () {
          return [{
            type: undefined,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
              args: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"]]
            }]
          }, {
            type: undefined,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
              args: [config]
            }]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]
          }];
        }, null);
      })(); // tslint:disable deprecation
      // tslint:disable no-input-rename


      var MaskDirective = /*#__PURE__*/function () {
        function MaskDirective(document, _maskService, _config) {
          _classCallCheck(this, MaskDirective);

          this.document = document;
          this._maskService = _maskService;
          this._config = _config;
          this.maskExpression = '';
          this.specialCharacters = [];
          this.patterns = {};
          this.prefix = '';
          this.suffix = '';
          this.thousandSeparator = ' ';
          this.decimalMarker = '.';
          this.dropSpecialCharacters = null;
          this.hiddenInput = null;
          this.showMaskTyped = null;
          this.placeHolderCharacter = null;
          this.shownMaskExpression = null;
          this.showTemplate = null;
          this.clearIfNotMatch = null;
          this.validation = null;
          this.separatorLimit = null;
          this.allowNegativeNumbers = null;
          this.leadZeroDateTime = null;
          this._maskValue = '';
          this._position = null;
          this._maskExpressionArray = [];
          this._justPasted = false;

          this.onChange = function (_) {};

          this.onTouch = function () {};
        }

        _createClass(MaskDirective, [{
          key: "ngOnChanges",
          value: function ngOnChanges(changes) {
            var maskExpression = changes.maskExpression,
                specialCharacters = changes.specialCharacters,
                patterns = changes.patterns,
                prefix = changes.prefix,
                suffix = changes.suffix,
                thousandSeparator = changes.thousandSeparator,
                decimalMarker = changes.decimalMarker,
                dropSpecialCharacters = changes.dropSpecialCharacters,
                hiddenInput = changes.hiddenInput,
                showMaskTyped = changes.showMaskTyped,
                placeHolderCharacter = changes.placeHolderCharacter,
                shownMaskExpression = changes.shownMaskExpression,
                showTemplate = changes.showTemplate,
                clearIfNotMatch = changes.clearIfNotMatch,
                validation = changes.validation,
                separatorLimit = changes.separatorLimit,
                allowNegativeNumbers = changes.allowNegativeNumbers,
                leadZeroDateTime = changes.leadZeroDateTime;

            if (maskExpression) {
              this._maskValue = maskExpression.currentValue || '';

              if (maskExpression.currentValue && maskExpression.currentValue.split('||').length > 1) {
                this._maskExpressionArray = maskExpression.currentValue.split('||').sort(function (a, b) {
                  return a.length - b.length;
                });
                this._maskValue = this._maskExpressionArray[0];
                this.maskExpression = this._maskExpressionArray[0];
                this._maskService.maskExpression = this._maskExpressionArray[0];
              }
            }

            if (specialCharacters) {
              if (!specialCharacters.currentValue || !Array.isArray(specialCharacters.currentValue)) {
                return;
              } else {
                this._maskService.maskSpecialCharacters = specialCharacters.currentValue || [];
              }
            } // Only overwrite the mask available patterns if a pattern has actually been passed in


            if (patterns && patterns.currentValue) {
              this._maskService.maskAvailablePatterns = patterns.currentValue;
            }

            if (prefix) {
              this._maskService.prefix = prefix.currentValue;
            }

            if (suffix) {
              this._maskService.suffix = suffix.currentValue;
            }

            if (thousandSeparator) {
              this._maskService.thousandSeparator = thousandSeparator.currentValue;
            }

            if (decimalMarker) {
              this._maskService.decimalMarker = decimalMarker.currentValue;
            }

            if (dropSpecialCharacters) {
              this._maskService.dropSpecialCharacters = dropSpecialCharacters.currentValue;
            }

            if (hiddenInput) {
              this._maskService.hiddenInput = hiddenInput.currentValue;
            }

            if (showMaskTyped) {
              this._maskService.showMaskTyped = showMaskTyped.currentValue;
            }

            if (placeHolderCharacter) {
              this._maskService.placeHolderCharacter = placeHolderCharacter.currentValue;
            }

            if (shownMaskExpression) {
              this._maskService.shownMaskExpression = shownMaskExpression.currentValue;
            }

            if (showTemplate) {
              this._maskService.showTemplate = showTemplate.currentValue;
            }

            if (clearIfNotMatch) {
              this._maskService.clearIfNotMatch = clearIfNotMatch.currentValue;
            }

            if (validation) {
              this._maskService.validation = validation.currentValue;
            }

            if (separatorLimit) {
              this._maskService.separatorLimit = separatorLimit.currentValue;
            }

            if (allowNegativeNumbers) {
              this._maskService.allowNegativeNumbers = allowNegativeNumbers.currentValue;

              if (this._maskService.allowNegativeNumbers) {
                this._maskService.maskSpecialCharacters = this._maskService.maskSpecialCharacters.filter(function (c) {
                  return c !== '-';
                });
              }
            }

            if (leadZeroDateTime) {
              this._maskService.leadZeroDateTime = leadZeroDateTime.currentValue;
            }

            this._applyMask();
          } // tslint:disable-next-line: cyclomatic-complexity

        }, {
          key: "validate",
          value: function validate(_ref3) {
            var _this25 = this;

            var value = _ref3.value;

            if (!this._maskService.validation || !this._maskValue) {
              return null;
            }

            if (this._maskService.ipError) {
              return this._createValidationError(value);
            }

            if (this._maskService.cpfCnpjError) {
              return this._createValidationError(value);
            }

            if (this._maskValue.startsWith('separator')) {
              return null;
            }

            if (withoutValidation.includes(this._maskValue)) {
              return null;
            }

            if (this._maskService.clearIfNotMatch) {
              return null;
            }

            if (timeMasks.includes(this._maskValue)) {
              return this._validateTime(value);
            }

            if (value && value.toString().length >= 1) {
              var counterOfOpt = 0;

              var _loop = function _loop(key) {
                if (_this25._maskService.maskAvailablePatterns[key].optional && _this25._maskService.maskAvailablePatterns[key].optional === true) {
                  if (_this25._maskValue.indexOf(key) !== _this25._maskValue.lastIndexOf(key)) {
                    var opt = _this25._maskValue.split('').filter(function (i) {
                      return i === key;
                    }).join('');

                    counterOfOpt += opt.length;
                  } else if (_this25._maskValue.indexOf(key) !== -1) {
                    counterOfOpt++;
                  }

                  if (_this25._maskValue.indexOf(key) !== -1 && value.toString().length >= _this25._maskValue.indexOf(key)) {
                    return {
                      v: null
                    };
                  }

                  if (counterOfOpt === _this25._maskValue.length) {
                    return {
                      v: null
                    };
                  }
                }
              };

              for (var key in this._maskService.maskAvailablePatterns) {
                var _ret = _loop(key);

                if (typeof _ret === "object") return _ret.v;
              }

              if (this._maskValue.indexOf('{') === 1 && value.toString().length === this._maskValue.length + Number(this._maskValue.split('{')[1].split('}')[0]) - 4) {
                return null;
              }

              if (this._maskValue.indexOf('*') === 1 || this._maskValue.indexOf('?') === 1) {
                return null;
              } else if (this._maskValue.indexOf('*') > 1 && value.toString().length < this._maskValue.indexOf('*') || this._maskValue.indexOf('?') > 1 && value.toString().length < this._maskValue.indexOf('?') || this._maskValue.indexOf('{') === 1) {
                return this._createValidationError(value);
              }

              if (this._maskValue.indexOf('*') === -1 || this._maskValue.indexOf('?') === -1) {
                var length = this._maskService.dropSpecialCharacters ? this._maskValue.length - this._maskService.checkSpecialCharAmount(this._maskValue) - counterOfOpt : this._maskValue.length - counterOfOpt;

                if (value.toString().length < length) {
                  return this._createValidationError(value);
                }
              }
            }

            return null;
          }
        }, {
          key: "onPaste",
          value: function onPaste() {
            this._justPasted = true;
          }
        }, {
          key: "onInput",
          value: function onInput(e) {
            var _this26 = this;

            var el = e.target;
            this._inputValue = el.value;

            this._setMask();

            if (!this._maskValue) {
              this.onChange(el.value);
              return;
            }

            var position = el.selectionStart === 1 ? el.selectionStart + this._maskService.prefix.length : el.selectionStart;
            var caretShift = 0;
            var backspaceShift = false;

            this._maskService.applyValueChanges(position, this._justPasted, this._code === 'Backspace', function (shift, _backspaceShift) {
              _this26._justPasted = false;
              caretShift = shift;
              backspaceShift = _backspaceShift;
            }); // only set the selection if the element is active


            if (this.document.activeElement !== el) {
              return;
            }

            this._position = this._position === 1 && this._inputValue.length === 1 ? null : this._position;
            var positionToApply = this._position ? this._inputValue.length + position + caretShift : position + (this._code === 'Backspace' && !backspaceShift ? 0 : caretShift);

            if (positionToApply > this._getActualInputLength()) {
              positionToApply = this._getActualInputLength();
            }

            el.setSelectionRange(positionToApply, positionToApply);
            this._position = null;
          }
        }, {
          key: "onBlur",
          value: function onBlur() {
            if (this._maskValue) {
              this._maskService.clearIfNotMatchFn();
            }

            this.onTouch();
          }
        }, {
          key: "onFocus",
          value: function onFocus(e) {
            if (!this._maskValue) {
              return;
            }

            var el = e.target;
            var posStart = 0;
            var posEnd = 0;

            if (el !== null && el.selectionStart !== null && el.selectionStart === el.selectionEnd && el.selectionStart > this._maskService.prefix.length && // tslint:disable-next-line
            e.keyCode !== 38) {
              if (this._maskService.showMaskTyped) {
                // We are showing the mask in the input
                this._maskService.maskIsShown = this._maskService.showMaskInInput();

                if (el.setSelectionRange && this._maskService.prefix + this._maskService.maskIsShown === el.value) {
                  // the input ONLY contains the mask, so position the cursor at the start
                  el.focus();
                  el.setSelectionRange(posStart, posEnd);
                } else {
                  // the input contains some characters already
                  if (el.selectionStart > this._maskService.actualValue.length) {
                    // if the user clicked beyond our value's length, position the cursor at the end of our value
                    el.setSelectionRange(this._maskService.actualValue.length, this._maskService.actualValue.length);
                  }
                }
              }
            }

            var nextValue = !el.value || el.value === this._maskService.prefix ? this._maskService.prefix + this._maskService.maskIsShown : el.value;
            /** Fix of cursor position jumping to end in most browsers no matter where cursor is inserted onFocus */

            if (el.value !== nextValue) {
              el.value = nextValue;
            }
            /** fix of cursor position with prefix when mouse click occur */


            if ((el.selectionStart || el.selectionEnd) <= this._maskService.prefix.length) {
              el.selectionStart = this._maskService.prefix.length;
              return;
            }
            /** select only inserted text */


            if (el.selectionEnd > this._getActualInputLength()) {
              el.selectionEnd = this._getActualInputLength();
            }
          } // tslint:disable-next-line: cyclomatic-complexity

        }, {
          key: "onKeyDown",
          value: function onKeyDown(e) {
            var _a;

            if (!this._maskValue) {
              return;
            }

            this._code = e.code ? e.code : e.key;
            var el = e.target;
            this._inputValue = el.value;

            this._setMask();

            if (e.keyCode === 38) {
              e.preventDefault();
            }

            if (e.keyCode === 37 || e.keyCode === 8 || e.keyCode === 46) {
              if (e.keyCode === 8 && el.value.length === 0) {
                el.selectionStart = el.selectionEnd;
              }

              if (e.keyCode === 8 && el.selectionStart !== 0) {
                // If specialChars is false, (shouldn't ever happen) then set to the defaults
                this.specialCharacters = ((_a = this.specialCharacters) === null || _a === void 0 ? void 0 : _a.length) ? this.specialCharacters : this._config.specialCharacters;

                if (this.prefix.length > 1 && el.selectionStart <= this.prefix.length) {
                  el.setSelectionRange(this.prefix.length, this.prefix.length);
                } else {
                  if (this._inputValue.length !== el.selectionStart && el.selectionStart !== 1) {
                    while (this.specialCharacters.includes(this._inputValue[el.selectionStart - 1].toString()) && (this.prefix.length >= 1 && el.selectionStart > this.prefix.length || this.prefix.length === 0)) {
                      el.setSelectionRange(el.selectionStart - 1, el.selectionStart - 1);
                    }
                  }

                  this.suffixCheckOnPressDelete(e.keyCode, el);
                }
              }

              this.suffixCheckOnPressDelete(e.keyCode, el);

              if (this._maskService.prefix.length && el.selectionStart <= this._maskService.prefix.length && el.selectionEnd <= this._maskService.prefix.length) {
                e.preventDefault();
              }

              var cursorStart = el.selectionStart; // this.onFocus(e);

              if (e.keyCode === 8 && !el.readOnly && cursorStart === 0 && el.selectionEnd === el.value.length && el.value.length !== 0) {
                this._position = this._maskService.prefix ? this._maskService.prefix.length : 0;

                this._maskService.applyMask(this._maskService.prefix, this._maskService.maskExpression, this._position);
              }
            }

            if (!!this.suffix && this.suffix.length > 1 && this._inputValue.length - this.suffix.length < el.selectionStart) {
              el.setSelectionRange(this._inputValue.length - this.suffix.length, this._inputValue.length);
            } else if (e.keyCode === 65 && e.ctrlKey === true || // Ctrl+ A
            e.keyCode === 65 && e.metaKey === true // Cmd + A (Mac)
            ) {
                el.setSelectionRange(0, this._getActualInputLength());
                e.preventDefault();
              }

            this._maskService.selStart = el.selectionStart;
            this._maskService.selEnd = el.selectionEnd;
          }
          /** It writes the value in the input */

        }, {
          key: "writeValue",
          value: function writeValue(inputValue) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      if (typeof inputValue === 'object' && inputValue !== null && 'value' in inputValue) {
                        if ('disable' in inputValue) {
                          this.setDisabledState(Boolean(inputValue.disable));
                        }

                        inputValue = inputValue.value;
                      }

                      if (inputValue === undefined) {
                        inputValue = '';
                      }

                      if (typeof inputValue === 'number') {
                        inputValue = String(inputValue);
                        inputValue = this.decimalMarker !== '.' ? inputValue.replace('.', this.decimalMarker) : inputValue;
                        this._maskService.isNumberValue = true;
                      }

                      if (inputValue && this._maskService.maskExpression || this._maskService.maskExpression && (this._maskService.prefix || this._maskService.showMaskTyped)) {
                        // Let the service we know we are writing value so that triggering onChange function wont happen during applyMask
                        this._maskService.writingValue = true;
                        this._maskService.formElementProperty = ['value', this._maskService.applyMask(inputValue, this._maskService.maskExpression)]; // Let the service know we've finished writing value

                        this._maskService.writingValue = false;
                      } else {
                        this._maskService.formElementProperty = ['value', inputValue];
                      }

                      this._inputValue = inputValue;

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "registerOnChange",
          value: function registerOnChange(fn) {
            this.onChange = fn;
            this._maskService.onChange = this.onChange;
          }
        }, {
          key: "registerOnTouched",
          value: function registerOnTouched(fn) {
            this.onTouch = fn;
          }
        }, {
          key: "suffixCheckOnPressDelete",
          value: function suffixCheckOnPressDelete(keyCode, el) {
            if (keyCode === 46 && this.suffix.length > 0) {
              if (this._inputValue.length - this.suffix.length <= el.selectionStart) {
                el.setSelectionRange(this._inputValue.length - this.suffix.length, this._inputValue.length);
              }
            }

            if (keyCode === 8) {
              if (this.suffix.length > 1 && this._inputValue.length - this.suffix.length < el.selectionStart) {
                el.setSelectionRange(this._inputValue.length - this.suffix.length, this._inputValue.length);
              }

              if (this.suffix.length === 1 && this._inputValue.length === el.selectionStart) {
                el.setSelectionRange(el.selectionStart - 1, el.selectionStart - 1);
              }
            }
          }
          /** It disables the input element */

        }, {
          key: "setDisabledState",
          value: function setDisabledState(isDisabled) {
            this._maskService.formElementProperty = ['disabled', isDisabled];
          }
        }, {
          key: "_repeatPatternSymbols",
          value: function _repeatPatternSymbols(maskExp) {
            var _this27 = this;

            return maskExp.match(/{[0-9]+}/) && maskExp.split('').reduce(function (accum, currval, index) {
              _this27._start = currval === '{' ? index : _this27._start;

              if (currval !== '}') {
                return _this27._maskService._findSpecialChar(currval) ? accum + currval : accum;
              }

              _this27._end = index;
              var repeatNumber = Number(maskExp.slice(_this27._start + 1, _this27._end));
              var replaceWith = new Array(repeatNumber + 1).join(maskExp[_this27._start - 1]);
              return accum + replaceWith;
            }, '') || maskExp;
          } // tslint:disable-next-line:no-any

        }, {
          key: "_applyMask",
          value: function _applyMask() {
            this._maskService.maskExpression = this._repeatPatternSymbols(this._maskValue || '');
            this._maskService.formElementProperty = ['value', this._maskService.applyMask(this._inputValue, this._maskService.maskExpression)];
          }
        }, {
          key: "_validateTime",
          value: function _validateTime(value) {
            var rowMaskLen = this._maskValue.split('').filter(function (s) {
              return s !== ':';
            }).length;

            if (value === null || value.length === 0) {
              return null; // Don't validate empty values to allow for optional form control
            }

            if (+value[value.length - 1] === 0 && value.length < rowMaskLen || value.length <= rowMaskLen - 2) {
              return this._createValidationError(value);
            }

            return null;
          }
        }, {
          key: "_getActualInputLength",
          value: function _getActualInputLength() {
            return this._maskService.actualValue.length || this._maskService.actualValue.length + this._maskService.prefix.length;
          }
        }, {
          key: "_createValidationError",
          value: function _createValidationError(actualValue) {
            return {
              mask: {
                requiredMask: this._maskValue,
                actualValue: actualValue
              }
            };
          }
        }, {
          key: "_setMask",
          value: function _setMask() {
            var _this28 = this;

            if (this._maskExpressionArray.length > 0) {
              this._maskExpressionArray.some(function (mask) {
                var test = _this28._maskService.removeMask(_this28._inputValue).length <= _this28._maskService.removeMask(mask).length;

                if (_this28._inputValue && test) {
                  _this28._maskValue = mask;
                  _this28.maskExpression = mask;
                  _this28._maskService.maskExpression = mask;
                  return test;
                } else {
                  _this28._maskValue = _this28._maskExpressionArray[_this28._maskExpressionArray.length - 1];
                  _this28.maskExpression = _this28._maskExpressionArray[_this28._maskExpressionArray.length - 1];
                  _this28._maskService.maskExpression = _this28._maskExpressionArray[_this28._maskExpressionArray.length - 1];
                }
              });
            }
          }
        }]);

        return MaskDirective;
      }();

      MaskDirective.ɵfac = function MaskDirective_Factory(t) {
        return new (t || MaskDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](MaskService), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](config));
      };

      MaskDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineDirective"]({
        type: MaskDirective,
        selectors: [["input", "mask", ""], ["textarea", "mask", ""]],
        hostBindings: function MaskDirective_HostBindings(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("paste", function MaskDirective_paste_HostBindingHandler() {
              return ctx.onPaste();
            })("input", function MaskDirective_input_HostBindingHandler($event) {
              return ctx.onInput($event);
            })("blur", function MaskDirective_blur_HostBindingHandler() {
              return ctx.onBlur();
            })("click", function MaskDirective_click_HostBindingHandler($event) {
              return ctx.onFocus($event);
            })("keydown", function MaskDirective_keydown_HostBindingHandler($event) {
              return ctx.onKeyDown($event);
            });
          }
        },
        inputs: {
          maskExpression: ["mask", "maskExpression"],
          specialCharacters: "specialCharacters",
          patterns: "patterns",
          prefix: "prefix",
          suffix: "suffix",
          thousandSeparator: "thousandSeparator",
          decimalMarker: "decimalMarker",
          dropSpecialCharacters: "dropSpecialCharacters",
          hiddenInput: "hiddenInput",
          showMaskTyped: "showMaskTyped",
          placeHolderCharacter: "placeHolderCharacter",
          shownMaskExpression: "shownMaskExpression",
          showTemplate: "showTemplate",
          clearIfNotMatch: "clearIfNotMatch",
          validation: "validation",
          separatorLimit: "separatorLimit",
          allowNegativeNumbers: "allowNegativeNumbers",
          leadZeroDateTime: "leadZeroDateTime"
        },
        features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵProvidersFeature"]([{
          provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALUE_ACCESSOR"],
          useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () {
            return MaskDirective;
          }),
          multi: true
        }, {
          provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALIDATORS"],
          useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () {
            return MaskDirective;
          }),
          multi: true
        }, MaskService]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵNgOnChangesFeature"]]
      });

      MaskDirective.ctorParameters = function () {
        return [{
          type: undefined,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
            args: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"]]
          }]
        }, {
          type: MaskService
        }, {
          type: undefined,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
            args: [config]
          }]
        }];
      };

      MaskDirective.propDecorators = {
        maskExpression: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
          args: ['mask']
        }],
        specialCharacters: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        patterns: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        prefix: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        suffix: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        thousandSeparator: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        decimalMarker: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        dropSpecialCharacters: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        hiddenInput: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        showMaskTyped: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        placeHolderCharacter: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        shownMaskExpression: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        showTemplate: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        clearIfNotMatch: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        validation: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        separatorLimit: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        allowNegativeNumbers: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        leadZeroDateTime: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        onPaste: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['paste']
        }],
        onInput: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['input', ['$event']]
        }],
        onBlur: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['blur']
        }],
        onFocus: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['click', ['$event']]
        }],
        onKeyDown: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['keydown', ['$event']]
        }]
      };
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MaskDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
          args: [{
            selector: 'input[mask], textarea[mask]',
            providers: [{
              provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALUE_ACCESSOR"],
              useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () {
                return MaskDirective;
              }),
              multi: true
            }, {
              provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALIDATORS"],
              useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () {
                return MaskDirective;
              }),
              multi: true
            }, MaskService]
          }]
        }], function () {
          return [{
            type: undefined,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
              args: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"]]
            }]
          }, {
            type: MaskService
          }, {
            type: undefined,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
              args: [config]
            }]
          }];
        }, {
          maskExpression: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"],
            args: ['mask']
          }],
          specialCharacters: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          patterns: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          prefix: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          suffix: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          thousandSeparator: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          decimalMarker: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          dropSpecialCharacters: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          hiddenInput: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          showMaskTyped: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          placeHolderCharacter: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          shownMaskExpression: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          showTemplate: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          clearIfNotMatch: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          validation: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          separatorLimit: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          allowNegativeNumbers: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          leadZeroDateTime: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          onPaste: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['paste']
          }],
          onInput: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['input', ['$event']]
          }],
          onBlur: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['blur']
          }],
          onFocus: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['click', ['$event']]
          }],
          // tslint:disable-next-line: cyclomatic-complexity
          onKeyDown: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['keydown', ['$event']]
          }]
        });
      })();

      var MaskPipe = /*#__PURE__*/function () {
        function MaskPipe(_maskService) {
          _classCallCheck(this, MaskPipe);

          this._maskService = _maskService;
        }

        _createClass(MaskPipe, [{
          key: "transform",
          value: function transform(value, mask) {
            var thousandSeparator = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

            if (!value && typeof value !== 'number') {
              return '';
            }

            if (thousandSeparator) {
              this._maskService.thousandSeparator = thousandSeparator;
            }

            if (typeof mask === 'string') {
              return this._maskService.applyMask("".concat(value), mask);
            }

            return this._maskService.applyMaskWithPattern("".concat(value), mask);
          }
        }]);

        return MaskPipe;
      }();

      MaskPipe.ɵfac = function MaskPipe_Factory(t) {
        return new (t || MaskPipe)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](MaskApplierService));
      };

      MaskPipe.ɵpipe = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({
        name: "mask",
        type: MaskPipe,
        pure: true
      });

      MaskPipe.ctorParameters = function () {
        return [{
          type: MaskApplierService
        }];
      };
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MaskPipe, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"],
          args: [{
            name: 'mask',
            pure: true
          }]
        }], function () {
          return [{
            type: MaskApplierService
          }];
        }, null);
      })();

      var NgxMaskModule = /*#__PURE__*/function () {
        function NgxMaskModule() {
          _classCallCheck(this, NgxMaskModule);
        }

        _createClass(NgxMaskModule, null, [{
          key: "forRoot",
          value: function forRoot(configValue) {
            return {
              ngModule: NgxMaskModule,
              providers: [{
                provide: NEW_CONFIG,
                useValue: configValue
              }, {
                provide: INITIAL_CONFIG,
                useValue: initialConfig
              }, {
                provide: config,
                useFactory: _configFactory,
                deps: [INITIAL_CONFIG, NEW_CONFIG]
              }, MaskApplierService]
            };
          }
        }, {
          key: "forChild",
          value: function forChild() {
            return {
              ngModule: NgxMaskModule
            };
          }
        }]);

        return NgxMaskModule;
      }();

      NgxMaskModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: NgxMaskModule
      });
      NgxMaskModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function NgxMaskModule_Factory(t) {
          return new (t || NgxMaskModule)();
        }
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](NgxMaskModule, {
          declarations: [MaskDirective, MaskPipe],
          exports: [MaskDirective, MaskPipe]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgxMaskModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            exports: [MaskDirective, MaskPipe],
            declarations: [MaskDirective, MaskPipe]
          }]
        }], null, null);
      })();
      /**
       * @internal
       */


      function _configFactory(initConfig, configValue) {
        return configValue instanceof Function ? Object.assign(Object.assign({}, initConfig), configValue()) : Object.assign(Object.assign({}, initConfig), configValue);
      }

      var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

      (function () {
        if (!commonjsGlobal.KeyboardEvent) {
          commonjsGlobal.KeyboardEvent = function (_eventType, _init) {};
        }
      })();
      /**
       * Generated bundle index. Do not edit.
       */
      //# sourceMappingURL=ngx-mask.js.map

      /***/

    }
  }]);
})();
//# sourceMappingURL=main-block-main-block-module-es5.js.map