(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "+AxC":
/*!************************************************!*\
  !*** ./src/app/services/my-service.service.ts ***!
  \************************************************/
/*! exports provided: MyServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyServiceService", function() { return MyServiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var _sse_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./sse-service.service */ "AB80");




class MyServiceService {
    constructor(_zone, _sseService) {
        this._zone = _zone;
        this._sseService = _sseService;
    }
    getServerSentEvent(url) {
        return rxjs__WEBPACK_IMPORTED_MODULE_1__["Observable"].create(observer => {
            const eventSource = this._sseService.getEventSource(url);
            eventSource.onmessage = event => {
                this._zone.run(() => {
                    observer.next(event);
                });
            };
            eventSource.onerror = error => {
                this._zone.run(() => {
                    observer.error(error);
                });
            };
        });
    }
}
MyServiceService.ɵfac = function MyServiceService_Factory(t) { return new (t || MyServiceService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_sse_service_service__WEBPACK_IMPORTED_MODULE_2__["SseServiceService"])); };
MyServiceService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: MyServiceService, factory: MyServiceService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MyServiceService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"] }, { type: _sse_service_service__WEBPACK_IMPORTED_MODULE_2__["SseServiceService"] }]; }, null); })();


/***/ }),

/***/ "AB80":
/*!*************************************************!*\
  !*** ./src/app/services/sse-service.service.ts ***!
  \*************************************************/
/*! exports provided: SseServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SseServiceService", function() { return SseServiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");


class SseServiceService {
    constructor() { }
    getEventSource(url) {
        return new EventSource(url);
    }
}
SseServiceService.ɵfac = function SseServiceService_Factory(t) { return new (t || SseServiceService)(); };
SseServiceService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: SseServiceService, factory: SseServiceService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SseServiceService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();


/***/ })

}]);
//# sourceMappingURL=common-es2015.js.map