(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["roleManagment-add-role-add-role-module"],{

/***/ "+zu2":
/*!****************************************************************************!*\
  !*** ./src/app/content/pages/roleManagment/add-role/add-role.component.ts ***!
  \****************************************************************************/
/*! exports provided: AddRoleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddRoleComponent", function() { return AddRoleComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "vDqi");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "PSD3");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _app_services_base_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @app/services/base-service.service */ "eWbo");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular2-multiselect-dropdown */ "e/mZ");








function AddRoleComponent_div_13_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Role Name is required. ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function AddRoleComponent_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, AddRoleComponent_div_13_div_1_Template, 2, 0, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r0.errors.required);
} }
class AddRoleComponent {
    constructor(baseService, cdRef) {
        this.baseService = baseService;
        this.cdRef = cdRef;
        this.dropdownSettings = {};
        this.dropdownList = [];
        this.selectedItems = [];
        this.finalData = {
            roleName: '',
            screenData: []
        };
        this.dropdownList = [
            { "id": "Dashboard", "itemName": "Dashboard" },
            { "id": "Filling Progress", "itemName": "Filling Progress" },
            { "id": "Truck", "itemName": "Truck" },
            { "id": "Customer", "itemName": "Customer" },
            { "id": "Product", "itemName": "Product" },
            { "id": "Card", "itemName": "Card" },
            { "id": "Order Entry", "itemName": "Order Entry" },
            { "id": "Order Status", "itemName": "Order Status" },
            { "id": "Order Cancelled", "itemName": "Order Cancelled" },
            { "id": "Order Completed", "itemName": "Order Completed" }
        ];
    }
    formSubmit() {
        console.log(this.selectedItems);
        console.log("Final Data");
        this.finalData.screenData = this.selectedItems;
        console.log(this.finalData);
        axios__WEBPACK_IMPORTED_MODULE_1___default.a.post(this.baseService.baseUrl + 'api/roleMgmt/', this.finalData, {
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
            },
        })
            .then(response => {
            if (response.data.status) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    position: 'center',
                    icon: 'success',
                    title: response.data["message"],
                    showConfirmButton: false,
                    timer: 4000
                }).then(() => {
                });
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    position: 'center',
                    icon: 'error',
                    title: response.data["message"],
                    showConfirmButton: false,
                    timer: 4000
                }).then(() => {
                });
            }
            // console.log('resp: ', response);
            // location.reload();
        })
            .catch(function (error) {
            console.log(error);
        });
    }
    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }
    ngOnInit() {
        this.dropdownSettings = {
            singleSelection: false,
            text: "Select Screens",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class"
        };
    }
    onItemSelect(item) {
        console.log("on Select Item...");
        console.log(this.selectedItems);
        // for(let i=0;i<this.formModel.selectedItems.length;i++)
        // {
        // this.newTicket.tags.push(this.formModel.selectedItems[i].id);
        // }
        console.log("Tag Names");
        // console.log(this.newTicket);
        // this.newTicket.tags.push(this.formModel.selectedItems)
        // console.log(this.formModel.selectedItems);
    }
    OnItemDeSelect(item) {
        console.log(item);
        // console.log(this.formModel.selectedItems);
    }
    onSelectAll(items) {
        console.log("On Select All");
        console.log(this.selectedItems);
        // this.tags.push(this.formModel.selectedItems);
    }
    onDeSelectAll(items) {
        console.log(items);
    }
}
AddRoleComponent.ɵfac = function AddRoleComponent_Factory(t) { return new (t || AddRoleComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_app_services_base_service_service__WEBPACK_IMPORTED_MODULE_3__["BaseServiceService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"])); };
AddRoleComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AddRoleComponent, selectors: [["app-add-role"]], decls: 24, vars: 5, consts: [[1, "container"], [1, "card", "mt-2"], [1, "card-header", "text-center"], [1, "card-body"], ["novalidate", "", 3, "ngSubmit"], [1, "form"], [1, "row"], ["for", "validationDefault01", 1, "col-sm-3", "col-md-3", "mb-3"], [1, "col-sm-6", "col-md-6", "mb-3"], ["type", "text", "id", "normal-input-5", "placeholder", "Enter Role Name", "name", "roleName", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["roleName", "ngModel"], ["class", "form-text text-danger", 4, "ngIf"], [1, "form-row"], ["name", "selectedItems", "required", "", 3, "data", "ngModel", "settings", "ngModelChange", "onSelect", "onDeSelect", "onSelectAll", "onDeSelectAll"], [1, "col-sm-4", "mb-3", "subbutton"], ["type", "submit", 1, "btn", "btn-primary"], ["type", "reset", 1, "btn", "btn-danger"], [1, "form-text", "text-danger"], [4, "ngIf"]], template: function AddRoleComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, " Role Managment ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "form", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function AddRoleComponent_Template_form_ngSubmit_5_listener() { return ctx.formSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "label", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Roles");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "input", 9, 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function AddRoleComponent_Template_input_ngModelChange_11_listener($event) { return ctx.finalData.roleName = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, AddRoleComponent_div_13_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "label", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Screens");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "angular2-multiselect", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function AddRoleComponent_Template_angular2_multiselect_ngModelChange_18_listener($event) { return ctx.selectedItems = $event; })("onSelect", function AddRoleComponent_Template_angular2_multiselect_onSelect_18_listener($event) { return ctx.onItemSelect($event); })("onDeSelect", function AddRoleComponent_Template_angular2_multiselect_onDeSelect_18_listener($event) { return ctx.OnItemDeSelect($event); })("onSelectAll", function AddRoleComponent_Template_angular2_multiselect_onSelectAll_18_listener($event) { return ctx.onSelectAll($event); })("onDeSelectAll", function AddRoleComponent_Template_angular2_multiselect_onDeSelectAll_18_listener($event) { return ctx.onDeSelectAll($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "button", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Submit");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "button", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Clear");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.finalData.roleName);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r0.invalid && (_r0.dirty || _r0.touched));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.dropdownList)("ngModel", ctx.selectedItems)("settings", ctx.dropdownSettings);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_6__["AngularMultiSelect"]], styles: [".subbutton[_ngcontent-%COMP%] {\n  margin-left: 40%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFwuLlxcLi5cXGFkZC1yb2xlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksZ0JBQWlCO0FBQXJCIiwiZmlsZSI6ImFkZC1yb2xlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnN1YmJ1dHRvblxyXG57XHJcbiAgICBtYXJnaW4tbGVmdDogNDAlIDtcclxufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AddRoleComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-add-role',
                templateUrl: './add-role.component.html',
                styleUrls: ['./add-role.component.scss']
            }]
    }], function () { return [{ type: _app_services_base_service_service__WEBPACK_IMPORTED_MODULE_3__["BaseServiceService"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"] }]; }, null); })();


/***/ }),

/***/ "QD4o":
/*!*************************************************************************!*\
  !*** ./src/app/content/pages/roleManagment/add-role/add-role.module.ts ***!
  \*************************************************************************/
/*! exports provided: AddRoleModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddRoleModule", function() { return AddRoleModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular2-multiselect-dropdown */ "e/mZ");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _add_role_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-role.component */ "+zu2");








const appsRoutes = [
    {
        path: '',
        component: _add_role_component__WEBPACK_IMPORTED_MODULE_5__["AddRoleComponent"]
    },
    {
        path: '**',
        component: _add_role_component__WEBPACK_IMPORTED_MODULE_5__["AddRoleComponent"]
    }
];
class AddRoleModule {
}
AddRoleModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AddRoleModule });
AddRoleModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AddRoleModule_Factory(t) { return new (t || AddRoleModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_3__["AngularMultiSelectModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(appsRoutes)
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AddRoleModule, { declarations: [_add_role_component__WEBPACK_IMPORTED_MODULE_5__["AddRoleComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
        angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_3__["AngularMultiSelectModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]], exports: [_add_role_component__WEBPACK_IMPORTED_MODULE_5__["AddRoleComponent"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AddRoleModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [_add_role_component__WEBPACK_IMPORTED_MODULE_5__["AddRoleComponent"]],
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                    angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_3__["AngularMultiSelectModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                    _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(appsRoutes)
                ],
                schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["CUSTOM_ELEMENTS_SCHEMA"]],
                exports: [
                    _add_role_component__WEBPACK_IMPORTED_MODULE_5__["AddRoleComponent"]
                ]
            }]
    }], null, null); })();


/***/ })

}]);
//# sourceMappingURL=roleManagment-add-role-add-role-module-es2015.js.map