(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["configuration-global-config-global-config-module"], {
    /***/
    "6MgZ":
    /*!***********************************************************************************!*\
      !*** ./src/app/content/pages/configuration/global-config/global-config.module.ts ***!
      \***********************************************************************************/

    /*! exports provided: GlobalConfigModule */

    /***/
    function MgZ(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GlobalConfigModule", function () {
        return GlobalConfigModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _global_config_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./global-config.component */
      "xIS8");

      var appsRoutes = [{
        path: '',
        component: _global_config_component__WEBPACK_IMPORTED_MODULE_4__["GlobalConfigComponent"]
      }, {
        path: '**',
        component: _global_config_component__WEBPACK_IMPORTED_MODULE_4__["GlobalConfigComponent"]
      }];

      var GlobalConfigModule = function GlobalConfigModule() {
        _classCallCheck(this, GlobalConfigModule);
      };

      GlobalConfigModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: GlobalConfigModule
      });
      GlobalConfigModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function GlobalConfigModule_Factory(t) {
          return new (t || GlobalConfigModule)();
        },
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(appsRoutes)]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](GlobalConfigModule, {
          declarations: [_global_config_component__WEBPACK_IMPORTED_MODULE_4__["GlobalConfigComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"]],
          exports: [_global_config_component__WEBPACK_IMPORTED_MODULE_4__["GlobalConfigComponent"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](GlobalConfigModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            declarations: [_global_config_component__WEBPACK_IMPORTED_MODULE_4__["GlobalConfigComponent"]],
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(appsRoutes)],
            exports: [_global_config_component__WEBPACK_IMPORTED_MODULE_4__["GlobalConfigComponent"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "xIS8":
    /*!**************************************************************************************!*\
      !*** ./src/app/content/pages/configuration/global-config/global-config.component.ts ***!
      \**************************************************************************************/

    /*! exports provided: GlobalConfigComponent */

    /***/
    function xIS8(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GlobalConfigComponent", function () {
        return GlobalConfigComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! axios */
      "vDqi");
      /* harmony import */


      var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
      /* harmony import */


      var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! sweetalert2 */
      "PSD3");
      /* harmony import */


      var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
      /* harmony import */


      var _app_services_base_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @app/services/base-service.service */
      "eWbo");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");

      function GlobalConfigComponent_h4_5_Template(rf, ctx) {
        if (rf & 1) {
          var _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h4", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "input", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function GlobalConfigComponent_h4_5_Template_input_ngModelChange_4_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5);

            var data_r2 = ctx.$implicit;
            return data_r2.isActive = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var data_r2 = ctx.$implicit;
          var i_r3 = ctx.index;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", data_r2.name, " : ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("id", "switch", i_r3, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("checked", data_r2.isActive)("ngModel", data_r2.isActive);
        }
      }

      var GlobalConfigComponent = /*#__PURE__*/function () {
        function GlobalConfigComponent(baseService) {
          _classCallCheck(this, GlobalConfigComponent);

          this.baseService = baseService;
          this.globalConfigData = {
            cardFlowConfig: []
          };
        }

        _createClass(GlobalConfigComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.getGlobalConfigList();
          }
        }, {
          key: "getGlobalConfigList",
          value: function getGlobalConfigList() {
            var _this = this;

            console.log("Common Config..");
            axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.baseService.baseUrl + 'api/config/global-config', {
              headers: {
                'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
              }
            }).then(function (response) {
              console.log(response);
              _this.globalConfigData = response.data.data.configurationDetails;
              console.log(_this.globalConfigData.cardFlowConfig);
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }, {
          key: "finalSubmit",
          value: function finalSubmit() {
            var _this2 = this;

            //  var data=[
            //  {name: "Begin Cycle", isActive: true},
            //  {name: "Gate-B (ENTRY)", isActive: true},
            //  {name: "offline-weightbridge (Tare)", isActive: true},
            //  {name: "Gantry", isActive: true},
            //  {name: "offline-weightbridge (Gross)", isActive: true},
            //  {name: "Gate-B (EXIT)", isActive: true},
            //  {name: "Stop Cycle", isActive: true}
            //  ]
            // this.globalConfigData.cardFlowConfig.push(data);
            // this.globalConfigData.printerConfig={ip: "192.169.0.1", port: "0.0.0.0:0000"};
            console.log(this.globalConfigData);
            axios__WEBPACK_IMPORTED_MODULE_1___default.a.post(this.baseService.baseUrl + 'api/config/global-config', this.globalConfigData, {
              headers: {
                'Authorization': "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
              }
            }).then(function (response) {
              if (response.data.status) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                  position: 'center',
                  icon: 'success',
                  title: "Update Successfully",
                  showConfirmButton: false,
                  timer: 3000
                });
              } else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                  position: 'center',
                  icon: 'error',
                  title: "Update Fail",
                  showConfirmButton: false,
                  timer: 3000
                });
              }

              _this2.getGlobalConfigList();
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }]);

        return GlobalConfigComponent;
      }();

      GlobalConfigComponent.ɵfac = function GlobalConfigComponent_Factory(t) {
        return new (t || GlobalConfigComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_app_services_base_service_service__WEBPACK_IMPORTED_MODULE_3__["BaseServiceService"]));
      };

      GlobalConfigComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: GlobalConfigComponent,
        selectors: [["app-global-config"]],
        decls: 16,
        vars: 2,
        consts: [[1, "card", "card-style"], [1, "text-center", "header", "p-3"], ["class", "row text-center", 4, "ngFor", "ngForOf"], [1, "form-group", "form-row"], ["for", "normal-input-5", 1, "col-md-4", "col-sm-3", "col-form-label", "col-form-label-sm", "text-sm-right"], [1, "col-md-4", "col-sm-6"], ["type", "text", "id", "normal-input-5", "placeholder", "0.0.0.0:0000", "name", "globalConfigData.printerConfig.port", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["globalConfigData.printerConfig.port", "ngModel"], [1, "btn", "btn-primary", 3, "click"], [1, "row", "text-center"], [1, "col-md-6"], [1, "name"], ["type", "checkbox", 1, "col-md-2", 3, "checked", "ngModel", "id", "ngModelChange"]],
        template: function GlobalConfigComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h1", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " GLOBAL COMMUNICATION ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h4");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Card Flow Configuration");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, GlobalConfigComponent_h4_5_Template, 5, 4, "h4", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h4");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Printer Configuration");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "label", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "IP & PORT : ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "input", 6, 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function GlobalConfigComponent_Template_input_ngModelChange_12_listener($event) {
              return (ctx.globalConfigData == null ? null : ctx.globalConfigData.printerConfig).port = $event;
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "button", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function GlobalConfigComponent_Template_button_click_14_listener() {
              return ctx.finalSubmit();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Save");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.globalConfigData.cardFlowConfig[0]);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.globalConfigData == null ? null : ctx.globalConfigData.printerConfig.port);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["CheckboxControlValueAccessor"]],
        styles: [".name[_ngcontent-%COMP%] {\n  display: contents;\n}\n\n.row[_ngcontent-%COMP%] {\n  margin-bottom: 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFwuLlxcLi5cXGdsb2JhbC1jb25maWcuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFHQSxpQkFBaUI7QUFEakI7O0FBSUE7RUFFSSxtQkFBbUI7QUFGdkIiLCJmaWxlIjoiZ2xvYmFsLWNvbmZpZy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5uYW1lXHJcbntcclxuLy8gZm9udC13ZWlnaHQ6IDgwMDtcclxuZGlzcGxheTogY29udGVudHM7XHJcbn1cclxuXHJcbi5yb3dcclxue1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNTBweDtcclxufSJdfQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](GlobalConfigComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-global-config',
            templateUrl: './global-config.component.html',
            styleUrls: ['./global-config.component.scss']
          }]
        }], function () {
          return [{
            type: _app_services_base_service_service__WEBPACK_IMPORTED_MODULE_3__["BaseServiceService"]
          }];
        }, null);
      })();
      /***/

    }
  }]);
})();
//# sourceMappingURL=configuration-global-config-global-config-module-es5.js.map