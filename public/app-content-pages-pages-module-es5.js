(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-content-pages-pages-module"], {
    /***/
    "3APk":
    /*!******************************************************************************!*\
      !*** ./src/app/content/pages/roleManagment/role-list/role-list.component.ts ***!
      \******************************************************************************/

    /*! exports provided: RoleListComponent */

    /***/
    function APk(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RoleListComponent", function () {
        return RoleListComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var RoleListComponent = /*#__PURE__*/function () {
        function RoleListComponent() {
          _classCallCheck(this, RoleListComponent);
        }

        _createClass(RoleListComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return RoleListComponent;
      }();

      RoleListComponent.ɵfac = function RoleListComponent_Factory(t) {
        return new (t || RoleListComponent)();
      };

      RoleListComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: RoleListComponent,
        selectors: [["app-role-list"]],
        decls: 2,
        vars: 0,
        template: function RoleListComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "role-list works!");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyb2xlLWxpc3QuY29tcG9uZW50LnNjc3MifQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](RoleListComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-role-list',
            templateUrl: './role-list.component.html',
            styleUrls: ['./role-list.component.scss']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "AfIt":
    /*!********************************************************!*\
      !*** ./src/app/content/pages/blank/blank.component.ts ***!
      \********************************************************/

    /*! exports provided: BlankComponent */

    /***/
    function AfIt(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BlankComponent", function () {
        return BlankComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _app_settings_settings_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @app/settings/settings.service */
      "b1TM");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _gaxon_components_common_page_header_page_header_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../../../gaxon/components/common/page-header/page-header.component */
      "dCDM");
      /* harmony import */


      var _gaxon_components_gx_card_gx_card_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../../gaxon/components/gx-card/gx-card.component */
      "zhXz");

      function BlankComponent_ng_container_0_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-page-header", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "This is a blank page provided in the template which could be used to setup your own custom page.");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
        }
      }

      var _c0 = function _c0() {
        return {
          height: "400px"
        };
      };

      function BlankComponent_ng_container_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "gx-card", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "p", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "This is a blank page provided in the template which could be used to setup your own custom page.");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](1, _c0));
        }
      }

      var BlankComponent = /*#__PURE__*/function () {
        function BlankComponent(settingService) {
          var _this = this;

          _classCallCheck(this, BlankComponent);

          this.settingService = settingService;
          this.onSettingChanged = this.settingService.onSettingChanged.subscribe(function (newSettings) {
            _this.settings = newSettings;
          });
        }

        _createClass(BlankComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.onSettingChanged.unsubscribe();
          }
        }]);

        return BlankComponent;
      }();

      BlankComponent.ɵfac = function BlankComponent_Factory(t) {
        return new (t || BlankComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_app_settings_settings_service__WEBPACK_IMPORTED_MODULE_1__["SettingsService"]));
      };

      BlankComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: BlankComponent,
        selectors: [["app-blank"]],
        decls: 2,
        vars: 2,
        consts: [[4, "ngIf"], ["pageTitle", "Blank Page"], [1, "row"], [1, "col-xl-12"], [1, "lead"], ["card-title", "Blank Page", 3, "ngStyle"]],
        template: function BlankComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, BlankComponent_ng_container_0_Template, 6, 0, "ng-container", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, BlankComponent_ng_container_1_Template, 4, 2, "ng-container", 0);
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.settings.layout !== "intranet");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.settings.layout === "intranet");
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], _gaxon_components_common_page_header_page_header_component__WEBPACK_IMPORTED_MODULE_3__["PageHeaderComponent"], _gaxon_components_gx_card_gx_card_component__WEBPACK_IMPORTED_MODULE_4__["GxCardComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgStyle"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJibGFuay5jb21wb25lbnQuc2NzcyJ9 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](BlankComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-blank',
            templateUrl: './blank.component.html',
            styleUrls: ['./blank.component.scss']
          }]
        }], function () {
          return [{
            type: _app_settings_settings_service__WEBPACK_IMPORTED_MODULE_1__["SettingsService"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "x997":
    /*!***********************************************!*\
      !*** ./src/app/content/pages/pages.module.ts ***!
      \***********************************************/

    /*! exports provided: PagesModule */

    /***/
    function x997(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PagesModule", function () {
        return PagesModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _gaxon_modules__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @gaxon/modules */
      "1u1q");
      /* harmony import */


      var _blank_blank_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./blank/blank.component */
      "AfIt");
      /* harmony import */


      var angular_datatables__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! angular-datatables */
      "njyG");
      /* harmony import */


      var _roleManagment_role_list_role_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./roleManagment/role-list/role-list.component */
      "3APk");

      var appsRoutes = [{
        path: 'filling',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | filling-filling-module */
          [__webpack_require__.e("common"), __webpack_require__.e("filling-filling-module")]).then(__webpack_require__.bind(null,
          /*! ./filling/filling.module */
          "PAGz")).then(function (m) {
            return m.FillingModule;
          });
        }
      }, {
        path: 'main-block',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | main-block-main-block-module */
          [__webpack_require__.e("default~configuration-common-config-common-config-module~configuration-global-config-global-config-m~8e851c70"), __webpack_require__.e("main-block-main-block-module")]).then(__webpack_require__.bind(null,
          /*! ./main-block/main-block.module */
          "d8wM")).then(function (m) {
            return m.MainBlockModule;
          });
        }
      }, {
        path: 'reports',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | reports-reports-module */
          [__webpack_require__.e("default~configuration-common-config-common-config-module~configuration-global-config-global-config-m~8e851c70"), __webpack_require__.e("default~pdfmake~reports-reports-module"), __webpack_require__.e("default~dashboard-dashboard-module~reports-reports-module"), __webpack_require__.e("reports-reports-module")]).then(__webpack_require__.bind(null,
          /*! ./reports/reports.module */
          "841u")).then(function (m) {
            return m.ReportsModule;
          });
        }
      }, {
        path: 'dashboard',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | dashboard-dashboard-module */
          [__webpack_require__.e("default~dashboard-dashboard-module~reports-reports-module"), __webpack_require__.e("common"), __webpack_require__.e("dashboard-dashboard-module")]).then(__webpack_require__.bind(null,
          /*! ./dashboard/dashboard.module */
          "CBHK")).then(function (m) {
            return m.DashboardModule;
          });
        }
      }, {
        path: 'role',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | roleManagment-role-managment-role-managment-module */
          [__webpack_require__.e("default~configuration-common-config-common-config-module~configuration-global-config-global-config-m~8e851c70"), __webpack_require__.e("default~roleManagment-add-role-add-role-module~roleManagment-role-managment-role-managment-module"), __webpack_require__.e("roleManagment-role-managment-role-managment-module")]).then(__webpack_require__.bind(null,
          /*! ./roleManagment/role-managment/role-managment.module */
          "qZ3D")).then(function (m) {
            return m.RoleManagmentModule;
          });
        }
      }, {
        path: 'user',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | user-managment-user-list-user-list-module */
          [__webpack_require__.e("default~configuration-common-config-common-config-module~configuration-global-config-global-config-m~8e851c70"), __webpack_require__.e("user-managment-user-list-user-list-module")]).then(__webpack_require__.bind(null,
          /*! ./user-managment/user-list/user-list.module */
          "Bcql")).then(function (m) {
            return m.UserListModule;
          });
        }
      }, {
        path: 'add-user',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | user-managment-add-user-add-user-module */
          [__webpack_require__.e("default~configuration-common-config-common-config-module~configuration-global-config-global-config-m~8e851c70"), __webpack_require__.e("user-managment-add-user-add-user-module")]).then(__webpack_require__.bind(null,
          /*! ./user-managment/add-user/add-user.module */
          "a+tY")).then(function (m) {
            return m.AddUserModule;
          });
        }
      }, {
        path: 'user-active',
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | user-managment-user-active-user-active-module */
          [__webpack_require__.e("default~configuration-common-config-common-config-module~configuration-global-config-global-config-m~8e851c70"), __webpack_require__.e("user-managment-user-active-user-active-module")]).then(__webpack_require__.bind(null,
          /*! ./user-managment/user-active/user-active.module */
          "s1ti")).then(function (m) {
            return m.UserActiveModule;
          });
        }
      }, {
        path: "add-role",
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | roleManagment-add-role-add-role-module */
          [__webpack_require__.e("default~configuration-common-config-common-config-module~configuration-global-config-global-config-m~8e851c70"), __webpack_require__.e("default~roleManagment-add-role-add-role-module~roleManagment-role-managment-role-managment-module"), __webpack_require__.e("roleManagment-add-role-add-role-module")]).then(__webpack_require__.bind(null,
          /*! ./roleManagment/add-role/add-role.module */
          "QD4o")).then(function (m) {
            return m.AddRoleModule;
          });
        }
      }, {
        path: "common-config",
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | configuration-common-config-common-config-module */
          [__webpack_require__.e("default~configuration-common-config-common-config-module~configuration-global-config-global-config-m~8e851c70"), __webpack_require__.e("configuration-common-config-common-config-module")]).then(__webpack_require__.bind(null,
          /*! ./configuration/common-config/common-config.module */
          "ilZ7")).then(function (m) {
            return m.CommonConfigModule;
          });
        }
      }, {
        path: "global-config",
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | configuration-global-config-global-config-module */
          [__webpack_require__.e("default~configuration-common-config-common-config-module~configuration-global-config-global-config-m~8e851c70"), __webpack_require__.e("configuration-global-config-global-config-module")]).then(__webpack_require__.bind(null,
          /*! ./configuration/global-config/global-config.module */
          "6MgZ")).then(function (m) {
            return m.GlobalConfigModule;
          });
        }
      }, {
        path: "offline-weight",
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | offline-weight-add-offline-weight-add-offline-weight-module */
          [__webpack_require__.e("default~configuration-common-config-common-config-module~configuration-global-config-global-config-m~8e851c70"), __webpack_require__.e("offline-weight-add-offline-weight-add-offline-weight-module")]).then(__webpack_require__.bind(null,
          /*! ./offline-weight/add-offline-weight/add-offline-weight.module */
          "Mrl1")).then(function (m) {
            return m.AddOfflineWeightModule;
          });
        }
      }, {
        path: "view-offline-weight",
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | offline-weight-view-offline-weight-view-offline-weight-module */
          "offline-weight-view-offline-weight-view-offline-weight-module").then(__webpack_require__.bind(null,
          /*! ./offline-weight/view-offline-weight/view-offline-weight.module */
          "s5PV")).then(function (m) {
            return m.ViewOfflineWeightModule;
          });
        }
      }];

      var PagesModule = function PagesModule() {
        _classCallCheck(this, PagesModule);
      };

      PagesModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: PagesModule
      });
      PagesModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function PagesModule_Factory(t) {
          return new (t || PagesModule)();
        },
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _gaxon_modules__WEBPACK_IMPORTED_MODULE_3__["SharedModule"], angular_datatables__WEBPACK_IMPORTED_MODULE_5__["DataTablesModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(appsRoutes)]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](PagesModule, {
          declarations: [_blank_blank_component__WEBPACK_IMPORTED_MODULE_4__["BlankComponent"], _roleManagment_role_list_role_list_component__WEBPACK_IMPORTED_MODULE_6__["RoleListComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _gaxon_modules__WEBPACK_IMPORTED_MODULE_3__["SharedModule"], angular_datatables__WEBPACK_IMPORTED_MODULE_5__["DataTablesModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PagesModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _gaxon_modules__WEBPACK_IMPORTED_MODULE_3__["SharedModule"], angular_datatables__WEBPACK_IMPORTED_MODULE_5__["DataTablesModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(appsRoutes)],
            declarations: [_blank_blank_component__WEBPACK_IMPORTED_MODULE_4__["BlankComponent"], _roleManagment_role_list_role_list_component__WEBPACK_IMPORTED_MODULE_6__["RoleListComponent"]]
          }]
        }], null, null);
      })();
      /***/

    }
  }]);
})();
//# sourceMappingURL=app-content-pages-pages-module-es5.js.map