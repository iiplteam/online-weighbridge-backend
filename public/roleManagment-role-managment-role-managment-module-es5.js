(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["roleManagment-role-managment-role-managment-module"], {
    /***/
    "Erl+":
    /*!****************************************************************************************!*\
      !*** ./src/app/content/pages/roleManagment/role-managment/role-managment.component.ts ***!
      \****************************************************************************************/

    /*! exports provided: RoleManagmentComponent */

    /***/
    function Erl(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RoleManagmentComponent", function () {
        return RoleManagmentComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! axios */
      "vDqi");
      /* harmony import */


      var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
      /* harmony import */


      var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! sweetalert2 */
      "PSD3");
      /* harmony import */


      var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
      /* harmony import */


      var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ng-bootstrap/ng-bootstrap */
      "1kSV");
      /* harmony import */


      var _app_services_base_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @app/services/base-service.service */
      "eWbo");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! angular2-multiselect-dropdown */
      "e/mZ");

      function RoleManagmentComponent_option_16_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var role_r3 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](role_r3.roleName);
        }
      }

      function RoleManagmentComponent_ng_template_25_div_16_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Role Name is required. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function RoleManagmentComponent_ng_template_25_div_16_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, RoleManagmentComponent_ng_template_25_div_16_div_1_Template, 2, 0, "div", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r6.errors.required);
        }
      }

      function RoleManagmentComponent_ng_template_25_Template(rf, ctx) {
        if (rf & 1) {
          var _r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "form", 19, 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h4", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Role");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function RoleManagmentComponent_ng_template_25_Template_button_click_5_listener() {
            var modal_r4 = ctx.$implicit;
            return modal_r4.dismiss("Cross click");
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "span", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "\xD7");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "label", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Role Name");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "input", 29, 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function RoleManagmentComponent_ng_template_25_Template_input_ngModelChange_14_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r11);

            var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r10.selectObj.roleName = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, RoleManagmentComponent_ng_template_25_div_16_Template, 2, 1, "div", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "button", 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function RoleManagmentComponent_ng_template_25_Template_button_click_18_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r11);

            var modal_r4 = ctx.$implicit;

            var _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](1);

            modal_r4.close();
            return _r5.reset();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Add");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](1);

          var _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](15);

          var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r2.selectObj.roleName);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _r6.invalid && (_r6.dirty || _r6.touched));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !_r5.valid);
        }
      }

      var RoleManagmentComponent = /*#__PURE__*/function () {
        function RoleManagmentComponent(modalService, baseService, cdRef) {
          _classCallCheck(this, RoleManagmentComponent);

          this.modalService = modalService;
          this.baseService = baseService;
          this.cdRef = cdRef;
          this.dropdownSettings = {};
          this.selectObj = {};
          this.dropdownList = [];
          this.selectedItems = [];
          this.finalData = {
            roleName: '',
            screenData: []
          };
          this.getScreens(); // this.roleData=[
          //   {id:1, name:"Admin"},
          //   {id:2, name:"Super Admin"},
          //   {id:3, name:"User"}
          // ]

          this.dropdownList = [{
            "id": "Dashboard",
            "itemName": "Dashboard"
          }, {
            "id": "Filling Progress",
            "itemName": "Filling Progress"
          }, {
            "id": "Truck",
            "itemName": "Truck"
          }, {
            "id": "Customer",
            "itemName": "Customer"
          }, {
            "id": "Product",
            "itemName": "Product"
          }, {
            "id": "Card",
            "itemName": "Card"
          }, {
            "id": "Order Entry",
            "itemName": "Order Entry"
          }, {
            "id": "Order Status",
            "itemName": "Order Status"
          }, {
            "id": "Order Cancelled",
            "itemName": "Order Cancelled"
          }, {
            "id": "Order Completed",
            "itemName": "Order Completed"
          }];
          this.selectedItems = [// {"id":2,"itemName":"Singapore"},
            // {"id":3,"itemName":"Australia"},
            // {"id":4,"itemName":"Canada"},
            // {"id":5,"itemName":"South Korea"}
          ];
        }

        _createClass(RoleManagmentComponent, [{
          key: "ngAfterViewChecked",
          value: function ngAfterViewChecked() {
            this.cdRef.detectChanges();
          }
        }, {
          key: "getScreens",
          value: function getScreens() {
            var _this = this;

            // this.dropdownList=[
            //   { id:1 , itemName : "Dashboard"},
            //   { id:2 , itemName : "Filling Progress"},
            //   { id:3 , itemName : "Master Entry"},
            //   { id:4 , itemName : "Order Process"},
            //   { id:5 , itemName : "Reports"}
            // ]
            axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.baseService.baseUrl + 'api/roleMgmt', {
              headers: {
                "Authorization": "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
              }
            }).then(function (response) {
              if (response.data.status) {
                console.log(response);
                _this.roleData = response.data.data;
                var tmp = [];

                _this.getScreensByRoleName(_this.roleData[0]['roleName']); // console.log(this.roleData.screenData);
                //   for(let i=0;i < this.roleData.length;i++)
                //   {
                //     let count=0;
                //     console.log(this.roleData[i]);
                //     for(let j=0; j < this.roleData[i].screenData.length; j++) {
                //       console.log(this.roleData[i].screenData[j]);
                //       tmp.push({ id: count++, itemName: this.roleData[i].screenData[j].screenName });  //this.roleData[i].screenData[j]._id
                //     }
                //   }
                //   this.formModel.selectedItems= tmp;
                //   console.log("DropDown Data");
                //   console.log(this.selectedItems);

              } else {}
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }, {
          key: "getScreensByRoleName",
          value: function getScreensByRoleName(roleName) {
            var _this2 = this;

            console.log("Change Value");
            console.log(roleName);
            this.finalData.roleName = roleName;
            axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(this.baseService.baseUrl + 'api/roleMgmt/role', {
              headers: {
                "Authorization": "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
              },
              params: {
                roleName: roleName
              }
            }).then(function (response) {
              if (response.data.status) {
                console.log(response);
                _this2.screenNameData = response.data.data; // let tmp = [];
                // for(let i=0;i < this.screenNameData.screenData.length;i++)
                // {
                //   let count=0;
                //     tmp.push({ id:this.screenNameData.screenData[i]['screenName']  , itemName: this.screenNameData.screenData[i]['screenName'] });  //this.roleData[i].screenData[j]._id
                // }

                _this2.selectedItems = _this2.screenNameData.screenData;
                console.log("DropDown Data");
                console.log(_this2.selectedItems);
              } else {}
            })["catch"](function (error) {
              console.log(error);
            });
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            this.dropdownSettings = {
              singleSelection: false,
              text: "Select Screens",
              selectAllText: 'Select All',
              unSelectAllText: 'UnSelect All',
              enableSearchFilter: true,
              classes: "myclass custom-class"
            };
          } //Add New Role

        }, {
          key: "addNewRole",
          value: function addNewRole(content) {
            var _this3 = this;

            var self = this; // self.selectObj = self.notificationsList.data[index];

            self.modalService.open(content, {
              ariaLabelledBy: 'modal-basic-title',
              keyboard: false,
              backdrop: 'static',
              centered: true
            }).result.then(function (result) {
              // self.notificationsList.data[index] = self.selectObj;
              // self.ticketService.mdmPost(this.conf, {
              //   configurationName: this.name,
              //   configurationDetails: self.notificationsList,
              // }, false).subscribe((data) => {
              //   self.getNotificationsList();
              // }, (err) => {
              //   self.getNotificationsList();
              //   console.log(err);
              _this3.selectObj.screenData = [{
                id: "Dashboard",
                itemName: "Dashboard"
              }];
              console.log(_this3.selectObj);
              axios__WEBPACK_IMPORTED_MODULE_1___default.a.post(_this3.baseService.baseUrl + 'api/roleMgmt/', self.selectObj, {
                headers: {
                  "Authorization": "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
                }
              }).then(function (response) {
                if (response.data.status) {
                  sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    position: 'center',
                    icon: 'success',
                    title: response.data["message"],
                    showConfirmButton: false,
                    timer: 3000
                  }).then(function (result) {});
                }
              })["catch"](function (error) {
                console.log(error);
              }); // });
            }, function (reason) {// self.getNotificationsList();
            });
          }
        }, {
          key: "onItemSelect",
          value: function onItemSelect(item) {
            console.log("on Select Item...");
            console.log(this.selectedItems); // for(let i=0;i<this.formModel.selectedItems.length;i++)
            // {
            // this.newTicket.tags.push(this.formModel.selectedItems[i].id);
            // }

            console.log("Tag Names"); // console.log(this.newTicket);
            // this.newTicket.tags.push(this.formModel.selectedItems)
            // console.log(this.formModel.selectedItems);
          }
        }, {
          key: "OnItemDeSelect",
          value: function OnItemDeSelect(item) {
            console.log(item); // console.log(this.formModel.selectedItems);
          }
        }, {
          key: "onSelectAll",
          value: function onSelectAll(items) {
            console.log("On Select All");
            console.log(this.selectedItems); // this.tags.push(this.formModel.selectedItems);
          }
        }, {
          key: "onDeSelectAll",
          value: function onDeSelectAll(items) {
            console.log(items);
          }
        }, {
          key: "formSubmit",
          value: function formSubmit() {
            var _this4 = this;

            console.log(this.selectedItems);
            console.log("Final Data");
            this.finalData.screenData = this.selectedItems;
            console.log(this.finalData);
            axios__WEBPACK_IMPORTED_MODULE_1___default.a.put(this.baseService.baseUrl + 'api/roleMgmt/', this.finalData, {
              headers: {
                "Authorization": "Bearer " + localStorage.getItem("OnlineWeighbridgeToken")
              }
            }).then(function (response) {
              if (response.data.status) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                  position: 'center',
                  icon: 'success',
                  title: response.data["message"],
                  showConfirmButton: false,
                  timer: 4000
                }).then(function () {});

                _this4.getScreens();
              } else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                  position: 'center',
                  icon: 'error',
                  title: response.data["message"],
                  showConfirmButton: false,
                  timer: 4000
                }).then(function () {});
              } // console.log('resp: ', response);
              // location.reload();

            })["catch"](function (error) {
              console.log(error);
            });
          }
        }]);

        return RoleManagmentComponent;
      }();

      RoleManagmentComponent.ɵfac = function RoleManagmentComponent_Factory(t) {
        return new (t || RoleManagmentComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_app_services_base_service_service__WEBPACK_IMPORTED_MODULE_4__["BaseServiceService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]));
      };

      RoleManagmentComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: RoleManagmentComponent,
        selectors: [["app-role-managment"]],
        decls: 27,
        vars: 4,
        consts: [[1, "container"], [1, "row", "mt-3"], [1, "col-md-9"], [1, "float-right", "mb-3"], [1, "btn", "btn-primary", 3, "click"], [1, "card", "mt-2"], [1, "card-body"], ["novalidate", "", 3, "ngSubmit"], [1, "form"], [1, "row"], ["for", "validationDefault01", 1, "col-sm-3", "col-md-3", "mb-3"], [1, "col-sm-6", "col-md-6", "mb-3"], [1, "form-control", 3, "change"], [4, "ngFor", "ngForOf"], [1, "form-row"], ["name", "selectedItems", "required", "", 3, "data", "ngModel", "settings", "ngModelChange", "onSelect", "onDeSelect", "onSelectAll", "onDeSelectAll"], [1, "col-sm-4", "mb-3", "subbutton"], ["type", "submit", 1, "btn", "btn-primary"], ["remarksContent", ""], ["novalidate", ""], ["lpForm", "ngForm"], [1, "modal-header"], ["id", "modal-basic-title", 1, "modal-title"], ["type", "button", "aria-label", "Close", 1, "close", 3, "click"], ["aria-hidden", "true"], [1, "modal-body"], [1, "form-group", "col-12"], ["for", "dateOfBirth"], [1, "input-group"], ["type", "text", "id", "normal-input-5", "placeholder", "Enter Role Name", "name", "roleName", "required", "", 1, "form-control", "form-control-sm", 3, "ngModel", "ngModelChange"], ["roleName", "ngModel"], ["class", "form-text text-danger", 4, "ngIf"], [1, "modal-footer"], ["type", "button", 1, "btn", "btn-outline-danger", 3, "disabled", "click"], [1, "form-text", "text-danger"], [4, "ngIf"]],
        template: function RoleManagmentComponent_Template(rf, ctx) {
          if (rf & 1) {
            var _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, " Role Managment ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function RoleManagmentComponent_Template_button_click_5_listener() {
              _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r13);

              var _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](26);

              return ctx.addNewRole(_r1);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "+ Role");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "form", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function RoleManagmentComponent_Template_form_ngSubmit_9_listener() {
              return ctx.formSubmit();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "label", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Roles");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "select", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function RoleManagmentComponent_Template_select_change_15_listener($event) {
              return ctx.getScreensByRoleName($event.target.value);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, RoleManagmentComponent_option_16_Template, 2, 1, "option", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "label", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Screens");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "angular2-multiselect", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function RoleManagmentComponent_Template_angular2_multiselect_ngModelChange_21_listener($event) {
              return ctx.selectedItems = $event;
            })("onSelect", function RoleManagmentComponent_Template_angular2_multiselect_onSelect_21_listener($event) {
              return ctx.onItemSelect($event);
            })("onDeSelect", function RoleManagmentComponent_Template_angular2_multiselect_onDeSelect_21_listener($event) {
              return ctx.OnItemDeSelect($event);
            })("onSelectAll", function RoleManagmentComponent_Template_angular2_multiselect_onSelectAll_21_listener($event) {
              return ctx.onSelectAll($event);
            })("onDeSelectAll", function RoleManagmentComponent_Template_angular2_multiselect_onDeSelectAll_21_listener($event) {
              return ctx.onDeSelectAll($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "button", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "update");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](25, RoleManagmentComponent_ng_template_25_Template, 20, 3, "ng-template", null, 18, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.roleData);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.dropdownList)("ngModel", ctx.selectedItems)("settings", ctx.dropdownSettings);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgForm"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgForOf"], angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_7__["AngularMultiSelect"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ɵangular_packages_forms_forms_x"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["DefaultValueAccessor"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"]],
        styles: [".subbutton[_ngcontent-%COMP%] {\n  margin-left: 40%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFwuLlxcLi5cXHJvbGUtbWFuYWdtZW50LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksZ0JBQWlCO0FBQXJCIiwiZmlsZSI6InJvbGUtbWFuYWdtZW50LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnN1YmJ1dHRvblxyXG57XHJcbiAgICBtYXJnaW4tbGVmdDogNDAlIDtcclxufSJdfQ== */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](RoleManagmentComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-role-managment',
            templateUrl: './role-managment.component.html',
            styleUrls: ['./role-managment.component.scss']
          }]
        }], function () {
          return [{
            type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"]
          }, {
            type: _app_services_base_service_service__WEBPACK_IMPORTED_MODULE_4__["BaseServiceService"]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "qZ3D":
    /*!*************************************************************************************!*\
      !*** ./src/app/content/pages/roleManagment/role-managment/role-managment.module.ts ***!
      \*************************************************************************************/

    /*! exports provided: RoleManagmentModule */

    /***/
    function qZ3D(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RoleManagmentModule", function () {
        return RoleManagmentModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! angular2-multiselect-dropdown */
      "e/mZ");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _role_managment_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./role-managment.component */
      "Erl+");

      var appsRoutes = [{
        path: '',
        component: _role_managment_component__WEBPACK_IMPORTED_MODULE_5__["RoleManagmentComponent"]
      }, {
        path: '**',
        component: _role_managment_component__WEBPACK_IMPORTED_MODULE_5__["RoleManagmentComponent"]
      }];

      var RoleManagmentModule = function RoleManagmentModule() {
        _classCallCheck(this, RoleManagmentModule);
      };

      RoleManagmentModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: RoleManagmentModule
      });
      RoleManagmentModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function RoleManagmentModule_Factory(t) {
          return new (t || RoleManagmentModule)();
        },
        imports: [[_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_3__["AngularMultiSelectModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(appsRoutes)]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](RoleManagmentModule, {
          declarations: [_role_managment_component__WEBPACK_IMPORTED_MODULE_5__["RoleManagmentComponent"]],
          imports: [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_3__["AngularMultiSelectModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]],
          exports: [_role_managment_component__WEBPACK_IMPORTED_MODULE_5__["RoleManagmentComponent"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](RoleManagmentModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            declarations: [_role_managment_component__WEBPACK_IMPORTED_MODULE_5__["RoleManagmentComponent"]],
            imports: [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_3__["AngularMultiSelectModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(appsRoutes)],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["CUSTOM_ELEMENTS_SCHEMA"]],
            exports: [_role_managment_component__WEBPACK_IMPORTED_MODULE_5__["RoleManagmentComponent"]]
          }]
        }], null, null);
      })();
      /***/

    }
  }]);
})();
//# sourceMappingURL=roleManagment-role-managment-role-managment-module-es5.js.map