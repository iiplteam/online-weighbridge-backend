const SerialPort = require('serialport');
const Delimiter = require('@serialport/parser-delimiter');
const Readline = require('@serialport/parser-readline');
var logger = require('logger').createLogger('development.log');
const Ready = require('@serialport/parser-ready');
const args = require("args-parser")(process.argv)
console.info(args);
var request = require('request');

//return;
// node .\comm.js --bay=2 --comm=COM2 --value=0
// mongodb
var mongo_url = 'mongodb://localhost:27017/online-weighbridge'
//var mongo_url = 'mongodb+srv://admin:VsKl9x8yEWIqllxh@cluster0-m73oz.mongodb.net/online-bridge-comm?retryWrites=true&w=majority'
var Agent = require('sqlagent/mongodb').connect(mongo_url);
var nosql = new Agent();
var action = "";
action = "AWAIT_CARD";
// action = "AWAIT_CARD"  - before card
// action = "AWAIT_FILLING_PROGRESS" -
// action = "FILLING_INPROGRESS" -   Y , 1 
// action = "LOADING_COMPLETED" - G , T
var chunk = "";
var DEBUG = false;
var port;
var WRITE_COMMAND;
var oldchunk = "";
var OLDCARD = "";
var bay_no = args.bay + "";
// COMM BAY 100 / 50 / 100 / 250 
if (!DEBUG) {
    port = new SerialPort(args.comm, { baudRate: 9600 });
    port.on('open', function () {
        port.on('readable', function () {
            port.read();
        })
    })
    const parserD = port.pipe(new Delimiter({ delimiter: String.fromCharCode(30) }));
    parserD.on('data', async function (data) {
        chunk = data.toString();
        if (oldchunk != chunk) {
            console.log("DATA parserD", chunk, action);
        }

        DataProcessing(chunk);
        oldchunk = chunk;
    });
    port.on('error', function (err) {
        console.log('Error: ', err.message)
    });
}
async function DataProcessing(data) {
    var singleData;
    if (chunk.indexOf(String.fromCharCode(29))) {
        singleData = data.substring(chunk.indexOf(String.fromCharCode(29)));
    } else {
        singleData = data;
    }
    //var singleData = chunk.replace();
    //console.log("sin", singleData);
    var splitData = singleData.split(",");
    //console.log(singleData);
    var firstFieldLength = splitData[0].length;
    var CARD_NO = singleData[0];
    // console.log("firstFieldLength", splitData[0], firstFieldLength);
    // action = "AWAIT_CARD"  - before card
    // action = "AWAIT_FILLING_PROGRESS" -
    // action = "FILLING_INPROGRESS" -   Y , 1 
    // action = "LOADING_COMPLETED" - G , T

    if (firstFieldLength > 2) {
        nosql.select('customerOrder', 'customer_orders').make(function (builder) {
            builder.where('card_num', splitData[0].substring(1));
            builder.sort("creationDate", 'desc')
            builder.first();
        })
        var customerOrder = await nosql.promise('customerOrder');
        //console.log("customerOrder------------------", JSON.stringify(customerOrder));
        if (customerOrder == null) {
            //console.log("INVALID_CARD");
            var INVALID_CARD = `F#1=${String.fromCharCode(2)}IVC`;
            if (OLDCARD != CARD_NO) {
                sendCommandToScale(INVALID_CARD);
            }
            return;
        }
        if (customerOrder.action == null) {
            action = "AWAIT_CARD"
        } else {
            action = customerOrder.action;
        }
        var Wt = splitData[3];
        var vehicleNum = customerOrder.truck_number;
        var cardNum = customerOrder.card_num;
        var maxGrossWt = customerOrder.max_gross_wt;
        var x = maxGrossWt - parseInt(Wt);
        //var y = customerOrder.target_wt;
        var z = parseInt(customerOrder.qty_to_be_loaded)
        //console.log("x", x, 'y', y);
        // var t1 = Math.min(x, y);
        var t2 = Math.min(x, z);
        //console.log("targetWt", t2);
        if (args.value) {
            t2 = t2 - args.value;
        }

        if (action == "AWAIT_CARD" && singleData.indexOf("G,R") != -1) {
            nosql.update('updateBayData', 'customer_orders').make(function (builder) {
                builder.set('action', action);
                builder.set('status', 'inprogress');
                builder.set('bay_no', args.bay + "");
                builder.set('target_wt', t2);
                builder.where('card_num', cardNum);
            })
            await nosql.promise('updateBayData');

            // F#1=2S129650TS07UJ922417950000000000000050 
            var wrirtecardNum = "";
            var wrirtevechileNumber = "";
            var wrirtetargetWt = "" + (t2);
            if (wrirtetargetWt.length != 5) {
                if (wrirtetargetWt.length == 4) {
                    wrirtetargetWt = wrirtetargetWt + " ";
                }
                if (wrirtetargetWt.length == 3) {
                    wrirtetargetWt = wrirtetargetWt + "  ";
                }
            }
            if (cardNum.length == 4) {
                wrirtecardNum = cardNum;
            }
            if (cardNum.length == 3) {
                wrirtecardNum = cardNum + " ";
            }
            if (vehicleNum.length == 10) {
                wrirtevechileNumber = vehicleNum;
            }
            if (vehicleNum.length == 9) {
                wrirtevechileNumber = vehicleNum + " ";
            }
            if (vehicleNum.length == 8) {
                wrirtevechileNumber = vehicleNum + "  ";
            }
            if (vehicleNum.length == 7) {
                wrirtevechileNumber = vehicleNum + "   ";
            }
            if (vehicleNum.length == 6) {
                wrirtevechileNumber = vehicleNum + "    ";
            }
            WRITE_COMMAND = `F#1=2S1${bay_no}${wrirtecardNum}${wrirtevechileNumber}${wrirtetargetWt}000000000000050 1`;
            sendCommandToScale(WRITE_COMMAND);
            sendCommandToScale(WRITE_COMMAND);
            setTimeout(function () {
                WRITE_COMMAND = `F#1=2S1${bay_no}${wrirtecardNum}${wrirtevechileNumber}${wrirtetargetWt}0000000000TOK50 1`;
            }, 500);
        }
        if (singleData.indexOf('G,V') != -1 && action == "AWAIT_CARD") {
            action = "AWAIT_FILLING_PROGRESS";
            var bay_status = "Engaged";

            var dataObj = {
                actual_wt: parseInt(Wt),
                earthing_connected: splitData[4],
                loading_started: splitData[5],
                loading_complete: splitData[9],
                //target_wt: targetWt,
                card_punch_time: new Date(),
                bay_status: bay_status,
                target_wt_write: t2
            }
            nosql.update('updateBayData', 'customer_orders').make(function (builder) {
                builder.set('action', action);
                builder.set('status', 'inprogress');
                builder.set(dataObj);
                builder.where('card_num', cardNum);
            });
            await nosql.promise('updateBayData');
        }
        if (singleData.indexOf('Y,1') != -1 && action == "AWAIT_FILLING_PROGRESS") {
            //console.log("dataObj", dataObj);
            sendCommandToScale(WRITE_COMMAND);

            action = "FILLING_INPROGRESS";
            var dataObj = {
                filling_progress_wt: 0,
                earthing_connected: splitData[4],
                loading_started: splitData[5],
                loading_complete: splitData[9],
                //target_wt: targetWt,
                //card_punch_time: new Date()
            }
            nosql.update('updateBayData', 'customer_orders').make(function (builder) {
                builder.set(dataObj);
                builder.set('start_time', new Date());
                builder.set('action', action);
                builder.set('status', 'inprogress');
                builder.where('card_num', cardNum);
            })
            await nosql.promise('updateBayData');

            WRITE_COMMAND = "";
        }
        if (singleData.indexOf('N,V') != -1 && action == "FILLING_INPROGRESS") {

            action = "FILLING_INPROGRESS";
            var dataObj = {
                filling_progress_wt: parseInt(Wt),
                earthing_connected: splitData[4],
                loading_started: splitData[5],
                loading_complete: splitData[9],
                //target_wt: targetWt,
                //card_punch_time: new Date()
            }
            nosql.update('updateBayData', 'customer_orders').make(function (builder) {
                builder.set(dataObj);
                builder.set('action', action);
                builder.set('status', 'inprogress');
                builder.where('card_num', cardNum);
            })
            await nosql.promise('updateBayData');
        }
        if (singleData.indexOf('N,T') != -1 && action == "FILLING_INPROGRESS") {
            //console.log("dataObj", dataObj);
            action = "FILLING_INPROGRESS";
            var dataObj = {
                filling_progress_wt: parseInt(Wt),
                earthing_connected: splitData[4],
                loading_started: splitData[5],
                loading_complete: splitData[9],
                //target_wt: targetWt,
                //card_punch_time: new Date()
            }
            nosql.update('updateBayData', 'customer_orders').make(function (builder) {
                builder.set(dataObj);
                builder.set('action', action);
                builder.set('status', 'inprogress');
                builder.where('card_num', cardNum);
            })
            var updateBayData = await nosql.promise('updateBayData');
            // console.log("updateBayData", updateBayData);
        }
        if (singleData.indexOf() != -1) {
            var STOP_COMMAND = `F#1=${String.fromCharCode(2)}O`;
            sendCommandToScale(STOP_COMMAND);
        }
        if (singleData.indexOf('G,T') && (singleData.indexOf('Y,0,1') != -1 || singleData.indexOf('N,0,1') != -1) && action == "FILLING_INPROGRESS") {
            action = "LOADING_COMPLETED";

            var dataObj = {
                // filling_progress_wt: parseInt(Wt),
                earthing_connected: splitData[4],
                loading_started: splitData[5],
                loading_complete: splitData[9],
                //target_wt: targetWt,
                gross_wt: parseInt(Wt)
                //card_punch_time: new Date()
            }
            nosql.update('updateBayData', 'customer_orders').make(function (builder) {
                builder.set(dataObj);
                builder.set('end_time', new Date());
                builder.set('action', action);
                builder.set('status', 'completed');
                builder.where('card_num', cardNum);
            })
            await nosql.promise('updateBayData');

            setTimeout(function () {
                moveCustomerOrder(customerOrder._id);
            }, 100000);
        }
    }

    OLDCARD = CARD_NO;

    if (firstFieldLength < 2 && singleData.indexOf('G,R') != -1) {
        action = "AWAIT_CARD";
    }
}
async function moveCustomerOrder(id) {
    nosql.select('fetchCustomerOrder', 'customer_orders').make(function (builder) {
        builder.where('_id', id);
        builder.first();
    });
    var customerOrder = await nosql.promise('fetchCustomerOrder');
    if (customerOrder == null) {
        return;
    }
    customerOrder.temp_id = customerOrder._id;
    delete customerOrder._id;
    nosql.insert('reportSave', 'customer_orders_report').make(function (builder) {
        builder.set(customerOrder);
    });
    nosql.remove('removeTempOrder', 'customer_orders').make(function (builder) {
        builder.where('_id', customerOrder.temp_id);
    });
    await nosql.promise('reportSave');
    await nosql.promise('removeTempOrder');
    sendCommandToPrinter(customerOrder.serial_id);
}

function sendCommandToPrinter(id) {

    var options = {
        'method': 'POST',
        'url': 'http://localhost:3000/api/customer/order-bill?secure=1',
        'headers': {
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwNDIxMGE2OGY5MDU4NGFhOGQ5YzgxYiIsImlhdCI6MTYxNjQ5NTYwNiwiZXhwIjoxNjE5MDg3NjA2fQ.OfvGCT0gPCfJWA9HjhnzXS_A-heVgJEaEDTj5Z4VvWo',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ "id": id })

    };
    request(options, function (error, response) {
        if (error) throw new Error(error);
        console.log(response.body);
        setTimeout(function(){
            var options = {
                'method': 'GET',
                'url': 'http://localhost:3000/api/customer/print-order?secure=1',
                'headers': {
                    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwNDIxMGE2OGY5MDU4NGFhOGQ5YzgxYiIsImlhdCI6MTYxNjQ5NTYwNiwiZXhwIjoxNjE5MDg3NjA2fQ.OfvGCT0gPCfJWA9HjhnzXS_A-heVgJEaEDTj5Z4VvWo'
                }
            };
    
            request(options, function (error, response) {
                if (error) throw new Error(error);
                console.log(response.body);
            });
        }, 10000);
        
    });

}

function sendCommandToScale(command) {
    console.log(`WRITE : ${command}${String.fromCharCode(3)}${String.fromCharCode(13)}`);
    if (!DEBUG) {
        port.write(Buffer.from(command + String.fromCharCode(3) + String.fromCharCode(13), function (err) { }));
    }
}
// var counter = 0;
// var writecounter = 0;
// var oldchunk = "";
// setInterval(function () {
//     if (oldchunk != chunk) {
//         //console.log("CHUNK:", counter, chunk);
//         logger.info("CHUNK:", counter, chunk);
//     }
//     oldchunk = chunk;
//     if ((writecounter == 0 || writecounter == 1 || writecounter == 2 || writecounter == 3) && chunk != "" && chunk.indexOf("9650") != -1) {
//         //console.log("WRITE", Buffer.from("2S129626TN88C4959 100000000000000TOK50 13"));
//         //port.write(Buffer.from(String.fromCharCode(2)+'2S129716TN88C4959 100000000000000TOK50 1'+String.fromCharCode(3)))
//         var v = [];
//         v[0] = "F#1=2S129650TS07UJ922417950000000000000050 1";
//         v[1] = "F#1=2S129650TS07UJ922417950000000000000050 1";
//         v[2] = "F#1=2S129650TS07UJ9224179500000000000TOK50 1";
//         v[3] = `F#1=${String.fromCharCode(2)}O`;
//         if (writecounter == 2) {
//             if (chunk.indexOf("Y,1,0") != -1) {
//                 console.log("WRITE", v[writecounter] + String.fromCharCode(3));
//                 port.write(Buffer.from(v[writecounter] + String.fromCharCode(3) + String.fromCharCode(13), function (err) {
//                     console.log(err);
//                 }));
//                 writecounter++;
//             }
//         } else if (writecounter == 3) {
//             if (chunk.indexOf("G,T") != -1) {
//                 console.log("WRITE", v[writecounter] + String.fromCharCode(3));
//                 port.write(Buffer.from(v[writecounter] + String.fromCharCode(3) + String.fromCharCode(13), function (err) {
//                     console.log(err);
//                 }));
//                 writecounter++;
//             }
//         } else if(chunk.indexOf("G,R" != -1)) {
//             console.log("WRITE", v[writecounter] + String.fromCharCode(3));
//             port.write(Buffer.from(v[writecounter] + String.fromCharCode(3) + String.fromCharCode(13), function (err) {
//                 console.log(err);
//             }));
//             writecounter++;
//         }
//     }
//     //chunk = "";
//     counter++;
// }, 1000);
// TN88C4959
//console.log("BUFFER", Buffer.from(String.fromCharCode(2)+"IVC"+String.fromCharCode(3)));
//console.log("WRITE", Buffer.from(String.fromCharCode(2)+'2S129716TN88C4959 100000000000000TOK50 1'+String.fromCharCode(3)));
//console.log("WRITE", Buffer.from("2S129626TN88C4959 100000000000000TOK50 13"));
//port.write(Buffer.from("2S129716TN88C4959 100000000000000TOK50 13"))
//console.log("WRITE", Buffer.from("2IVC3"));
//port.write(Buffer.from("2IVC3"))
// function to save the comm data into db
var dataArr = [
    ',G,R,17600,Y,0,0,0,0,0',

    '9999,G,R,17600,Y,0,0,0,0,0',
    '9999,G,V,17600,Y,0,0,0,0,0', // after writing the  product wt data  to the scale R changes to V

    '9999,G,V,17600,Y,1,0,0,0,0', // loading started
    '9999,G,V,17600,Y,1,0,0,0,0', // write tok to the scale
    '9999,N,V,00000,Y,1,0,0,0,0',
    '9999,N,V,00000,Y,1,0,0,0,0',
    '9999,N,V,00000,Y,1,0,0,0,0',
    '9999,N,T,00000,Y,1,0,0,0,0', // N , V - filling progress started

    '9999,N,T,01230,Y,1,0,0,0,0',
    '9999,N,T,18670,Y,1,0,0,0,0',
    '9999,G,T,18660,Y,1,0,0,0,0',
    '9999,G,T,18660,Y,1,0,0,0,0',
    '9999,G,T,18660,Y,1,0,0,0,0',
    '9999,G,T,18660,Y,1,0,0,0,1', // filling completed
    '9999,G,T,18660,Y,1,0,0,0,1',
    '9999,G,T,18580,Y,1,0,0,0,1',
    '9999,G,T,18220,Y,1,0,0,0,1', // filling progress completed
    ',G,R,17600,Y,0,0,0,0,0',

]
/*
'9999 - cardno (,G  ,R ) - tare wt ,17600 -truckwt  ,Y - earthing  ,0 - loading not started   ,0 - ,0  ,0  ,0'
'9999 - cardno ,G  ,V  ,17600 -truckwt  ,Y - earthing ,0  ,0  ,0  ,0  ,0'
'9999 - cardno ,N  ,V  ,00000 -truckwt  ,Y - earthing ,1 - loading started ,0  ,0  ,0  ,0'
'9999 - cardno ,N  ,T  ,00000 -truckwt  ,Y - earthing ,1  ,0  ,0  ,0  ,0'
'9999,(G,T) - filling completed,18220,Y,1,0,0,0,1'
TN28BE2437
17300
7269
17950
*/
// saveCommData();
// STR1 - CARD NO 
// STR2 -  TRUCK NO 
// STR3 - 
// 21129626TN88C4959 2000 0000000000TOK50 13 
// MSComm8.Output = Chr(2) & Trim(rs!Loading_Type) & Trim(rs!NoOfWmnt) & Trim(rs!Bayno) & str1 & str2 & str3 & "00000" & "00000" & "TOK" & str9 & wmnt_cycle_bay8 & Chr(3)