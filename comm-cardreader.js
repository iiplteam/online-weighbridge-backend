const SerialPort = require('serialport');
const Delimiter = require('@serialport/parser-delimiter');
const InterByteTimeout = require('@serialport/parser-inter-byte-timeout')
const args = require("args-parser")(process.argv)
console.info(args);
// comm.js --comm=COM2
var port;
var DEBUG = false;
if (!DEBUG) {
    port = new SerialPort(args.comm, { baudRate: 57600, parity: "even" });
    port.on('open', function () {
        port.on('readable', function () {
            port.read();
        })
    })
    //const parserD = port.pipe(new SpacePacketParser({ timeCodeFieldLength: 32 }))
    //const parserD = port.pipe(new Delimiter({ delimiter: String.fromCharCode(2) }));
    const parserD = port.pipe(new InterByteTimeout({interval: 30}))
    parserD.on('data', async function (data) {
        chunk = data.toString("hex");
        DataProcessing(chunk);
    });
    port.on('error', function (err) {
        console.log('Error: ', err.message)
    });
}
function DataProcessing(chunk) {
    console.log('chunk', chunk);
    var card_data = chunk.split("02");
    var cd_no = ("0x"+card_data[1] >> 1)
    console.log("CARD NO", cd_no);
}