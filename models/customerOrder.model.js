var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');
var autoIncrement = require('mongoose-auto-increment');
var configdata = require('../config');
var MONGO_DB_CONNECTION = configdata.mongo;
var connection = mongoose.createConnection(MONGO_DB_CONNECTION);
 
autoIncrement.initialize(connection);

var customerOrderSchema = Schema({
    card_num: { type: String },
    bay_no:{type: String},
    truck_number: { type: String},
    product_code: { type: String },
    customer_code: { type: String },
    tare_wt: { type: Number },
    max_gross_wt: { type: Number },
    ccoe_wt: { type: String },
    loading_type: {
        number_of_loading: { type: String },
        tare_wt_one: { type: Number },
        tare_wt_two: { type: Number },
        tare_wt_three: { type: Number },
    },
    coarse_set_point: { type: String },
    qty_to_be_loaded: { type: String },
    customer_order_no: { type: String },
    transporter_name: { type: String },
    product_name: { type: String },
    customer_name: { type: String },
    destination: { type: String },
    srv_due_date: { type : String },
    hydro_test_due_date: { type : String },
    fire_extinguisher_date: { type : String },
    insurance_due_date: { type : String },
    ccoe_licence_date: { type : String },
    efcv_due_date: { type : String },
    fc_due_date: { type : String },
    read_permit_date: { type : String },
    remark: { type: String },
    status: { type: String },
    creationDate: { type : Date },
    lastUpdateDate: { type : String },
    serial_id: {type: Number, unique:true},
    is_loading :{type: Boolean}
});

customerOrderSchema.plugin(autoIncrement.plugin, {
    model: 'customer_order',
    field: 'serial_id',
    //startAt: 1000,
    //incrementBy: 700
//    startAt: 1
});
customerOrderSchema.pre('save', function (next) {
    this.creationDate = new Date();
    next();
})

customerOrderSchema.plugin(mongoosePaginate);
customerOrderSchema.plugin(require('mongoose-autopopulate'));

module.exports = mongoose.model('customer_order', customerOrderSchema);
