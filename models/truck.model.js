var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');

var truckSchema = Schema({
    truck_number: { type: String, unique: true },
    product_code: { type: String },
    customer_code: { type: String },
    tare_wt: { type: Number },
    max_gross_wt: { type: Number },
    coarse_set_point: { type: String},
    srv_due_date: { type: Date },
    efcv_due_date: { type: Date },
    road_permit_date: { type: Date},
    fc_due_date: { type: Date},
    insurance_due_date: { type: Date},
    special_remarks: { type: String},
    transporter_name: { type: String},
    product_name: { type: String},
    customer_name: { type: String},
    destination: { type: String},
    target_wt: { type: Number},
    ccoe_wt: { type: Number},
    ccoe_licence_date: { type: Date},
    hydro_test_due_date: { type: Date},
    fire_extinguisher_date: { type: Date},
    remarks: {type: String},
    creationDate: { type: Date },
    lastUpdateDate: { type: Date }
});

truckSchema.pre('save', function (next) {
    this.creationDate = new Date();
    next();
})

truckSchema.plugin(mongoosePaginate);
truckSchema.plugin(require('mongoose-autopopulate'));

module.exports = mongoose.model('truck_master', truckSchema);

