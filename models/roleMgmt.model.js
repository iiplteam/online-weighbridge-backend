var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');

var roleMgmt = Schema({
    roleName: { type: String, unique: true },
    screenData : { type: Array },
    creationDate : { type : Date}
});

roleMgmt.pre('save', function (next) {
    this.creationDate = new Date();
    next();
});

module.exports = mongoose.model('role_management', roleMgmt);