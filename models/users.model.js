var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');


var users = Schema({
    userName: { type: String, required: true ,unique: true,},
    password: {
        type: String,
        required: [true, 'Please add a password'],
        minlength: 6,
        select: false
    },
    roleName: { type: String },
    email: {
        type: String,
        required: [true, 'Please add an email'],
        match: [
            /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
            'Please add a valid email'
        ]
    },
    phoneNo: { type: String },
    creationDate: { type: Date, default: Date.now },
    lastUpdateDate: { type: Date, default: Date.now },
    isActive : { type: Boolean , default:true }
});

users.pre('save', function (next) {
    this.creationDate = new Date();
    next();
});

// Encrypt password using bcrypt
users.pre('save', async function (next) {
    if (!this.isModified('password')) {
        next();
    }

    const salt = await bcrypt.genSalt(10);
    this.password = await bcrypt.hash(this.password, salt);
});





module.exports = mongoose.model('users', users);