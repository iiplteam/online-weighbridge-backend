var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');

var configusers = Schema({
    configName: { type: String, unique: true },
    configData : { type: String },
    
});

configusers.pre('save', function (next) {
    this.creationDate = new Date();
    
    next();
});

module.exports = mongoose.model('configusers', configusers);