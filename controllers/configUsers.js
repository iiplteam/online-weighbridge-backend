const { json } = require('express');
const configusersModel = require('../models/configusers.model');
module.exports = {
    getUsers,
    configusers,
    updateUsers
}

async function configusers(req, res) {
    var body = {};
    body.configName = req.body.configName;
    let objList = JSON.stringify(req.body.configData);
    body.configData = objList;
    var dataObj = new configusersModel(body);
    dataObj.save(function (err, docs) {
        // console.log("docs",docs);
        if (err) {

            res.send({ status: false, message: err.message })
        }

        else if (docs != null) {
            res.send({ status: true, message: "Saved successfully" });
        } else {
            res.send({ status: false, message: "Save fail" })
        }
    });

}

async function getUsers(req, res) {

    configusersModel.find().lean().exec(function (err, docs) {
        if (err) {
            res.send({
                status: false,
                message: err
            })
        } else if (docs != null) {
           
            var data =[];
            for (let i = 0; i < docs.length; i++) {
                element = docs[i];
                element.configData = JSON.parse(docs[i].configData);
                data.push(element);
            }
            console.log("DDDDDDDDDDd",data);
            res.json({
                status: true,
                message: "Success",
                data: data
            });
        } else {
            res.send({
                status: false,
                message: "No data found",
                data: []
            })
        }
    });
}

function updateUsers(req, res) {
    var dataObj = JSON.stringify(req.body.configData);
    configusersModel.findOneAndUpdate({ _id: req.params.id }, { $set: {configData:dataObj} }, function (err, docs) {
        if (err) {
            res.send({ status: false, message: err.message });
        } else if (docs != null) {
            res.send({ status: true, message: "User Details updated successfully" });
        } else {
            res.send({ status: false, message: "User Details updated fail" });
        }
    });

}