
// mongo db connection 
var config = require('../config');
const roleMgmtModel = require('../models/roleMgmt.model');


module.exports = {
    addRole,
    getRoles,
    updateRoleById,
    getRoleByName
}


// Post Method
// Saved Roles 
function addRole(req, res) {
    console.log("reaaaa",req.body);
    var body = req.body;
    var dataObj = new roleMgmtModel(body);
    console.log("dddd",dataObj);
    if (!dataObj) {
        return res.send({ status: false, message: "Role Alredy exists" })
    } else {
        dataObj.save(function (err, docs) {
            console.log("DOCS",docs);
            if (err) {
                res.send({ status: false, message: err.message })
            }
            else if (docs != null) {
                res.send({ status: true, message: "Role saved successfully" });
            } else {
                res.send({ status: false, message: "Save fail" })
            }
        });
    }

   
}


// GET Method
// Get all Roles
function getRoles(req,res) {
    roleMgmtModel.find({},{creationDate: 0,__v : 0}).exec(function(err, docs) {
        console.log(docs);
      
        if (err) {
            res.json({
                status: false,
                message: err.message
            })
        }
        else if (docs.length > 0) {        
            res.json({
                status: true,
                message: "Success",
                data: docs
            })
        }
        else {
            res.json({
                status: false,
                message: "Data not found"
            })
        }
    })

}

//PUT Method
// Update each role by 
function updateRoleById(req,res) {
    var body =  req.body;

    body.lastUpdateDate = new Date();
    roleMgmtModel.findOneAndUpdate({roleName :body.roleName},{ $set: body }, { new: true }).exec(function (err,docs) {
        if (err) {
            res.json({
                status: false,
                message: err.message
            })
        }
        else if (!docs) {        
            res.json({
                status: false,
                message: "Role not found"
            })
        }
        else {
            res.json({
                status: true,
                message: "Role Updated"
            })
        
        }
    });

}


// Get Method
// Get Role by RoleName
function getRoleByName(req,res) {
    let query = {};
    
    query.roleName = req.query.roleName;
    roleMgmtModel.findOne(query).exec(function (err,docs) {
         if (err) {
             res.json({
                 status: false,
                 message: err.message
             })
         }
         else if (docs) {        
             res.json({
                 status: true,
                 message: "Sucess",
                 data:docs
             })
         }
         else {
             res.json({
                 status: false,
                 message: "Role not found"
             })
         }
     })
 }
