const truckModel = require('../models/truck.model');
const customerOrderModel = require('../models/customerOrder.model');
var logsEmitter = require('../emitters/logsEmitter');
module.exports = {
    addTruck,
    updateTruck,
    getTrucks,
    getTruckById,
    deleteTruck
}


function addTruck(req, res) {
    var body = req.body;
    var decoded = req.token;
    body.srv_due_date = new Date(body.srv_due_date);
    //body.eecv_due_date = new Date(body.eecv_due_date);
    body.read_permit_date = new Date(body.read_permit_date);
    body.fc_due_date = new Date(body.fc_due_date);
    body.insurance_due_date = new Date(body.insurance_due_date);
    body.ccoe_licence_date = new Date(body.ccoe_licence_date);
    body.ccoe_licence_date = new Date(body.ccoe_licence_date);
    body.hydro_test_due_date = new Date(body.hydro_test_due_date);
    body.fire_extinguisher_date = new Date(body.fire_extinguisher_date);
    var dataObj = new truckModel(req.body);
    //console.log(body.installDate,body.manufactureDate );
    //console.log("dataObj",dataObj);
    dataObj.save(function (err, docs) {
        // console.log("docs",docs);
        if (err) {
            if (err.message.toString().includes("duplicate")) {
                res.send({ status: false, message: "Truck Number Already exists" })
            } else {
                res.send({ status: false, message: err.message })
            }

        } else if (docs != null) {
            res.send({ status: true, message: "Truck Details saved successfully" });
            var logObj = {
                "user_name" : decoded.userName,
                "user_id": decoded._id,
                "event_type":"TRUCK-CREATE",
                "event_name": `TRUCK-MASTER-CREATE-${docs.truck_number}`,
                "log_id":docs.truck_number,
                "data": JSON.stringify(docs),
                "Created_on":new Date()
            };
            logsEmitter.emit('logs', logObj);
        } else {
            res.send({ status: false, message: "Save fail" })
        }
    })

}

function updateTruck(req, res) {
    var dataObj = req.body;
    var decoded = req.token;
    truckModel.updateOne({ _id: req.params.id }, { $set: dataObj }, function (err, docs) {
        if (err) {
            res.send({ status: false, message: err.message });
        } else if (docs != null) {
            res.send({ status: true, message: "Truck Details updated successfully" });
            var logObj = {
                "user_name" : decoded.userName,
                "user_id": decoded._id,
                "event_type":"TRUCK-UPDATE",
                "event_name": `TRUCK-MASTER-UPDATE-${docs.truck_number}`,
                "log_id":docs.truck_number,
                "data": JSON.stringify(docs),
                "Created_on":new Date()
            };
            logsEmitter.emit('logs', logObj);
        } else {
            res.send({ status: false, message: "Truck Details updated fail" });
        }
    });

}

async function getTrucks(req, res) {
    var query = {};
    let limit = parseInt(req.query.limit);
    let page = parseInt(req.query.page);
    truckModel.find(query,{truck_number:1}, function (err, docs) {
        if (err) {
            res.send({
                status: false,
                message: err
            })
        } else if (docs != null) {
            res.json({
                status: true,
                message: "Success",
                data: docs
            });
        } else {
            res.send({
                status: false,
                message: "No data found",
                data: []
            })
        }
    });
}

function getTruckById(req, res) {
    truckModel.findOne({ truck_number: req.params.id }, function (err, docs) {
        if (err) {
            res.send({
                status: false,
                message: err.message
            })
        } else if (docs != null) {
            res.send({
                status: true,
                message: "Success",
                data: docs
            });
        } else {
            res.send({
                status: false,
                message: "No data found",
                data: []
            })
        }
    })
}

function deleteTruck(req, res) {
    var decoded = req.token;
    truckModel.remove({ _id: req.params.id }, function (err, docs) {
        console.log("docs", docs);
        if (err) {
            res.send({
                status: false,
                message: err
            })
        } else if (docs != null) {
            res.send({
                status: true,
                message: "Truck Details deleted successfully"
            })
            var logObj = {
                "user_name" : decoded.userName,
                "user_id": decoded._id,
                "event_type":"TRUCK-DELETE",
                "event_name": `TRUCK-MASTER-DELETE-${docs.truck_number}`,
                "log_id":docs.truck_number,
                "data": JSON.stringify(docs),
                "Created_on":new Date()
            };
            logsEmitter.emit('logs', logObj);
        } else {
            res.send({
                status: false,
                message: "Truck delete fail"
            })
        }
    })
}



