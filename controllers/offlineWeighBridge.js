
var config = require('../config');
var MONGO_DB_CONNECTION = config.mongo;
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);

module.exports = {
    saveRecord,
    getRecords,
    getSingle
}


async function saveRecord(req, res) {  

    var body = req.body;
    var nosql = new Agent();
    if (!body) {
        res.send({
            status: false,
            message: "no data"
        })
    } else {
        

        nosql.select('docs', 'offlineWeighBridge').make(function (builder) {
            builder.where('cardNum', body.cardNum);
            builder.first();
        });

        var docs = await nosql.promise('docs');
      

        if (docs != null) {
            body.dateupdated = new Date();
            nosql.update('offlineData', 'offlineWeighBridge').make(function (builder) {
                builder.where('cardNum', body.cardNum);
               
                builder.set(body);
            });
            await nosql.promise('offlineData');
            res.send({
                status: true,
                message: "Updated Successfully"
            })
        } else {
            body.datecreated = new Date();
            body.dateupdated = new Date();
            nosql.insert('offlineData', 'offlineWeighBridge').make(function (builder) {
                builder.set(body);
            });
            await nosql.promise('offlineData');
            res.send({
                status: true,
                message: "Saved Successfully"
            })
        }
    }
}

async function getRecords(req, res) {  
    var nosql = new Agent();
    nosql.select('docs', 'offlineWeighBridge').make(function (builder) {
        
    });

    var docs = await nosql.promise('docs');
    if (docs != null) {
  
        res.send({
            status: true,
            message: "Success",
            data:docs
        })
    } else {
       
        res.send({
            status: false,
            message: "No Data found"
        })
    }
}

async function getSingle(req, res) {  
    console.log("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLL");
    var opt = req.query;
    var nosql = new Agent();
    console.log("DDDDDDd",opt.cardNum);
    nosql.select('docs', 'offlineWeighBridge').make(function (builder) {
        opt.cardNum && builder.where('cardNum',parseInt(opt.cardNum));
        builder.first();
    });

    var docs = await nosql.promise('docs');
    if (docs != null) {
  
        res.send({
            status: true,
            message: "Success",
            data:docs
        })
    } else {
       
        res.send({
            status: false,
            message: "No Data found"
        })
    }
}