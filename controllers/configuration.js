var config = require('../config');
var MONGO_DB_CONNECTION = config.mongo;
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);
const { exec } = require("child_process");
const EventEmitter = require('events');
const myEmitter = new EventEmitter();
const commEmmitter = require('../emitters/commEmiiter');
module.exports = {
    commConfig,
    getCommConfig,
    globalConfig,
    getGlobalConfig,
    BayConfig,
    getBayConfig
}

async function BayConfig(req, res) {
    var data = req.body;
    var nosql = new Agent();
    if (!data) {
        res.send({
            status: false,
            message: "no data"
        })
    } else {
        nosql.select('docs', 'configuration').make(function (builder) {
            builder.where('configurationName', 'bay_config');
            builder.first();
        });

        var docs = await nosql.promise('docs');
        
        if (docs != null) {
            nosql.update('saveHomepageRaw', 'configuration').make(function (builder) {
                builder.where('configurationName', 'bay_config');
                builder.set('configurationDetails', data);
            });
            await nosql.promise('saveHomepageRaw');
            commEmmitter.emit('comm-service');
            res.send({
                status: true,
                message: "Updated Successfully"
            })

        } else {
            nosql.insert('saveHomepageRaw', 'configuration').make(function (builder) {
                builder.set('configurationName', 'bay_config');
                builder.set('configurationDetails', data);
            });
            await nosql.promise('saveHomepageRaw');
            commEmmitter.emit('comm-service');
            res.send({
                status: true,
                message: "Saved Successfully"
            })
        }
    }
}

async function getBayConfig(req, res) {

    var nosql = new Agent();
    nosql.select('docs', 'configuration').make(function (builder) {
        builder.where('configurationName', 'bay_config');
        builder.first();
    });

    var docs = await nosql.promise('docs');
    //console.log("docs", docs);
    if (docs != null) {

        res.send({
            status: true,
            message: "Success",
            data: docs
        })
    } else {

        res.send({
            status: false,
            message: "No Data found"
        })
    }
}

async function commConfig(req, res) {
    var data = req.body;
    var nosql = new Agent();
    if (!data) {
        res.send({
            status: false,
            message: "no data"
        })
    } else {
        nosql.select('docs', 'configuration').make(function (builder) {
            builder.where('configurationName', 'comm_config');
            builder.first();
        });

        var docs = await nosql.promise('docs');
        
        if (docs != null) {
            nosql.update('saveHomepageRaw', 'configuration').make(function (builder) {
                builder.where('configurationName', 'comm_config');
                builder.set('configurationDetails', data);
            });
            await nosql.promise('saveHomepageRaw');
           
            res.send({
                status: true,
                message: "Updated Successfully"
            })

        } else {
            nosql.insert('saveHomepageRaw', 'configuration').make(function (builder) {
                builder.set('configurationName', 'comm_config');
                builder.set('configurationDetails', data);
            });
            await nosql.promise('saveHomepageRaw');
          
            res.send({
                status: true,
                message: "Saved Successfully"
            })
        }
    }
}

async function getCommConfig(req, res) {

    var nosql = new Agent();
    nosql.select('docs', 'configuration').make(function (builder) {
        builder.where('configurationName', 'comm_config');
        builder.first();
    });

    var docs = await nosql.promise('docs');
    console.log("docs", docs);
    if (docs != null) {

        res.send({
            status: true,
            message: "Success",
            data: docs
        })
    } else {

        res.send({
            status: false,
            message: "No Data found"
        })
    }
}

async function globalConfig(req, res) {
    var data = req.body;
    var nosql = new Agent();
    if (!data) {
        res.send({
            status: false,
            message: "no data"
        })
    } else {
        nosql.select('docs', 'configuration').make(function (builder) {
            builder.where('configurationName', 'global_config');
            builder.first();
        });

        var docs = await nosql.promise('docs');
        console.log("docs", docs);
        if (docs != null) {
            nosql.update('saveHomepageRaw', 'configuration').make(function (builder) {
                builder.where('configurationName', 'global_config');
                builder.set('configurationDetails', data);
            });
            await nosql.promise('saveHomepageRaw');
            res.send({
                status: true,
                message: "Updated Successfully"
            })
        } else {
            nosql.insert('saveHomepageRaw', 'configuration').make(function (builder) {
                builder.set('configurationName', 'global_config');
                builder.set('configurationDetails', data);
            });
            await nosql.promise('saveHomepageRaw');
            res.send({
                status: true,
                message: "Saved Successfully"
            })
        }
    }
}

async function getGlobalConfig(req, res) {

    var nosql = new Agent();
    nosql.select('docs', 'configuration').make(function (builder) {
        builder.where('configurationName', 'global_config');
        builder.first();
    });

    var docs = await nosql.promise('docs');
    console.log("docs", docs);
    if (docs != null) {

        res.send({
            status: true,
            message: "Success",
            data: docs
        })
    } else {

        res.send({
            status: false,
            message: "No Data found"
        })
    }
}





