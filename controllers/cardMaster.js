
var config = require('../config');
var MONGO_DB_CONNECTION = config.mongo;
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);

module.exports = {
    saveCard,
    getCards,
    getCard
}


async function saveCard(req, res) {  

    var body = req.body;
    var nosql = new Agent();
    if (!body) {
        res.send({
            status: false,
            message: "no data"
        })
    } else {
        

        nosql.select('docs', 'cardmaster').make(function (builder) {
            builder.where('number', body.number);
            builder.first();
        });

        var docs = await nosql.promise('docs');
      

        if (docs != null) {
            body.dateupdated = new Date();
            nosql.update('cardData', 'cardmaster').make(function (builder) {
                builder.where('number', body.number);
                builder.set(body);
            });
            await nosql.promise('cardData');
            res.send({
                status: true,
                message: "Updated Successfully"
            })
        } else {
            body.datecreated = new Date();
            body.dateupdated = new Date();
            nosql.insert('cardData', 'cardmaster').make(function (builder) {
                builder.set(body);
            });
            await nosql.promise('cardData');
            res.send({
                status: true,
                message: "Saved Successfully"
            })
        }
    }
}

async function getCards(req, res) {  
    var nosql = new Agent();
    nosql.select('docs', 'cardmaster').make(function (builder) {
        
    });

    var docs = await nosql.promise('docs');
    if (docs != null) {
  
        res.send({
            status: true,
            message: "Success",
            data:docs
        })
    } else {
       
        res.send({
            status: false,
            message: "No Data found"
        })
    }
}

async function getCard(req, res) {  
    var opt = req.query;
    var nosql = new Agent();
    nosql.select('docs', 'cardmaster').make(function (builder) {
        opt.number && builder.where('number',parseInt(opt.number));
        builder.first();
    });

    var docs = await nosql.promise('docs');
    if (docs != null) {
  
        res.send({
            status: true,
            message: "Success",
            data:docs
        })
    } else {
       
        res.send({
            status: false,
            message: "No Data found"
        })
    }
}