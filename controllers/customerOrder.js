const customerOrderModel = require('../models/customerOrder.model');

// mongo db connection 
var config = require('../config');
var MONGO_DB_CONNECTION = config.mongo;
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);
const { createLoadingBill } = require("../externalModules/createLoadingBill");
var moment = require('moment');
var logsEmitter = require('../emitters/logsEmitter');
var notifictionEmitter = require('../emitters/notificationsEmitter');
var ipp = require('ipp');
var fs = require('fs');
var path = require('path');
module.exports = {
    addcustomerOrder,
    getcustomerOrders,
    getcustomerOrdersCopmpleted,
    getcustomerOrdersCancelled,
    getCustomerOrderStatus,
    cancleCustomerOrder,
    cardNumVerify,
    truckVerify,
    orderBill,
    cancelInprogressOrder,
    getCancelOrderNotifyCount,
    getCancelOrderNotify,
    printWeighmentData,
    getDeviatedOrders,
    resetOrder,
    quantityCompletedData,
    quantityBookedData,
    customerOrders
}

async function customerOrders(req, res) {
    var nosql = new Agent();
    var opt = req.query;
    if (!opt.fromDt) {
        fromDt = new Date(new Date().setUTCHours(0, 0, 0, 0));
    } else {
        fromDt = new Date(new Date(opt.fromDt).setUTCHours(0, 0, 0, 0))
    }

    if (!opt.toDt) {
        toDt = new Date(new Date().setUTCHours(23, 59, 59, 999));
    } else {
        toDt = new Date(new Date(opt.toDt).setUTCHours(23, 59, 59, 999));
    }
    nosql.select('truckReport', 'customer_orders_report').make(function (builder) {
        builder.query('creationDate', {
            $gte: fromDt,
            $lte: toDt
        });
        //builder.where('bay_no', bay + "");
        builder.fields('truck_number', 'max_gross_wt', 'bay_no', 'tare_wt', 'customer_code', 'product_code');
        console.log(builder.builder);
    })

    var truckReport = await nosql.promise('truckReport');
    //console.table(truckReport);
    var productsObj = {};
    var customerObj = {};
    
    if (truckReport.length > 0) {
        for (let i = 0; i < truckReport.length; i++) {
            const element = truckReport[i];
            if (productsObj[element.product_code] == undefined) {
                productsObj[element.product_code] = 0;
            }
            productsObj[element.product_code] += 1;
            if (customerObj[element.customer_code] == undefined) {
                customerObj[element.customer_code] = 0;
            }
            customerObj[element.customer_code] += 1;
        }

        res.json({ status: true, products: productsObj, customer: customerObj })
    } else {
        res.json({ status: false, message: "No data found" })
    }

}


// function to get the auntity loaded data 
async function quantityBookedData(req, res) {
    var nosql = new Agent();
    var opt = req.query;
    let qty_to_be_loaded = 0;
    if (!opt.fromDt) {
        fromDt = new Date(new Date().setUTCHours(0, 0, 0, 0));
    } else {
        fromDt = new Date(new Date(opt.fromDt).setUTCHours(0, 0, 0, 0))
    }

    if (!opt.toDt) {
        toDt = new Date(new Date().setUTCHours(23, 59, 59, 999));
    } else {
        toDt = new Date(new Date(opt.toDt).setUTCHours(23, 59, 59, 999));
    }
    nosql.select('truckReport', 'customer_orders').make(function (builder) {
        builder.query('creationDate', {
            $gte: fromDt,
            $lte: toDt
        });
        //builder.where('bay_no', bay + "");
        builder.fields('truck_number', 'max_gross_wt', 'bay_no', 'tare_wt');
        console.log(builder.builder);
    })

    var truckReport = await nosql.promise('truckReport');
    console.log(truckReport.length)
    if (truckReport.length > 0) {
        for (let i = 0; i < truckReport.length; i++) {
            var order = truckReport[i];
            qty_to_be_loaded += (order.max_gross_wt - order.tare_wt);
        }

        res.json({ status: true, data: qty_to_be_loaded / 1000 })
    } else {
        res.json({ status: false, message: "No data found" })
    }

}

// function to get the auntity loaded data 
async function quantityCompletedData(req, res) {
    var nosql = new Agent();
    var opt = req.query;
    let qty_to_be_loaded = 0;
    if (!opt.fromDt) {
        fromDt = new Date(new Date().setUTCHours(0, 0, 0, 0));
    } else {
        fromDt = new Date(new Date(opt.fromDt).setUTCHours(0, 0, 0, 0))
    }

    if (!opt.toDt) {
        toDt = new Date(new Date().setUTCHours(23, 59, 59, 999));
    } else {
        toDt = new Date(new Date(opt.toDt).setUTCHours(23, 59, 59, 999));
    }
    nosql.select('truckReport', 'customer_orders_report').make(function (builder) {
        builder.query('creationDate', {
            $gte: fromDt,
            $lte: toDt
        });
        //builder.where('bay_no', bay + "");
        builder.fields('truck_number', 'target_wt', 'bay_no', 'actual_wt', 'gross_wt', 'offline_gross_wt');
        builder.sort('start_time', 'desc')
        console.log(builder.builder);
    })

    var truckReport = await nosql.promise('truckReport');
    console.log(truckReport.length)
    if (truckReport.length > 0) {
        for (let i = 0; i < truckReport.length; i++) {
            var order = truckReport[i];
            qty_to_be_loaded += (order.gross_wt - order.actual_wt);
        }

        res.json({ status: true, data: qty_to_be_loaded / 1000 })
    } else {
        res.json({ status: false, message: "No data found" })
    }

}

// function to get the deviated customer orders 
async function resetOrder(req, res) {
    var nosql = new Agent();
    nosql.select('getOrder', 'customer_orders').make(function (builder) {
        builder.where('_id', req.body.id);
        builder.first()
    })
    var order = await nosql.promise('getOrder');
    if (order != null) {
        var reset_wt = order.target_wt - order.filling_progress_wt;
        var status = "await_reset_card_punch";
        nosql.update('updateOrder', 'customer_orders').make(function (builder) {
            // builder.set('reset_wt', reset_wt );
            builder.where('action', "AWAIT_CARD_RESET");
            builder.where('_id', req.body.id);
            builder.first()
        })
        var updateOrder = await nosql.promise('updateOrder');
        if (updateOrder > 1) {
            res.send({ status: true, message: "Reset Success" })
        } else {
            res.send({ status: false, message: "Reset Fail!" })
        }

    } else {
        res.send({ status: false, message: "No order found" });
    }
}


// function to get the deviated customer orders 
async function getDeviatedOrders(req, res) {
    var now = new Date();
    // var nowHours = now.getHours();
    var query = { status: "open" };
    var result = [];
    customerOrderModel.find(query).sort({ _id: -1 }).exec(function (err, docs) {
        if (err) {
            res.send({
                status: false,
                message: err
            })
        } else if (docs.length > 0) {
            for (let i = 0; i < docs.length; i++) {
                var doc = docs[i];
                var creationDate = doc.creationDate;
                var hours = Math.abs(creationDate - now) / 36e5;
                // console.log("hours",hours);
                if (hours > 4) {
                    result.push(doc);
                }
            }
            res.json({
                status: true,
                message: "Success",
                data: result
            });
        } else {
            res.send({
                status: false,
                message: "No data found",
                data: []
            })
        }
    });
}


async function printWeighmentData(req, res) {
    // get the printer ip from configuration
    var nosql = new Agent();
    nosql.select('config', 'configuration').make(function (builder) {
        builder.where('configurationName', "global_config");
        builder.first();
    })
    var data = await nosql.promise('config');
    var ip = data.configurationDetails.printerConfig.port;
    console.log("PRINT COMMAND SENT", ip);
    var printer = ipp.Printer(`http://${ip}/ipp/printer`);
    var weighmentData = fs.readFileSync(path.join(__dirname, '../loading.pdf'));
    //console.log("weighmentData-------------------------", weighmentData);
    var msg = {
        "operation-attributes-tag": {
            "requesting-user-name": "William",
            "job-name": "My Test Job",
            "document-format": "application/pdf"
        },
        data: weighmentData
    };
    printer.execute("Print-Job", msg, function (err, result) {
        console.log("PRINTER result------------------------------", result, err);
        res.json({ status: true })
    });
}

async function orderBill(req, res) {
    var body = req.body;
    var nosql = new Agent();
    console.log("body", body);
    var nosql = new Agent();
    nosql.select('docs', 'customer_orders_report').make(function (builder) {
        builder.where('serial_id', body.id);
        //builder.sort("creationDate", "desc");
        builder.first();
    })
    var docs = await nosql.promise('docs');
    console.log("docs", docs);
    if (docs != null) {
        var issueDate = moment(docs.creationDate).format("DD-MM-YYYY"); // note the extra Y in YYYY
        var startTime = moment(docs.start_time).format("DD-MM-YYYY HH:mm:ss");
        var endTime = moment(docs.end_time).format("DD-MM-YYYY HH:mm:ss");

        var obj = {
            slNo: docs.serial_id,
            CardIssueDate: issueDate,
            tankerNo: docs.truck_number,
            customer: docs.customer_code,
            product: docs.product_code,
            tareWt: docs.actual_wt,
            grossWt: docs.gross_wt,
            netWt: docs.gross_wt - docs.actual_wt,
            timeIn: startTime,
            timeOut: endTime,
            bayNo: docs.bay_no,
        };
        createLoadingBill(obj, "loading.pdf");
        res.json({ status: true })

    } else {
        res.json({ status: false, message: "No Data Found" })
    }


}

function addcustomerOrder(req, res) {
    var body = req.body;
    var decoded = req.token;
    // body.srv_due_date = new Date(body.srv_due_date);
    // body.efcv_due_date = new Date(body.eecv_due_date);
    // body.read_permit_date = new Date(body.read_permit_date);
    // body.fc_due_date = new Date(body.fc_due_date);
    // body.insurance_due_date = new Date(body.insurance_due_date);
    // body.ccoe_licence_date = new Date(body.ccoe_licence_date);
    // body.ccoe_licence_date = new Date(body.ccoe_licence_date);
    // body.hydro_test_due_date = new Date(body.hydro_test_due_date);
    // body.fire_extinguisher_date = new Date(body.fire_extinguisher_date);
    if (body._id) {
        delete body._id;
    }
    body.status = "open";
    var dataObj = new customerOrderModel(body);

    //console.log(body.installDate,body.manufactureDate );
    //console.log("dataObj",dataObj);

    dataObj.save(function (err, docs) {
        // console.log("docs",docs);
        if (err) {

            res.send({ status: false, message: err.message })
        }

        else if (docs != null) {
            res.send({ status: true, message: "Customer Order Details saved successfully" });
            let logObj = {
                "user_name": decoded.userName,
                "user_id": decoded._id,
                "event_type": "CUSTOMER-ORDER-CREATE",
                "event_name": `CUSTOMER-ORDER-CREATE-${docs.serial_id}`,
                "log_id": docs.serial_id,
                "data": JSON.stringify(docs),
                "Created_on": new Date()
            };
            logsEmitter.emit('logs', logObj);
        } else {
            res.send({ status: false, message: "Save fail" })
        }
    })

}

async function getcustomerOrders(req, res) {
    var query = {};
    customerOrderModel.find(query).sort({ _id: -1 }).exec(function (err, docs) {
        if (err) {
            res.send({
                status: false,
                message: err
            })
        } else if (docs != null) {
            res.json({
                status: true,
                message: "Success",
                data: docs
            });
        } else {
            res.send({
                status: false,
                message: "No data found",
                data: []
            })
        }
    });
}

async function getcustomerOrdersCopmpleted(req, res) {
    //console.log("LLLLLLLLLLLLLLLLLLLLLL");
    var nosql = new Agent();
    nosql.select('getData', 'customer_orders_report').make(function (builder) {
        builder.sort("end_time", "desc");
    });
    var getData = await nosql.promise('getData');
    // console.log("DDDDDDDDDDDDDDDDDDD", getData);
    if (getData != null) {
        res.json({
            status: true,
            data: getData
        });
    } else {
        res.json({
            status: false,
            data: "No data found"
        });
    }
}

async function getcustomerOrdersCancelled(req, res) {
    var nosql = new Agent();
    nosql.select('getData', 'customer_orders_cancelled').make(function (builder) {
        builder.sort("cancel_time", "desc");
    });
    var getData = await nosql.promise('getData');
    // console.log("DDDDDDDDDDDDDDDDDDD", getData);
    if (getData != null) {
        res.json({
            status: true,
            data: getData
        });
    } else {
        res.json({
            status: false,
            data: "No data found"
        });
    }
}

async function getCustomerOrderStatus(req, res) {
    var nosql = new Agent();
    var resArr = [];
    var fromDt;
    var toDt;
    console.log("STATUS API BODY", req.body);
    console.log("todt", new Date('24-04-2021'));
    if (!req.body.fromDt) {

        fromDt = new Date(new Date().setUTCHours(0, 0, 0, 0));
    } else {
        fromDt = new Date(new Date(req.body.fromDt).setUTCHours(0, 0, 0, 0))
    }

    if (!req.body.toDt) {
        toDt = new Date(new Date().setUTCHours(23, 59, 59, 999));
    } else {
        toDt = new Date(new Date(req.body.toDt).setUTCHours(23, 59, 59, 999));
    }



    var query = {
        creationDate: { $gte: fromDt, $lt: toDt }
    }
    console.log("QUERY", query);

    if (!query.status) {
        var resObj = {
            number: '0',
        }
        var bookedCount = await customerOrderModel.count(query);
        nosql.listing('completedCount', 'customer_orders_report').make(function (builder) {
            builder.query('creationDate', {
                $gte: new Date(fromDt),
                $lte: new Date(toDt)
            });
            builder.page(1, 70000);
        })
        var completedCount = await nosql.promise('completedCount');
        console.log("bookedCount-----", bookedCount, completedCount.count);
        if (!bookedCount || bookedCount == null) {
            resObj.number = "0";
            resArr.push(resObj);
        } else {
            resObj.number = bookedCount + completedCount.count + "";

            resArr.push(resObj);
        }


        console.log("BOOKED query", query, resArr);
    }

    query.status = "pending";
    if (query.status == "pending") {
        var resObj = {
            number: '0'
        }
        var pendingCount = await customerOrderModel.count(query);
        if (!pendingCount || pendingCount == null) {
            resObj.number = "0";
            resArr.push(resObj);
        } else {
            resObj.number = pendingCount + "";
            resArr.push(resObj);
        }
        // console.log("PENDIND query", query , resArr);
    }

    query.status = "inprogress";
    if (query.status == "inprogress") {
        var resObj = {
            number: '0'
        }
        var inProgressCount = await customerOrderModel.count(query);
        if (!inProgressCount || inProgressCount == null) {
            resObj.number = "0";
            resArr.push(resObj);
        } else {
            resObj.number = inProgressCount + "";
            resArr.push(resObj);
        }

        // console.log("INPROGRESS query", query , resArr);
    }

    query.status = "completed";
    if (query.status == "completed") {
        var resObj = {
            number: '0'
        }
        nosql.listing('completedCount', 'customer_orders_report').make(function (builder) {
            builder.query('creationDate', {
                $gte: new Date(fromDt),
                $lte: new Date(toDt)
            });
            builder.where('action', 'LOADING_COMPLETED');
            builder.page(1, 70000);
        })

        var completedCount = await nosql.promise('completedCount');
        if (!completedCount || completedCount == null) {
            resObj.number = "0";
            resArr.push(resObj);
        } else {
            resObj.number = completedCount.count + "";
            resArr.push(resObj);
        }
        console.log("COMPLETEDDDDDDDDDDDDd", query, completedCount);
    }

    query.status = "cancel"
    if (query.status == "cancel") {
        var resObj = {
            number: '0'
        }
        nosql.listing('cancelledCount', 'customer_orders_report').make(function (builder) {
            builder.query('creationDate', {
                $gte: new Date(fromDt),
                $lte: new Date(toDt)
            });
            builder.where('status', 'cancelled');
            builder.page(1, 70000);
        })

        var cancelledCount = await nosql.promise('cancelledCount');
        if (!cancelledCount || cancelledCount == null) {
            resObj.number = "0";
            resArr.push(resObj);
        } else {
            resObj.number = cancelledCount.count + "";
            resArr.push(resObj);
        }
        // console.log("cancelledCount",cancelledCount);
    }

    query.status = "missing"
    if (query.status == "missing") {
        var resObj = {
            number: '0'
        }
        nosql.listing('missingCount', 'customer_orders').make(function (builder) {
            builder.query('creationDate', {
                $gte: new Date(fromDt),
                $lte: new Date(toDt)
            });
            builder.where('action', 'CANCELLED');
            builder.page(1, 70000);
        })

        var missingCount = await nosql.promise('missingCount');
        if (!missingCount || missingCount == null) {
            resObj.number = "0";
            resArr.push(resObj);
        } else {
            resObj.number = missingCount.count + "";
            resArr.push(resObj);
        }
        // console.log("cancelledCount query",query ,resArr);
    }
    console.log("RES ARRRA", resArr);
    res.send({ status: true, data: resArr })

}

async function cancleCustomerOrder(req, res) {
    var body = req.body;
    var nosql = new Agent();
    console.log("body", body);
    // get canceled order
    nosql.select('getCancelOrder', 'customer_orders').make(function (builder) {
        builder.where('serial_id', body.serial_id);
        builder.first();
    });

    // move cancel order to customer order reports table
    var getCancelOrder = await nosql.promise('getCancelOrder');
    console.log("getCancelOrder", getCancelOrder);
    getCancelOrder.action = "cancelled";
    getCancelOrder.remarks = body.remarks;
    getCancelOrder.cancel_time = new Date();
    delete getCancelOrder._id;
    nosql.insert('reportSave', 'customer_orders_cancelled').make(function (builder) {
        builder.set(getCancelOrder);
    });
    await nosql.promise('reportSave');

    // remove the cancelle order in temp table
    nosql.remove('removeTempOrder', 'customer_orders').make(function (builder) {
        builder.where('serial_id', body.serial_id);
    });

    await nosql.promise('removeTempOrder');
    res.send({ status: true, message: "Order Cancel Success" })
    let logObj = {
        "user_id": decoded._id,
        "event_type": "CUSTOMER-ORDER-CANCEL",
        "event_name": `CUSTOMER-ORDER-CANCEL-${body.serial_id}`,
        "log_id": body.serial_id,
        "data": JSON.stringify(getCancelOrder),
        "Created_on": new Date()
    };
    logsEmitter.emit('logs', logObj);


}

function cardNumVerify(req, res) {
    var body = req.body;
    customerOrderModel.findOne({ card_num: body.cardNum }).exec(function (err, docs) {
        if (err) {
            res.send({ status: false, message: err })
        } else if (docs != null) {
            if (docs.status == "cancelled") {
                res.send({ status: true, message: "Card is valid" })
            } else {
                res.send({ status: false, message: "Card is already in use" })
            }
        } else {
            res.send({ status: true, message: "Card is valid" })
        }
    })
}

function truckVerify(req, res) {
    var body = req.body;
    customerOrderModel.findOne({ truck_number: body.truckNum }).exec(function (err, docs) {
        if (err) {
            res.send({ status: false, message: err })
        } else if (docs != null) {
            if (docs.status == "cancelled") {
                res.send({ status: true, message: "Truck Number is valid" })
            } else {
                res.send({ status: false, message: "Truck Number already exists" })
            }
        } else {
            res.send({ status: true, message: "Truck Number is valid" })
        }
    })
}

async function cancelInprogressOrder(req, res) {
    var body = req.body;
    var nosql = new Agent();
    var decoded = req.token;
    //console.log("body", body);
    // get canceled order
    nosql.select('getCancelOrder', 'customer_orders').make(function (builder) {
        builder.where('serial_id', body.serial_id);
        builder.first();
    });

    // move cancel order to customer order reports table
    var getCancelOrder = await nosql.promise('getCancelOrder');
    //console.log("getCancelOrder", getCancelOrder);
    if (getCancelOrder == null) {
        return res.send({
            status: false,
            message: "Order not found"
        })
    }
    if (getCancelOrder.status != "inprogress") {
        return res.send({
            status: false,
            message: "Order is not in progress!"
        })
    }
    getCancelOrder.action = "completed";
    getCancelOrder.gross_wt = body.gross_wt;
    getCancelOrder.remarks = body.remarks;
    getCancelOrder.end_time = new Date(body.end_time);
    getCancelOrder.update_type = body.update_type;
    delete getCancelOrder._id;
    nosql.insert('reportSave', 'customer_orders_report').make(function (builder) {
        builder.set(getCancelOrder);
    });
    await nosql.promise('reportSave');

    // remove the cancelle order in temp table
    nosql.remove('removeTempOrder', 'customer_orders').make(function (builder) {
        builder.where('serial_id', body.serial_id);
    });

    await nosql.promise('removeTempOrder');
    res.send({ status: true, message: "Order Cancel Success" });
    let logObj = {
        "user_name": decoded.userName,
        "user_id": decoded._id,
        "event_type": "CUSTOMER-INPROGRESS-ORDER-CANCEL",
        "event_name": `CUSTOMER-INPROGRESS-ORDER-CANCEL-${body.serial_id}`,
        "log_id": body.serial_id,
        "data": JSON.stringify(getCancelOrder),
        "Created_on": new Date()
    };
    logsEmitter.emit('logs', logObj);
    var Users = await getUsers();

    var userIds = [];
    if (Users.length > 0) {
        for (let i = 0; i < Users.length; i++) {
            const element = Users[i];
            var obj = {
                isSeen: false,
                id: element._id + ""
            }
            userIds.push(obj)
        }
    }

    let notifyObj = {
        "event_type": "CUSTOMER-INPROGRESS-ORDER-CANCEL",
        "event_name": `CUSTOMER-INPROGRESS-ORDER-CANCEL-${body.serial_id}`,
        "log_id": body.serial_id,
        "user_name": decoded.userName,
        "data": JSON.stringify(getCancelOrder),
        "user_ids": userIds,
        "Created_on": new Date()
    };
    notifictionEmitter.emit('notify', notifyObj);
}

async function getCancelOrderNotifyCount(req, res) {
    var nosql = new Agent();
    var decoded = req.token;
    console.log("decoded", decoded);
    nosql.listing('getNotify', 'notifications').make(function (builder) {
        builder.page(1, 1000);
        builder.where('user_ids.id', decoded._id);
        builder.and();
        builder.where('user_ids.isSeen', false);
    });

    var getNotify = await nosql.promise('getNotify');
    console.log("getNotify-------------", getNotify);
    if (getNotify.count > 0) {
        res.send({ status: true, data: getNotify.count })
    } else {
        res.send({ status: true, data: 0 })
    }
}

async function getCancelOrderNotify(req, res) {
    var nosql = new Agent();
    var decoded = req.token;
    nosql.select('getNotify', 'notifications').make(function (builder) {
        //builder.where('user_ids.id', decoded.id);
    });

    var getNotify = await nosql.promise('getNotify');
    //console.log("getNotify-------------", getNotify);
    if (getNotify.length > 0) {
        var mnosql = new Agent();
        res.send({ status: true, data: getNotify })
        getNotify.map((resp) => {
            if (resp.user_ids != null) {
                resp.user_ids.map((notify) => {
                    if (notify.id == decoded._id) {
                        notify.isSeen = true;
                    }
                })
            } else {
                console.log("userids null");
            }

        })
        // update the isSeen false
        mnosql.update('updateNotify', 'notifications').make(function (builder) {
            builder.where('user_ids.id', decoded._id);
            builder.and();
            builder.set('user_ids', getNotify.user_ids);
        });
        mnosql.exec(function (err, response) {
            if (err) {
                console.log("err", err);
            }
            console.log("updateNotify===============", response.updateNotify);

        })





    } else {
        res.send({ status: false, message: "No notifications" })
    }
}

async function getUsers() {
    var nosql = new Agent();
    nosql.select('getUsers', 'users').make(function (builder) {
        builder.where('isActive', true);
    });

    var users = await nosql.promise('getUsers');
    return users;
}







