// mongo db connection 
var config = require('../config');
var MONGO_DB_CONNECTION = config.mongo;
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);
var MongoClient = require('mongodb').MongoClient;
const DB_NAME = "online-weighbridge";
var moment = require('moment');
const assert = require('assert');
var async = require('async');

module.exports = {
  getActivityLogs,
  getCustomerOrders,
  getWeighmentData,
  getProductOrders,
  getTruckReport,
  getCardReport,
  getBayReport,
  getTransportReport,
  getAnalyticalReport,
  getUtilityReport
}

// Utility Map sample data format: [
//   {data: [65, 59, 80, 60, 50, 60, 70, 80], label: 'Engaged Hours'},
//   {data: [65, 30, 80, 49, 50, 86, 25, 82], label: 'Idel Hours'}
// ];

// function to get the utility report 
async function getUtilityReport(req, res) {
  var nosql = new Agent();
  var opt = req.query;
  nosql.select('truckReport', 'customer_orders_report').make(function (builder) {
    if (opt.date) {
      var fromDt = new Date(opt.date).setUTCHours(0, 0, 0, 0);
      var toDt = new Date(opt.date).setUTCHours(23, 59, 59, 999);
      builder.query('start_time', {
        $gte: new Date(fromDt),
        $lte: new Date(toDt)
      });
    }
    //builder.where('bay_no', bay + "");
    builder.fields('start_time', 'end_time', 'truck_number', 'target_wt', 'bay_no', 'actual_wt', 'gross_wt', 'offline_gross_wt');
    builder.sort('start_time', 'desc')
    console.log(builder.builder);
  })

  var truckReport = await nosql.promise('truckReport');
  //console.log("truckReport", truckReport.length);
  var minsEngaged = 0;
  var minsIdle = 0;
  var bayReportTemplate = {
    bay_1: { minsEngaged: 0 ,netwt:0},
    bay_2: { minsEngaged: 0 ,netwt:0},
    bay_3: { minsEngaged: 0 ,netwt:0},
    bay_4: { minsEngaged: 0 ,netwt:0},
    bay_5: { minsEngaged: 0 ,netwt:0},
    bay_6: { minsEngaged: 0 ,netwt:0},
    bay_7: { minsEngaged: 0 ,netwt:0},
    bay_8: { minsEngaged: 0 ,netwt:0}

  };
  //console.table(truckReport);
  for (let i = 0; i < truckReport.length; i++) {
    let report_time;
    if (i == 0) {
      report_time = new Date(opt.date).setUTCHours(0, 0, 0, 0);
    } else {
      report_time = truckReport[i - 1].end_time;
      //console.log("PREVIS START TIME", report_time);
    }
    var data = truckReport[i];
    var start_time = data.start_time;
    var end_time = data.end_time;
    var diff = end_time - start_time;

    var minutes = Math.floor((diff / 1000) / 60);
    if (data.bay_no != null) {
      bayReportTemplate['bay_' + data.bay_no].minsEngaged += minutes;
      bayReportTemplate['bay_' + data.bay_no].netwt += (data.gross_wt - data.actual_wt);
    }
    var duration = (minutes * 100) / 1440;
    minsEngaged += duration / 60;
    var diff1 = data.start_time - report_time;
    var dura = ((Math.floor((diff1 / 1000) / 60)) * 100) / 1440;
    // console.log("DURA ", dura);
    if (dura < 0) {
      dura = -1 * dura;
    }
    if (dura > 0) {
      minsIdle += dura / 60;
    }
  }
  var summaryData = [
    { data: [], label: "Engaged Hours" }, // in hours
    { data: [], label: "Idle Hours" } // in hours

  ]
  var netSummary = [
    { data: [], label: 'Tons' }
  ]
  for (const key in bayReportTemplate) {
    const element = bayReportTemplate[key];
    element.minsIdle = 1440 - element.minsEngaged;
    element.hoursIdle = parseFloat((element.minsIdle / 60).toFixed(2));
    element.hoursEngaged = parseFloat((element.minsEngaged / 60).toFixed(2));

    summaryData[0].data.push(element.hoursEngaged);
    summaryData[1].data.push(element.hoursIdle);
    netSummary[0].data.push(parseInt(element.netwt/1000)); // in tons
  }

  res.json({ status: true, summary: summaryData, bayreport: bayReportTemplate , netSummary: netSummary});
}

// function to get the analytical report 
async function getAnalyticalReport(req, res) {
  var nosql = new Agent();
  var opt = req.query;
  console.log("OOOOOOOO", opt); // 2021-03-02
  var result = [];
  var summaryData = {
    minsEngaged: 0, // in minutes
    minsIdle: 0, // in minutes
    grossDeviation: 0, // in count
    netQty: 0, //in kgs
  }
  nosql.select('truckReport', 'customer_orders_report').make(function (builder) {
    if (opt.date) {
      var fromDt = new Date(opt.date).setUTCHours(0, 0, 0, 0);
      var toDt = new Date(opt.date).setUTCHours(23, 59, 59, 999);
      builder.query('start_time', {
        $gte: new Date(fromDt),
        $lte: new Date(toDt)
      });
    }
    builder.fields('start_time', 'end_time', 'truck_number', 'target_wt', 'bay_no', 'actual_wt', 'gross_wt', 'offline_gross_wt');
    builder.sort('start_time', 'desc')
    console.log(builder.builder);
  })

  var truckReport = await nosql.promise('truckReport');
  truckReport.reverse();
  var offlineObj = {
    status: false,
    duration: 0
  }

  for (let i = 0; i < truckReport.length; i++) {
    let report_time;
    if (i == 0) {
      report_time = new Date(opt.date).setUTCHours(0, 0, 0, 0);
    } else {
      report_time = truckReport[i - 1].end_time;
      //console.log("PREVIS START TIME", report_time);
    }

    let tempObj = {};
    var data = truckReport[i];
    var start_time = data.start_time;
    var end_time = data.end_time;
    var diff = end_time - start_time;
    var minutes = Math.floor((diff / 1000) / 60);
    var duration = (minutes * 100) / 1440;
    data.status = true;
    data.duration = parseFloat(duration.toFixed(2));

    summaryData.minsEngaged += parseFloat(minutes.toFixed(2));
    summaryData.netQty += (data.gross_wt - data.actual_wt);
    //console.log("data.offline_gross_wt - data.gross_wt",data.truck_number, data.offline_gross_wt - data.gross_wt);
    if (data.offline_gross_wt - data.gross_wt > 500) {
      summaryData.grossDeviation += 1;
    }
    var diff1 = data.start_time - report_time;
    var dura = Math.floor((diff1 / 1000) / 60);
    // console.log("DURA ", dura);
    if (dura < 0) {
      dura = -1 * dura;
    }
    if (dura > 0) {
      tempObj.duration = parseFloat(dura.toFixed(2));
      tempObj.status = false;
      summaryData.minsIdle += tempObj.duration;
    }

    result.push(tempObj);
    var strtt1 = data.start_time.toISOString().split('T')[1].split(':')[0];
    var strtt2 = data.start_time.toISOString().split('T')[1].split(':')[1];
    var endt1 = data.end_time.toISOString().split('T')[1].split(':')[0];
    var endt2 = data.end_time.toISOString().split('T')[1].split(':')[1];
    data.startTime = strtt1 + ":" + strtt2;
    data.endTime = endt1 + ":" + endt2;
    result.push(data);

  }

  if (result != null) {
    summaryData.minsEngaged = parseFloat(summaryData.minsEngaged.toFixed(2))
    res.json({
      status: true,
      data: result,
      summary: summaryData
    })
  } else {
    res.json({
      status: false,
      message: "No data found"
    })
  }

  return;
}

async function getTruckReport(req, res) {
  var nosql = new Agent();
  var opt = req.query;
  console.log("OOOOOOOO", opt);
  nosql.select('truckReport', 'customer_orders_report').make(function (builder) {
    opt.truck_number && builder.where('truck_number', opt.truck_number);

    if (opt.fromDt && opt.toDt) {
      var fromDt = new Date(opt.fromDt).setUTCHours(0, 0, 0, 0);
      var toDt = new Date(opt.toDt).setUTCHours(23, 59, 59, 999);
      builder.query('creationDate', {
        $gte: new Date(fromDt),
        $lte: new Date(toDt)
      });
    }
    builder.fields();
    console.log(builder.builder);
  })

  var truckReport = await nosql.promise('truckReport');


  if (truckReport != null) {

    res.json({
      status: true,
      data: truckReport
    })
  } else {
    res.json({
      status: false,
      message: "No data found"
    })
  }

  return;
}


async function getCardReport(req, res) {
  var nosql = new Agent();
  var opt = req.query;
  console.log("OOOOOOOO", opt);
  nosql.select('cardReport', 'customer_orders_report').make(function (builder) {
    opt.card_num && builder.where('card_num', opt.card_num);

    if (opt.fromDt && opt.toDt) {
      var fromDt = new Date(opt.fromDt).setUTCHours(0, 0, 0, 0);
      var toDt = new Date(opt.toDt).setUTCHours(23, 59, 59, 999);
      builder.query('creationDate', {
        $gte: new Date(fromDt),
        $lte: new Date(toDt)
      });
    }
    builder.fields();
  })
  var cardReport = await nosql.promise('cardReport');


  if (cardReport != null) {

    res.json({
      status: true,
      data: cardReport
    })
  } else {
    res.json({
      status: false,
      message: "No data found"
    })
  }

}

async function getBayReport(req, res) {
  var nosql = new Agent();
  var opt = req.query;
  console.log("OOOOOOOO", opt);
  nosql.select('bayReport', 'customer_orders_report').make(function (builder) {
    opt.bay_no && builder.where('bay_no', opt.bay_no);

    if (opt.fromDt && opt.toDt) {
      var fromDt = new Date(opt.fromDt).setUTCHours(0, 0, 0, 0);
      var toDt = new Date(opt.toDt).setUTCHours(23, 59, 59, 999);
      builder.query('creationDate', {
        $gte: new Date(fromDt),
        $lte: new Date(toDt)
      });
    }
    builder.fields();
  })
  var bayReport = await nosql.promise('bayReport');


  if (bayReport != null) {

    res.json({
      status: true,
      data: bayReport
    })
  } else {
    res.json({
      status: false,
      message: "No data found"
    })
  }

}

async function getTransportReport(req, res) {
  var nosql = new Agent();
  var opt = req.query;
  console.log("OOOOOOOO", opt);
  nosql.select('transportReport', 'customer_orders_report').make(function (builder) {
    opt.transporter_name && builder.where('transporter_name', opt.transporter_name);

    if (opt.fromDt && opt.toDt) {
      var fromDt = new Date(opt.fromDt).setUTCHours(0, 0, 0, 0);
      var toDt = new Date(opt.toDt).setUTCHours(23, 59, 59, 999);
      builder.query('creationDate', {
        $gte: new Date(fromDt),
        $lte: new Date(toDt)
      });
    }
    builder.fields();
  })
  var transportReport = await nosql.promise('transportReport');


  if (transportReport != null) {

    res.json({
      status: true,
      data: transportReport
    })
  } else {
    res.json({
      status: false,
      message: "No data found"
    })
  }

}

// GET Method
// Get all Activity Logs
async function getActivityLogs(req, res) {
  var nosql = new Agent();
  let query = req.query;
  var fromDt;
  var toDt;
  if (query.fromDt && query.toDt) {
    fromDt = new Date(query.fromDt).setUTCHours(0, 0, 0, 0);
    toDt = new Date(query.toDt).setUTCHours(23, 59, 59, 999);
  }

  nosql.select('getData', 'activity_logs').make(function (builder) {
    query.event_type && builder.where('event_type', query.event_type);
    if (query.fromDt && query.toDt) {
      builder.query('Created_on', {
        $gte: new Date(fromDt),
        $lte: new Date(toDt)
      });
    }
    builder.fields('_id', 'user_name', 'user_id', 'event_type', 'event_name', 'log_id', 'Created_on');
    builder.sort("Created_on", "desc");
  });

  var getData = await nosql.promise('getData');
  if (getData.length > 0) {
    res.json({
      status: true,
      data: getData
    });
  } else {
    res.json({
      status: false,
      data: "No data found"
    });
  }
}

// GET Method
// Get all Customer Based Orders
async function getCustomerOrders(req, res) {

  let query = req.query;
  var fromDt;
  var toDt;
  if (query.fromDt && query.toDt) {
    fromDt = new Date(query.fromDt).setUTCHours(0, 0, 0, 0);
    toDt = new Date(query.toDt).setUTCHours(23, 59, 59, 999);
  }
  //console.log(query, "AAAAAAA", new Date(fromDt), toDt);

  const client = new MongoClient(MONGO_DB_CONNECTION, { useNewUrlParser: true });
  client.connect(function (err, C) {

    //const start = moment(new Date()).startOf('month').format();
    const db = C.db(DB_NAME);
    const collection = db.collection('customer_orders_report');
    collection.aggregate([
      {
        $match: {
          "creationDate": { $gte: new Date(fromDt), $lte: new Date(toDt) },
        }
      },
      {
        $group: {
          "_id": {
            "customerName": "$customer_name",
          },
          "Count": { "$sum": 1 }
        },
      },
      {
        $project: {
          "customerName": "$_id.customerName",
          "count": "$Count",
          "_id": 0
        }
      }
    ]).toArray(function (err, result) {
      // console.log("RESSSSSSSSSSSSU", result, err);
      C.close();
      if (err) {
        //console.log("EEEEEEEEEEe", err);
        res.json({
          status: false,
          data: "No data found"
        });
        return;
      }
      let tempObj = {
        names: [],
        counts: []
      };
      for (let i = 0; i < result.length; i++) {
        const element = result[i];
        if (element.customerName != null) {
          tempObj.names.push(element.customerName);
          tempObj.counts.push(element.count);
        }

      }
      res.json({
        status: true,
        data: tempObj
      });
    });
  });
}

// GET Method
// Get all Customer Based Orders
async function getProductOrders(req, res) {

  let query = req.query;
  var fromDt;
  var toDt;
  if (query.fromDt && query.toDt) {
    fromDt = new Date(query.fromDt).setUTCHours(0, 0, 0, 0);
    toDt = new Date(query.toDt).setUTCHours(23, 59, 59, 999);
  }
  //console.log(query, "AAAAAAA", new Date(fromDt), toDt);

  const client = new MongoClient(MONGO_DB_CONNECTION, { useNewUrlParser: true });
  client.connect(function (err, C) {

    //const start = moment(new Date()).startOf('month').format();
    const db = C.db(DB_NAME);
    const collection = db.collection('customer_orders_report');
    collection.aggregate([
      {
        $match: {
          "creationDate": { $gte: new Date(fromDt), $lte: new Date(toDt) },
        }
      },
      {
        $group: {
          "_id": {
            "productName": "$product_name",
          },
          "Count": { "$sum": 1 }
        },
      },
      {
        $project: {
          "productName": "$_id.productName",
          "count": "$Count",
          "_id": 0
        }
      }
    ]).toArray(function (err, result) {
      // console.log("RESSSSSSSSSSSSU", result, err);
      C.close();
      if (err || result.length == 0) {
        //console.log("EEEEEEEEEEe", err);
        res.json({
          status: false,
          data: "No data found"
        });
        return;
      }

      let tempObj = {
        names: [],
        counts: []
      };
      for (let i = 0; i < result.length; i++) {
        const element = result[i];
        if (element.productName != null) {
          tempObj.names.push(element.productName);
          tempObj.counts.push(element.count);
        }

      }
      res.json({
        status: true,
        data: tempObj
      });
    });
  });
}


// GET Method
// Get weighment data
async function getWeighmentData(req, res) {
  let query = req.query;
  var fromDt;
  var toDt;
  if (query.fromDt && query.toDt) {
    fromDt = new Date(query.fromDt).setUTCHours(0, 0, 0, 0);
    toDt = new Date(query.toDt).setUTCHours(23, 59, 59, 999);
  }
  //console.log(query, "AAAAAAA", new Date(fromDt), toDt);

  const client = new MongoClient(MONGO_DB_CONNECTION, { useNewUrlParser: true });
  client.connect(function (err, C) {

    //const start = moment(new Date()).startOf('month').format();
    const db = C.db(DB_NAME);
    const collection = db.collection('customer_orders_report');
    collection.aggregate([
      {
        $match: {
          "creationDate": { $gte: new Date(fromDt), $lte: new Date(toDt) },
        }
      },
      {
        $group: {
          "_id": null,
          "sumGross": { "$sum": "$gross_wt" },
          "sumActual": { "$sum": "$actual_wt" },
        },
      },
      {
        $project: {
          "GrossWt": "$sumGross",
          "ActualWt": "$sumActual",
          "_id": 0
        }
      }
    ]).toArray(function (err, result) {
      //console.log("RESSSSSSSSSSSSU", result);
      C.close();
      if (err || result.length == 0) {
        //console.log("EEEEEEEEEEe", err);
        res.json({
          status: false,
          data: "No data found"
        });
        return;
      }
      var weighmentData = result[0].GrossWt - result[0].ActualWt;
      res.json({
        status: true,
        data: weighmentData
      });
    });
  });
}
