
// mongo db connection 
var config = require('../config');
const userMgmtModel = require('../models/users.model');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
var JWT_SECRET = "GOLDBUILD";
var JWT_EXPIRE = "30d";
var JWT_COOKIE_EXPIRE = "30";
var logsEmitter = require('../emitters/logsEmitter');
module.exports = {
    addUser,
    getUsers,
    deleteUser,
    updateUserByName,
    getUserByName,
    userLogin,
    changePassword
}

// POST Method
// Login user with valid credentials
async function userLogin(req, res) {
    var body = req.body;

    //check userName & password exist or not
    if (!body.userName || !body.password) {
        res.json({
            status: false,
            messaage: "Please provide UserName & Password"
        })

    }

    //check userName and password exist in DB or not
    const user = await userMgmtModel.findOne({ userName: body.userName}).select('+password').lean();

    if (!user) {
        res.json({
            status: false,
            messaage: "UserName not found"
        })
    }
    if(!user.isActive) {
        return  res.json({
            status: false,
            messaage: "User is inactive!"
        })
    }
    const pwdMatch = await matchPassword(body.password, user.password);
    if (!pwdMatch) {
        res.json({
            status: false,
            messaage: "Incorrect Password"
        })

    }
    delete user.password;
    await sendTokenResponse(user, 200, res)

}


// Post Method
// Saved Users 
function addUser(req, res) {
    console.log("reaaaa", req.body);
    const userM = new userMgmtModel(req.body);
    var decoded = req.token;
    userM.save(function (err, docs) {
        if (err) {
            return res.json({
                status: false,
                message: err.message
            })
        }

        res.json({
            status: true,
            message: "User created."
        })
        let logObj = {
            "user_name" : decoded.userName,
            "user_id": decoded._id,
            "event_type":"USER-CREATE",
            "event_name": `USER-CREATE-${docs._id}`,
            "log_id":docs._id,
            "data": JSON.stringify(docs),
            "Created_on":new Date()
        };
        logsEmitter.emit('logs', logObj);
    });
}


// GET Method
// Get all Users which are isActive:true
function getUsers(req, res) {

    userMgmtModel.find({}, { creationDate: 0, __v: 0 }).exec(function (err, docs) {
        // console.log(docs);

        if (err) {
            res.json({
                status: false,
                message: err.message
            })
        }
        else if (docs.length > 0) {
            res.json({
                status: true,
                message: "Success",
                data: docs
            })
        }
        else {
            res.json({
                status: false,
                message: "Data not found"
            })
        }
    })

}


//PUT Method
// Logically delete user by change isActive to false
function deleteUser(req, res) {
    var body = req.body;
    var decoded = req.token;
    if (body.password) {
        delete body.password;
    }
    userMgmtModel.findOneAndUpdate({ userName: body.userName }, { $set: { isActive: body.isActive } }, { new: true }).exec(function (err, docs) {
        if (err) {
            res.json({
                status: false,
                message: err.message
            })
        }
        else if (docs) {
            res.json({
                status: true,
                message: "User Updated."
            })
            let logObj = {
                "user_name" : decoded.userName,
                "user_id": decoded._id,
                "event_type":"USER-DELETE",
                "event_name": `USER-DELETE-${docs._id}`,
                "log_id":docs._id,
                "data": JSON.stringify(docs),
                "Created_on":new Date()
            };
            logsEmitter.emit('logs', logObj);
        }
        else {
            res.json({
                status: false,
                message: "User not found"
            })
        }
    })

}

//PUT Method
// Update user 
function updateUserByName(req, res) {

    var body = req.body;
    var decoded = req.token;
    if (body.password) {
        delete body.password;
    }
    body.lastUpdateDate = new Date();
    userMgmtModel.findOneAndUpdate({ userName: body.userName }, { $set: body }, { new: true }).exec(function (err, docs) {
        console.log("dddddddddd", docs);
        if (err) {
            res.json({
                status: false,
                message: err.message
            })
        }
        else if (docs) {
            res.json({
                status: true,
                message: "User Updated"
            })
            let logObj = {
                "user_name" : decoded.userName,
                "user_id": decoded._id,
                "event_type":"USER-UPDATE",
                "event_name": `USER-UPDATE-${docs._id}`,
                "log_id":docs._id,
                "data": JSON.stringify(docs),
                "Created_on":new Date()
            };
            logsEmitter.emit('logs', logObj);
        }
        else {
            res.json({
                status: false,
                message: "User not found"
            })
        }
    })

}

//PUT Method
// Update user 
async function  changePassword(req, res) {

    var body = req.body;
    var decoded = req.token;
    const salt = await bcrypt.genSalt(10);
    let pwd = await bcrypt.hashSync(body.password, salt);

    body.lastUpdateDate = new Date();
    userMgmtModel.findOneAndUpdate({ userName: body.userName }, { $set: {password:pwd} }, { new: true }).exec(function (err, docs) {
        console.log("dddddddddd", docs);
        if (err) {
            res.json({
                status: false,
                message: err.message
            })
        }
        else if (docs) {
            res.json({
                status: true,
                message: "Password Updated"
            })
            let logObj = {
                "user_name" : decoded.userName,
                "user_id": decoded._id,
                "event_type":"USER-PASSWORD-UPDATE",
                "event_name": `USER-PASSWORD-UPDATE-${docs._id}`,
                "log_id":docs._id,
                "data": JSON.stringify(docs),
                "Created_on":new Date()
            };
            logsEmitter.emit('logs', logObj);
        }
        else {
            res.json({
                status: false,
                message: "User not found"
            })
        }
    })

}

// Get Method
// Get user by userName
function getUserByName(req, res) {
    let query = {};

    query.userName = req.query.userName;
    userMgmtModel.findOne(query).exec(function (err, docs) {
        if (err) {
            res.json({
                status: false,
                message: err.message
            })
        }
        else if (docs) {
            res.json({
                status: true,
                message: "Sucess",
                data: docs
            })
        }
        else {
            res.json({
                status: false,
                message: "User not found"
            })
        }
    })
}

//Get token from model, create cookie and send response
const sendTokenResponse = async (user, statusCode, res) => {
    //create token
    const token = await getSignedJwtToken(user._id);

    const options = {
        expires: new Date(
            Date.now() + 30 * 24 * 60 * 60 * 1000
        ),
        httpOnly: true
    }
    res.status(statusCode).cookie('token', token, options).json({
        success: true,
        message: "Hello..! You're Welcome",
        token: token,
        user: user
    });

}

// Match user entered password to hashed password in database
async function matchPassword(enteredPassword, userPassword) {
    return await bcrypt.compare(enteredPassword, userPassword);
};

// Sign JWT and return
async function getSignedJwtToken(id) {

    return jwt.sign({ id: id }, JWT_SECRET, {
        expiresIn: JWT_EXPIRE
    });
};