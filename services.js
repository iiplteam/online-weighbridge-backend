var pm2 = require('pm2');

pm2.connect(function (err) {
    if (err) {
        console.error(err);
        process.exit(2);
    }

    // pm2.list((err, list) => {
    //     console.log(err, list)
    //     pm2.disconnect();
    // })
    


    // pm2.stop('test-service', (err, proc) => {
    //     pm2.disconnect();
    // })

    pm2.restart('test-service', (err, proc) => {
        pm2.disconnect();
    })

    // pm2.start({
    //     name:"test-service",
    //     script: 'comm-test.js',         // Script to be run        
    //     max_memory_restart: '100M'   // Optional: Restarts your app if it reaches 100Mo
    // }, function (err, apps) {
    //     pm2.disconnect();   // Disconnects from PM2
    //     if (err) throw err
    // });
});