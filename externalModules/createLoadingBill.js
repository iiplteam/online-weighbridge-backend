const fs = require("fs");
const PDFDocument = require("pdfkit");

function createLoadingBill(invoice, path) {
  let doc = new PDFDocument({ 
    size: "A4", 
    margin: 50 , 
    //userPassword: invoice.employeeid
  });

  generateHeader(doc);
  generateEmployeeInformation(doc, invoice);
  generateInvoiceTable(doc, invoice);
  //generateFooter(doc);

  doc.end();
  doc.pipe(fs.createWriteStream(path));
}

function generateHeader(doc) {
  doc
    .image("total-logo.png", 250, 40, { width: 100 })
    .fillColor("#444444")
    .fontSize(16)
    .font("Helvetica-Bold")
    .fillColor("#000000")
    .text("MANGALORE MARITIME TERMINAL", 160, 90)
    .fontSize(10)
    .font("Helvetica-Bold")
    .text("WEIGHMENT SLIP", 250, 115 )
    .moveDown();
    // doc.image("anpr.png", 490, 50, { width: 60,align:"right" })
    // .text("ANPR", 450, 115,{ width: 90, align: "right" })  .moveDown();
}

function generateEmployeeInformation(doc, invoice) {
//   doc
//     .fillColor("#000000")
//     .fontSize(16)
//     .text("Loading Bill", 110, 120);

  generateHr(doc, 135);

  const customerInformationTop = 150;

  doc
    .fontSize(10)
    .font("Helvetica-Bold")
    .text("TOTAL OIL INDIA PVT LTD", 50, customerInformationTop)
    .font("Helvetica")
    .text("LPG DIVISION", 50, customerInformationTop + 15)
    .font("Helvetica")
    .text("Thokur Village, Jokatte Post", 50, customerInformationTop + 30)
    .font("Helvetica")
    .text("Baikampady Industrial Estate", 50, customerInformationTop + 45)
    .font("Helvetica")
    .text("Mangalore-574173", 50, customerInformationTop + 60)
    .text("Token No :", 300, customerInformationTop)
    .font("Helvetica")
    .text(invoice.slNo, 400, customerInformationTop)
    .font("Helvetica")
    .text("Card Issue Date", 300, customerInformationTop + 15)
    .font("Helvetica")
    .text(invoice.CardIssueDate, 400, customerInformationTop + 15)
    .font("Helvetica")
    .text("Tanker No.",300, customerInformationTop + 30)
    .font("Helvetica-Bold")
    .text(invoice.tankerNo,400, customerInformationTop + 30)
    .font("Helvetica")
    .text("Customer",300, customerInformationTop + 45)
    .font("Helvetica")
    .text(invoice.customer,400, customerInformationTop + 45)
    .font("Helvetica")
    .text("Product",300, customerInformationTop + 60)
    .font("Helvetica")
    .text(invoice.product,400, customerInformationTop + 60)


    // .font("Helvetica-Bold")
    // .text("UTR#",300, customerInformationTop + 45)
    // .font("Helvetica")
    // .text(invoice.UTRReference,400, customerInformationTop + 45)
    .moveDown();

  generateHr(doc, customerInformationTop + 75);
}

function generateInvoiceTable(doc, invoice) {
  let i;
  const invoiceTableTop = 95 + 150;
  doc.font("Helvetica");
  //generateCr(doc, 300, 300, 272, 500);

  i = 1;

  // for (i = 0; i < invoice.items.length; i++) {
  //   const item = invoice.items[i];
  //   const position = invoiceTableTop + (i + 1) * 30;
  //   generateTableRow(
  //     doc,
  //     position,
  //     item.item,
  //     item.description,
  //     formatCurrency(item.amount / item.quantity),
  //     formatCurrency(item.amount)
  //   );

  //   generateHr(doc, position + 20);
  //}

  const subtotalPosition = invoiceTableTop ;

  generateTableRow(
    doc,
    subtotalPosition,
    "Tare Weight (Kgs)",
    ":  "+invoice.tareWt,
    "Time Commenced",
    ":  "+ invoice.timeIn
  );

  const ConveyancePosition = subtotalPosition + 20;
  generateTableRow(
    doc,
    ConveyancePosition,
    "Gross Weight (Kgs)",
    ":  "+ invoice.grossWt,
    "Time Completed",
    // "",
    ":  "+invoice.timeOut
  );

  const tds = ConveyancePosition + 20;
  generateTableRow(
    doc,
    tds,
    "Net Weight (Kgs)",
    ":  "+ invoice.netWt,
    "Bay No",
    // "",
    ":  " + invoice.bayNo
  );

  generateHr(doc, tds + 20);
    
  var  final = tds + 40 

  doc
  .fontSize(10)
  .text("", 50, final)
  .text("", 200, final, { width: 90, align: "right" })

  .text("", 390, final )
//  .text(quantity, 370, y, { width: 90, align: "right" })
  .text("", 0, final, { align: "right" });

  final = final + 45;
  doc
  .fontSize(10)
  .text("", 50, final)
  .text("", 200, final, { width: 90, align: "right" })
  .text("Logistics Staff", 380, final )
  .text("", 0, final, { align: "right" });
    
}

function generateFooter(doc) {
  doc.font("Helvetica");
  doc
    .fontSize(10)
    .text(
      "This is a computer generated salary slip signature not needed.",
      50,
      750,
      { 
        align: "center", 
        width: 500 
      }
    );
}

function generateTableRow(
  doc,
  y,
  item,
  description,
  unitCost,
  lineTotal
) {
  doc
    .fontSize(10)
    .text(item, 50, y)
    .text(description, 175, y, { width: 90})
    .text(unitCost, 300, y )
  //  .text(quantity, 370, y, { width: 90, align: "right" })
    .text(lineTotal, 400, y);
}

function generateHr(doc, y) {
  doc
    .strokeColor("#aaaaaa")
    .lineWidth(1)
    .moveTo(50, y)
    .lineTo(550, y)
    .stroke();
}

function generateCr(doc, x1, x2, y1, y2) {
  doc
    .strokeColor("#aaaaaa")
    .lineWidth(1)
    .moveTo(x1, y1)
    .lineTo(x2, y2)
    .stroke();
}

function formatCurrency(cents) {
  return "INR " + (cents / 100).toFixed(2);
}

function formatDate(date) {
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();
  return year + "/" + month + "/" + day;
}

module.exports = {
    createLoadingBill
};
