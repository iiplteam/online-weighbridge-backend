var configdata = require('./config.js');
var MONGO_DB_CONNECTION = configdata.mongo;
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);
var fs = require('fs');
var async = require('async');

async function insertTruckDetails() {
    //console.log("HELLLOOOOOO");
    var nosql = new Agent();
    var res = JSON.parse(fs.readFileSync(__dirname + '/truckmaster.json'));
    //console.log("res", res);
    async.eachLimit(res, 500, function (element, callback) {
            //return;
            var obj = {
                "truck_number": element.Tno,
                "transporter_name": element.Transpname,
                "tare_wt": element.PrintedTare,
                "max_gross_wt": element.PrintedGross,
                "ccoe_wt": element.PrintedCCOE,
                "target_wt": element.Max_Qty_Load,
                "coarse_set_point": element.Coarse_Set_Point,
                "srv_due_date": new Date(element.SRVDue),
                "efcv_due_date": new Date(element.EFCV_Due_Dt),
                "hydro_test_due_date": new Date(element.HydroDue),
                "fc_due_date": new Date(element.FCDue),
                "fire_extinguisher_date": new Date(element.Fire_Exti_Date),
                "road_permit_date": new Date(element.Road_Permit_Dt),
                "product_code": element.Pcode,
                "customer_code": element.ccode,
                "remarks": element.remarks
              }

            //   console.log("obj", obj);
            //   return ;
            nosql.insert('save', 'truck_masters').make(function (builder) {
                builder.set(obj);
                // builder.where('name', element.name)
            });
            nosql.exec(function (err, response) {
                if (err) {
                    //console.log("MongoErr", err);
                }
                console.log("response", response.save);
                callback();
            });

        
    }, function (err) {
        // if any of the file processing produced an error, err would equal that error
        if (err) {
            // One of the iterations produced an error.
            // All processing will now stop.
            console.log('A file failed to process');
        } else {
            console.log('All files have been processed successfully');
        }
    });

}

insertTruckDetails();