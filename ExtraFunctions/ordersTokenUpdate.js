var config = require('../config');
var MONGO_DB_CONNECTION = config.mongo;
var Agent = require('sqlagent/mongodb').connect(MONGO_DB_CONNECTION);

async function tokenSerialize() {
    var nosql = new Agent();
    
    nosql.select('completedOrders','customer_orders_report').make(function (builder) {
    })
    var completedOrders = await nosql.promise('completedOrders');
    let token = 1;
    for (let i = 0; i < completedOrders.length; i++) {
       
        let order = completedOrders[i];
        let serial_id = token++;
        nosql.update('updateOrder','customer_orders_report').make(function (builder) {
            builder.set('serial_id', serial_id);
            builder.where('_id',order._id)
        })
        var updateOrder = await nosql.promise('updateOrder');
        console.log("updateOrder" ,updateOrder);
    }

}

tokenSerialize();