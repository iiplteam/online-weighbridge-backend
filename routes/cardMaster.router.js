var express = require('express');
var router = express.Router();
var cardMaster = require('../controllers/cardMaster');
const { protect } = require('../middleware/auth');


router.post('/save-card', protect, cardMaster.saveCard);
router.get('/get-cards', protect, cardMaster.getCards);
router.get('/get-card', protect, cardMaster.getCard);

module.exports = router;