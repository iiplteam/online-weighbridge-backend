var express = require('express');
var router = express.Router();
var user_controller = require('../controllers/userMgmt');
const { protect } = require('../middleware/auth');
/* GET home page. */
router.post('/' , protect , user_controller.addUser);
router.put('/', protect , user_controller.updateUserByName);
router.get('/user', protect , user_controller.getUserByName);

router.get('/', protect , user_controller.getUsers);
router.post('/user-login', user_controller.userLogin);
router.delete('/', protect ,user_controller.deleteUser);
router.put('/change-password', protect ,user_controller.changePassword);


module.exports = router;
