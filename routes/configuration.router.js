var express = require('express');
var router = express.Router();
var configurations = require('../controllers/configuration');
const { protect } = require('../middleware/auth');

/* GET home page. */
router.post('/bay-config', protect, configurations.BayConfig);
router.get('/bay-config', protect, configurations.getBayConfig);

router.post('/comm-config', protect, configurations.commConfig);
router.get('/comm-config', protect, configurations.getCommConfig);

router.post('/global-config', protect, configurations.globalConfig);
router.get('/global-config', protect, configurations.getGlobalConfig);

module.exports = router;
