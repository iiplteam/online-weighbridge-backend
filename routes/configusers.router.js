var express = require('express');
var router = express.Router();
var configUsers = require('../controllers/configUsers');
const { protect } = require('../middleware/auth');

/* GET home page. */
router.get('/',protect, configUsers.getUsers);
router.put('/:id',protect, configUsers.updateUsers);
router.post('/save-config',protect, configUsers.configusers);

module.exports = router;
