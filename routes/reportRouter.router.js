var express = require('express');
var router = express.Router();
var report_controller = require('../controllers/reports');
const { protect } = require('../middleware/auth');

/* GET home page. */
router.get('/truck-report', protect, report_controller.getTruckReport);
router.get('/card-report', protect, report_controller.getCardReport);
router.get('/bay-report', protect, report_controller.getBayReport);
router.get('/transporter-report', protect, report_controller.getTransportReport);

router.get('/weighment-data', protect, report_controller.getWeighmentData);
router.get('/activity-log', protect, report_controller.getActivityLogs);
router.get('/customerbased-orders', protect, report_controller.getCustomerOrders);
router.get('/productbased-orders', protect, report_controller.getProductOrders);

// api to get the analytical report
router.get('/analytical-report', protect, report_controller.getAnalyticalReport);

// api to get the utility report
router.get('/utility-report', protect, report_controller.getUtilityReport);




module.exports = router;
