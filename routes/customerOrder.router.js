var express = require('express');
var router = express.Router();
var customer_controller = require('../controllers/customerOrder');
const { protect } = require('../middleware/auth');

/* GET home page. */
router.post('/',protect, customer_controller.addcustomerOrder);
router.get('/',protect, customer_controller.getcustomerOrders);
router.get('/completed',protect, customer_controller.getcustomerOrdersCopmpleted);
router.post('/status-counts',protect, customer_controller.getCustomerOrderStatus);
router.put('/cancel-order',protect, customer_controller.cancleCustomerOrder);
router.get('/cancelled',protect, customer_controller.getcustomerOrdersCancelled);

router.post('/card-verify',protect, customer_controller.cardNumVerify);
router.post('/truck-verify', protect,customer_controller.truckVerify);
router.post('/order-bill',protect, customer_controller.orderBill);

// api to cancel inprogress cutomer order
router.put('/cancel-inprogress-order',protect, customer_controller.cancelInprogressOrder);
router.get('/cancel-order-notify',protect, customer_controller.getCancelOrderNotify);
router.get('/cancel-order-notify-count',protect, customer_controller.getCancelOrderNotifyCount);

// api to get the deviated customer orders
router.get('/deviated-orders', protect, customer_controller.getDeviatedOrders);

// api to reset the customer order
router.post('/reset-order', protect, customer_controller.resetOrder);

// api to print the customer order
router.get('/print-order', protect, customer_controller.printWeighmentData);

// api to get the quantity completed 
router.get('/quantity-completed', protect, customer_controller.quantityCompletedData);

// api to get the quantity booked 
router.get('/quantity-booked', protect, customer_controller.quantityBookedData);

// api to get the customer orders reports
router.get('/customer-orders', protect, customer_controller.customerOrders);

module.exports = router;
