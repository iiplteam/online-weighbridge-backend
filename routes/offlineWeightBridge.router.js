var express = require('express');
var router = express.Router();
var offlineWeightBridge = require('../controllers/offlineWeighBridge');
const { protect } = require('../middleware/auth');

/* GET home page. */
router.get('/get-records',protect, offlineWeightBridge.getRecords);
router.get('/get-record',protect, offlineWeightBridge.getSingle);
router.post('/save',protect, offlineWeightBridge.saveRecord);

module.exports = router;
