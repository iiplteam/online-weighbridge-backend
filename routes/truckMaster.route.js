var express = require('express');
var router = express.Router();
var truck_controller = require('../controllers/truckMaster');
const { protect } = require('../middleware/auth');
/* GET home page. */
router.post('/',protect, truck_controller.addTruck);
router.put('/:id',protect, truck_controller.updateTruck);
router.get('/',protect, truck_controller.getTrucks);
router.get('/:id',protect, truck_controller.getTruckById);
router.delete('/',protect, truck_controller.deleteTruck);


module.exports = router;
