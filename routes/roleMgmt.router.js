var express = require('express');
var router = express.Router();
var role_controller = require('../controllers/roleMgmt');
const { protect } = require('../middleware/auth');

/* GET home page. */
router.post('/', protect, role_controller.addRole);
router.put('/', protect, role_controller.updateRoleById);
router.get('/', protect, role_controller.getRoles);
router.get('/role', protect, role_controller.getRoleByName);
// router.delete('/', role_controller.deleteRole);


module.exports = router;
