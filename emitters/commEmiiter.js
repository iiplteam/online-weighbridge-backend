// config file
var configdata = require('../config');

var mongo_url = configdata.mongo;
var Agent = require('sqlagent/mongodb').connect(mongo_url);

// importing emitter module
const EventEmitter = require('events');
const myEmitter = new EventEmitter();

// importing pm2 module
var pm2 = require('pm2');
var as = require('async')



var commEmitter = myEmitter.on('comm-service', async function services() {
    var nosql = new Agent();
    nosql.select('docs', 'configuration').make(function (builder) {
        builder.where('configurationName', 'bay_config');
        builder.first();
    });

    var docs = await nosql.promise('docs');
    var configuration = docs.configurationDetails;
    pm2.connect(function (err) {
        if (err) {
            console.error(err);
            return;
        }
        as.eachOfLimit(configuration, 1, function (bayData, key, callback) {
            if (bayData.isActive == false) {
                pm2.stop(`bay-${bayData.bayNo}`, (err, proc) => {
                    console.log(`bay-${bayData.bayNo} successfully stopped`);
                    callback();
                })
            } else {
                pm2.restart(`bay-${bayData.bayNo}`, (err, proc) => {
                    console.log(`bay-${bayData.bayNo} successfully restarted`);
                    callback();
                })

            }

        }, function (err) {
            if (err) console.error(err.message);
            // configs is now a map of JSON data
            pm2.disconnect();
        });
    })
})
module.exports = commEmitter;